<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\School;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $school = School::ghostSchools()->first();

        $user = User::create([
            'school_id' => $school->id,
            'first_name' => 'Super',
            'surname' => 'Administrator',
            'password' => Hash::make('super.Admin.P@ss'),
            'email' =>'superadmin@' . parse_url(config('app.url'))['host'],
            'email_verified_at' => now(),
            'gender' => 'male',
        ]);

        $role = Role::superAdmin()->first();
        $user->roles()->attach($role->id);

        //Create an Admin Entry for this user
        $user->admin()->create([
            'first_name' => $user->first_name,
            'last_name' => $user->surname,
            'username' => 'superadmin'
        ]);
    }
}
