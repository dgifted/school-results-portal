<?php

namespace Database\Seeders;

use App\Models\ScratchCard;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ScratchCardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ScratchCard::factory()->count(20)->create();
    }
}
