<?php

namespace Database\Seeders;

use App\Models\School;
use App\Models\Subject;
use App\Services\GeneralService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $schools = School::all();
        $subjectsId = Subject::all()->pluck('id')->toArray();
        $schools->each(function ($school) use ($subjectsId) {
            $school->subjects()->detach($subjectsId);
        });
        DB::table('school_subject')->truncate();
        Subject::truncate();

        $commonSubjects = [
            'English Language' => 'Eng',
            'Mathematics' => 'Maths',
            'Biology' => 'Bio',
            'Physics' => 'Phy',
            'Igbo Language' => 'Igbo',
            'Chemistry' => 'Chem',
            'Commerce' => 'Comm',
            'Further Maths' => 'F.Maths',
            'Agricultural Science' => 'Agric',
            'Geography' => 'Geo',
            'Christian Religious Studies' => 'C.R.S',
            'History' => 'Hist',
            'Fine & Applied Arts' => 'Arts',
            'Government' => 'Govt',
            'Economics' => 'Econs',
            'Literature in English' => 'Lit',
            'Health & Physical Education' => 'H.P.E',
            'Introductory Technology' => 'Intro. Tech',
            'Integrated Science' => 'Inte Sci'
        ];

            foreach ($commonSubjects as $subject => $code) {
                Subject::create([
                    'name' => $subject,
                    'code' => $code,
                    'ref_id' => GeneralService::generateReferenceId()
                ]);
            }
            $schools->each(function ($school) {
                Subject::query()->each(function ($subj) use ($school) {
                    $school->subjects()->attach($subj->id);
                });
            });
    }
}
