<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            SchoolsSeeder::class,
            UsersSeeder::class,
            AdminsSeeder::class,
            SubjectsSeeder::class,
//            SchoolClassesSeeder::class,  //NOTICE Done in the one of the migration files.
            StudentsSeeder::class,
            ScratchCardsSeeder::class
        ]);
    }
}
