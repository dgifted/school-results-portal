<?php

namespace Database\Seeders;

use App\Models\SchoolClass;
use App\Models\YearGroup;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        SchoolClass::truncate();

        $commonClassSections = ['A', 'B', 'C', 'D', 'E'];


        YearGroup::query()->each(function ($yG) use ($commonClassSections) {
            $schools = $yG->schools;

            $schools->each(function ($school) use ($commonClassSections, $yG) {
                foreach ($commonClassSections as $className) {
                    $yG->classes()->create([
                        'name' => $className,
                        'school_id' => $school->id
                    ]);
                }
            });

        });

    }
}
