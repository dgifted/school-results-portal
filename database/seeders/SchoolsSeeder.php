<?php

namespace Database\Seeders;

use App\Models\School;
use App\Models\SchoolType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        School::truncate();

        $secondarySchoolType = SchoolType::secondarySchool()->first();

        $domesticSchools = [
            [
                'name' => 'Saint Teresa\'s College Nsukka',
                'abbr' => 'STCN',
                'address' => 'Odenigbo Road by Total Round About Nsukka, Enugu State.'
            ],
            [
                'name' => 'Nsukka High School',
                'abbr' => 'NHSN',
                'address' => 'Nsukka High School, Nsukka.',
            ],
            [
                'name' => 'Queen of the Holy Rosary Secondary School, Nsukka',
                'abbr' => 'QRSSN',
                'address' => 'Enugu Rd, Owere Nsukka 410001, Nsukka, Enugu.'
            ],
            [
                'name' => 'Shalom Academy Nsukka',
                'abbr' => 'SAN',
                'address' => 'Nsukka - Onitsha Rd, Nsukka, Nigeria.',
            ],
            [
                'name' => 'New Creation Secondary School',
                'abbr' => 'NCSS',
                'address' => 'Zion City, Agbani Nsukka, Enugu State.'
            ],
            [
                'name' => 'OBFO International Academy',
                'abbr' => 'OIA',
                'address' => 'University Town, opposite Urban Girls Secondary School, Ihe Nsukka 410105, Nsukka, Enugu.'
            ],
            [
                'name' => 'Model Secondary School Nsukka',
                'abbr' => 'MSSN',
                'address' => 'Barracks Road, Esut Campus Road, Nsukka.'
            ],
            [
                'name' => 'Community Secondary School Igogoro',
                'abbr' => 'CSSI',
                'address' => 'Umuozzi II Igogoro, Nigeria Enugu State.'
            ],
            [
                'name' => 'Government Technical School Nsukka',
                'abbr' => 'GTCN',
                'address' => 'P.M.B 923 Nsukka, Enugu State, Nigeria.'
            ],
            [
                'name' => 'Mea Mater Elizabeth High School, Agbani',
                'abbr' => 'MEHSA',
                'address' => 'Ojiagu-Agbani, PMB 01610 Enugu Nigeria.'
            ]
        ];

        foreach ($domesticSchools as $domesticSchool) {
            $secondarySchoolType->schools()->create([
                'name' => $domesticSchool['name'] . ' [' . $domesticSchool['abbr'] . ']',
                'address' => $domesticSchool['address']
            ]);
        }
    }
}
