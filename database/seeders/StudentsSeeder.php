<?php

namespace Database\Seeders;

use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::notAdmin()->get()->each(function ($nonAdmin) {
            $selSchool = $nonAdmin->school;
            $randomFilteredClass = $selSchool->classes()->get()->pluck('id')->toArray();

            $nonAdmin->student()->create([
                'school_class_id' => $randomFilteredClass[array_rand($randomFilteredClass)],
                'first_name' => $nonAdmin->first_name,
                'surname' => $nonAdmin->surname,
                'roll_id' => rand(1, 3000),
                'dob' => fake()->dateTimeBetween(),
                'reg_no' => rand(1, 9) . '-' .rand(100000, 999999)
            ]);

        });
    }
}
