<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('student_passwords', function (Blueprint $table) {
            $table->id();
            $table->foreignId('student_id');
            $table->string('plain_text');
            $table->timestamps();
        });

        \App\Models\Student::query()->each(function (\App\Models\Student $student) {
            $student->plainPassword()->create([
                'plain_text' => \Illuminate\Support\Str::random(10)
            ]);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('student_passwords');
    }
};
