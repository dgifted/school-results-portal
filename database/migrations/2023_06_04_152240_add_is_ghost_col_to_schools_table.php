<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->enum('is_ghost', [
                \App\Models\School::IS_GHOST,
                \App\Models\School::IS_NOT_GHOST
            ])->default(\App\Models\School::IS_NOT_GHOST);
        });

        //Create the ghost school entry
        \App\Models\School::create([
            'school_type_id' =>  \App\Models\SchoolType::SecondarySchool()->first()->id,
            'name' => 'Ghost School [GS]',
            'address' => '',
            'is_ghost' => \App\Models\School::IS_GHOST,
            'ref_id' => \App\Services\GeneralService::generateReferenceId()
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('is_ghost');
        });
    }
};
