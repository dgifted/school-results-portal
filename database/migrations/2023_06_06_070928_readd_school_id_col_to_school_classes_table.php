<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('school_classes', function (Blueprint $table) {
            $table->foreignId('school_id')->nullable()->after('id');
        });

        $schools = \App\Models\School::all();
        \App\Models\SchoolClass::all()->each(function ($class) use ($schools) {
            $schools->each(function ($school) use ($class) {
                $school->classes()->create();
            });
        });
//        $classes = \App\Models\SchoolClass::all();
//        \App\Models\School::all()->each(function ($school) use (&$classes) {
//            $classes->each(function ($class) use ($school) {
//                $class->school_id = $school->id;
//                $class->save();
//            });
//        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('school_classes', function (Blueprint $table) {
            $table->dropColumn('school_id');
        });
    }
};
