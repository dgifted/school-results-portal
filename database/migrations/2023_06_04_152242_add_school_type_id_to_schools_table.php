<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->foreignId('school_type_id')->nullable()->after('id');
        });

        $secondarySchoolType = \App\Models\SchoolType::secondarySchool()->first();
        \App\Models\School::all()->each(function ($school) use ($secondarySchoolType) {
            $school->school_type_id = $secondarySchoolType->id;
            $school->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('school_type_id');
        });
    }
};
