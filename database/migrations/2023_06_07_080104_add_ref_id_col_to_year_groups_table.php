<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('year_groups', function (Blueprint $table) {
            $table->string('ref_id')->nullable()->after('id')->unique();
        });

        \App\Models\YearGroup::query()->each(function ($yG) {
            $yG->ref_id = \App\Services\GeneralService::generateReferenceId();
            $yG->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('year_groups', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
};
