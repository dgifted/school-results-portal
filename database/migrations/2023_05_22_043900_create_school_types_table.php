<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('school_types', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30)->unique();
            $table->string('short_code', 10)->nullable();
            $table->enum('status', [
                \App\Models\SchoolType::STATUS_ACTIVE,
                \App\Models\SchoolType::STATUS_INACTIVE
            ])->default(\App\Models\SchoolType::STATUS_ACTIVE);
            $table->timestamps();
        });

        $commonSchoolTypes = [
            'nursery' => 'NS',
            'primary' => 'PS',
            'secondary' => 'SS'
        ];

        foreach ($commonSchoolTypes as $name => $shortCode) {
            \App\Models\SchoolType::create([
                'name' => $name,
                'short_code' => $shortCode
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('school_types');
    }
};
