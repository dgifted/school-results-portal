<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('school_classes', function (Blueprint $table) {
            $table->dropColumn('school_id');
            $table->string('alias')->nullable()->after('name_numeric');
        });

//        $commonClasses = [
//            'JSS 1' => [
//                'numeric' => 1,
//                'sections' => ['A', 'B', 'C', 'D', 'E']
//            ],
//            'JSS 2' => [
//                'numeric' => 2,
//                'sections' => ['A', 'B', 'C', 'D']
//            ],
//            'JSS 3' => [
//                'numeric' => 3,
//                'sections' => ['A', 'B', 'C']
//            ],
//            'SS 1' => [
//                'numeric' => 4,
//                'sections' => ['A', 'B', 'C', 'D', 'E']
//            ],
//            'SS 2' => [
//                'numeric' => 5,
//                'sections' => ['A', 'B', 'C', 'D']
//            ],
//            'SS 3' => [
//                'numeric' => 6,
//                'sections' => ['A', 'B', 'C']
//            ],
//        ];
//
//        foreach ($commonClasses as $name => $data) {
//            foreach ($data['sections'] as $section) {
//                \App\Models\SchoolClass::create([
//                    'name' => $name,
//                    'name_numeric' => $data['numeric'],
//                    'section' => $section
//                ]);
//            }
//        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('school_classes', function (Blueprint $table) {
            $table->foreignId('school_id');
            $table->dropColumn('alias');
        });
    }
};
