<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('notices', function (Blueprint $table) {
            $table->string('ref_id')->nullable()->unique()->after('id');
        });

        \App\Models\Notice::withoutGlobalScopes()->get()->each(function ($notice) {
            $notice->ref_id = \App\Services\GeneralService::generateReferenceId();
            $notice->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('notices', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
};
