<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('school_sign_ups', function (Blueprint $table) {
            $table->id();
            $table->string('school_name', 50);
            $table->string('email', 50)->nullable();
            $table->string('phone', 15);
            $table->string('address', 50);
            $table->enum('website_exist', ['yes', 'no'])->nullable();
            $table->string('website_address', 30)->nullable();
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('school_sign_ups');
    }
};
