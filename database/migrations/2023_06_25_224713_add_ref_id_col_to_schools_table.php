<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->string('ref_id')->after('id')->nullable()->unique();
        });

        \App\Models\School::query()->each(function ($school) {
            $school->ref_id = \App\Services\GeneralService::generateReferenceId();
            $school->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
};
