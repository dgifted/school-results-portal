<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name', 20);
            $table->string('common_name', 20);
            $table->timestamps();
        });

        $systemRoles = [
            'admin' => 'Administrator',
            'teacher' => 'Teacher',
            'student' => 'Student',
            'super' => 'Super Admin'
        ];

        foreach ($systemRoles as $name => $c_name) {
            \App\Models\Role::create([
                'name' => $name,
                'common_name' => $c_name
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('roles');
    }
};
