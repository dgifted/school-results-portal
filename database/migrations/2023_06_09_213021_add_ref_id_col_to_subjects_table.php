<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->string('ref_id')->nullable()->after('id')->unique();
        });

        \App\Models\Subject::query()->each(function ($subject) {
            $subject->ref_id = \App\Services\GeneralService::generateReferenceId();
            $subject->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('subjects', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
};
