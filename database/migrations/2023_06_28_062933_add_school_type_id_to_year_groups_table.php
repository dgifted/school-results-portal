<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('year_groups', function (Blueprint $table) {
            $table->foreignId('school_type_id')->nullable()->after('id');
        });

        $schoolType = \App\Models\SchoolType::secondarySchool()->first();
        \App\Models\YearGroup::query()->each(function ($yG) use ($schoolType) {
            $yG->school_type_id = $schoolType->id;
            $yG->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('year_groups', function (Blueprint $table) {
            $table->dropColumn('school_type_id');
        });
    }
};
