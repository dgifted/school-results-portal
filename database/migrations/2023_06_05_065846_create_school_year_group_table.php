<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('school_year_group', function (Blueprint $table) {
            $table->id();
            $table->foreignId('school_id');
            $table->foreignId('year_group_id');
            $table->timestamps();
        });

        $yearGroups = \App\Models\YearGroup::all();
        \App\Models\School::query()->each(function ($school) use ($yearGroups) {
            $yearGroups->each(function ($yG) use ($school) {
                $yG->schools()->attach($school->id);
            });
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('school_year_group');
    }
};
