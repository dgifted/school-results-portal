<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('year_groups', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('short_code')->nullable();
            $table->enum('status', [
                \App\Services\GeneralService::STATUS_ACTIVE,
                \App\Services\GeneralService::STATUS_INACTIVE,
            ])->default(\App\Services\GeneralService::STATUS_ACTIVE);
            $table->timestamps();
        });

        $commonYearGroups = [
            'Year one' => 'JSS 1',
            'Year two' => 'JSS 2',
            'Year three' => 'JSS 3',
            'Year four' => 'SS 1',
            'Year five' => 'SS 2',
            'Year six' => 'SS 3'
        ];

        foreach ($commonYearGroups as $name => $shortCode) {
            \App\Models\YearGroup::create([
                'name' => $name,
                'short_code' => $shortCode
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('year_groups');
    }
};
