<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('scratch_cards', function (Blueprint $table) {
            $table->string('ref_id')->unique()->nullable()->after('id');
        });

        if (\App\Models\ScratchCard::query()->count() > 0) {
            \App\Models\ScratchCard::query()->each(function ($card) {
                $card->ref_id = \App\Services\GeneralService::generateReferenceId();
                $card->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('scratch_cards', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
};
