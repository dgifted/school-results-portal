<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('school_classes', function (Blueprint $table) {
            $table->string('ref_id')->nullable()->unique()->after('id');
        });

        \App\Models\SchoolClass::all()->each(function ($class) {
            $class->ref_id = \App\Services\GeneralService::generateReferenceId();
            $class->save();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('school_classes', function (Blueprint $table) {
            $table->dropColumn('ref_id');
        });
    }
};
