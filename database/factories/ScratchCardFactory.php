<?php

namespace Database\Factories;

use App\Services\GeneralService;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ScratchCard>
 */
class ScratchCardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     * @throws \Exception
     */
    public function definition(): array
    {
        return [
            'pin' => random_int(1000, 9999) .
                '-' . random_int(1000, 9999) .
                '-' . random_int(1000, 9999) .
                '-' . random_int(1000, 9999),
            'ref_id' => GeneralService::generateReferenceId()
        ];
    }
}
