<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Admin>
 */
class AdminFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $users = User::all()->filter(function ($user) {
            return is_null($user->admin);
        })->toArray();

        $user = fake()->randomElement($users);

        return [
            'user_id' => $user['id'],
            'first_name' => $user['first_name'],
            'last_name' => $user['surname'],
            'username' => fake()->userName
        ];
    }
}
