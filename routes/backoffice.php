<?php

use Illuminate\Support\Facades\Route;

Route::prefix('backoffice')
    ->middleware('superAdmin')
    ->name('backoffice.')
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\BackOffice\DashboardController::class, 'showDashboardPage'])->name('home');
        Route::get('onboard-school', [\App\Http\Controllers\BackOffice\SchoolOnboardController::class, 'showOnboardPage'])->name('onboard-school');
        Route::post('onboard-school', [\App\Http\Controllers\BackOffice\SchoolOnboardController::class, 'storeSchool']);
        Route::get('onboard-school/assign-admin/{schoolRefId}', [\App\Http\Controllers\BackOffice\SchoolOnboardController::class, 'assignAdminToSchool'])->name('assign-admin');
        Route::post('onboard-school/assign-admin/{schoolRefId}', [\App\Http\Controllers\BackOffice\SchoolOnboardController::class, 'storeSchoolAdmin']);
        Route::get('schools', [\App\Http\Controllers\BackOffice\SchoolController::class, 'showAllSchoolsPage'])->name('schools');
        Route::get('schools/edit/{schoolRefId}', [\App\Http\Controllers\BackOffice\SchoolController::class, 'showSchoolUpdatePage'])->name('schools.edit');
        Route::patch('schools/edit/{schoolRefId}', [\App\Http\Controllers\BackOffice\SchoolController::class, 'updateSchool']);
        Route::get('schools/details/{schoolRefId}', [\App\Http\Controllers\BackOffice\SchoolController::class, 'showSchoolDetailsPage'])->name('schools.details');

        Route::get('year-groups', [\App\Http\Controllers\BackOffice\YearGroupController::class, 'showYearGroupsPage'])->name('year-groups');
        Route::post('year-groups', [\App\Http\Controllers\BackOffice\YearGroupController::class, 'storeYearGroup']);
        Route::get('year-groups/{yearGroupRefId}', [\App\Http\Controllers\BackOffice\YearGroupController::class, 'editYearGroup'])->name('year-groups.edit');
        Route::patch('year-groups/{yearGroupRefId}', [\App\Http\Controllers\BackOffice\YearGroupController::class, 'updateYearGroup']);

        Route::get('school-classes', [\App\Http\Controllers\BackOffice\SchoolClassController::class, 'showClassesPage'])->name('school-classes');
        Route::get('school-classes/{schoolRefId}/add', [\App\Http\Controllers\BackOffice\SchoolClassController::class, 'showAddClassPage'])->name('school-classes.add');
        Route::post('school-classes/{schoolRefId}/add', [\App\Http\Controllers\BackOffice\SchoolClassController::class, 'storeClass']);

        Route::get('students-regenerate-ids/{schoolRefId}', [\App\Http\Controllers\BackOffice\StudentController::class, 'regenerateStudentsId'])->name('students.regenerate-ids');

        Route::get('manage-scratch-cards', [\App\Http\Controllers\BackOffice\ScratchCardController::class, 'showScratchCardPage'])->name('scratch-cards.manage');
        Route::get('create-scratch-cards', [\App\Http\Controllers\BackOffice\ScratchCardController::class, 'generateScratchCards'])->name('scratch-cards.create');
        Route::delete('delete-scratch-card/{cardRefId}', [\App\Http\Controllers\BackOffice\ScratchCardController::class, 'deleteScratchCard'])->name('scratch-cards.delete');

        //Logs
        Route::get('/logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index'])->name('admin.view-logs');

        Route::prefix('ajax-routes')->group(function () {
            Route::get('school/classes/{schoolRefId}', [\App\Http\Controllers\BackOffice\SchoolClassController::class, 'getAllClassesForSchool']);
        });
    });
