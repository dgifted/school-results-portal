<?php

use Illuminate\Support\Facades\Route;

Route::redirect('/', '/home');
Route::middleware('guest')->group(function () {
    Route::get('/home', [\App\Http\Controllers\PageViewController::class, 'landingPage'])->name('home');
    Route::get('/check-result', [\App\Http\Controllers\PageViewController::class, 'checkResult'])->name('results.check');
    Route::post('/check-result', [\App\Http\Controllers\ResultCheckController::class, 'checkResult']);
    Route::get('/result-detail', [\App\Http\Controllers\PageViewController::class, 'studentResultDetailsPage'])->name('results.detail');
    Route::get('/school-signup', [\App\Http\Controllers\PageViewController::class, 'schoolSignupPage'])->name('school.signup');
    Route::post('/school-signup', [\App\Http\Controllers\SchoolSignUpController::class, 'storeSchoolSignUp']);
});

Route::middleware(['auth'])->prefix('admin')->group(function () {
    Route::get('/', [\App\Http\Controllers\PageViewController::class, 'dashboard'])->name('dashboard');

    Route::get('/add-class', [\App\Http\Controllers\PageViewController::class, 'addClasses'])->name('classes.add');
    Route::post('/add-class', [\App\Http\Controllers\SchoolClassController::class, 'storeSchoolClass'])->name('classes.add.post');
    Route::get('/edit-class/{refId}', [\App\Http\Controllers\PageViewController::class, 'editClass'])->name('classes.edit');
    Route::patch('/edit-class/{refId}', [\App\Http\Controllers\SchoolClassController::class, 'updateSchoolClass']);
    Route::get('/manage-classes', [\App\Http\Controllers\PageViewController::class, 'manageClasses'])->name('classes.manage');
    Route::get('/manage-year-group', [\App\Http\Controllers\PageViewController::class, 'manageYearGroup'])->name('year-groups.manage');
    Route::post('/import-classes', [\App\Http\Controllers\SchoolClassController::class, 'bulkStudentClassImport'])->name('classes.import');

    Route::get('/create-subject', [\App\Http\Controllers\PageViewController::class, 'createSubject'])->name('subjects.create');
    Route::post('/create-subject', [\App\Http\Controllers\SubjectController::class, 'storeSubject']);
    Route::get('/manage-subject', [\App\Http\Controllers\PageViewController::class, 'manageSubject'])->name('subjects.manage');
    Route::post('/import-subjects', [\App\Http\Controllers\SubjectController::class, 'bulkSubjectsImport'])->name('subjects.import');

    Route::get('/add-subject-combination', [\App\Http\Controllers\PageViewController::class, 'addSubjectCombination'])->name('subject.add-combination');
    Route::post('/add-subject-combination', [\App\Http\Controllers\SubjectCombinationController::class, 'storeSubjectCombination']);
    Route::patch('/toggle-subject-combination/{id}', [\App\Http\Controllers\SubjectCombinationController::class, 'toggleSubComboStatus'])->name('subject.toggle-combination-status');
    Route::get('/manage-subject-combination', [\App\Http\Controllers\PageViewController::class, 'manageSubjectCombination'])->name('subject.manage-combination');

    Route::get('/add-student', [\App\Http\Controllers\PageViewController::class, 'addStudent'])->name('students.add');
    Route::post('/add-student', [\App\Http\Controllers\StudentController::class, 'storeStudent']);
    Route::get('/edit-student/{uId}', [\App\Http\Controllers\PageViewController::class, 'editStudent'])->name('students.edit');
    Route::patch('/edit-student/{uId}', [\App\Http\Controllers\StudentController::class, 'updateStudent']);
    Route::get('/manage-student', [\App\Http\Controllers\PageViewController::class, 'manageStudents'])->name('students.manage');
    Route::get('/student-detail/{uId}', [\App\Http\Controllers\PageViewController::class, 'studentDetails'])->name('students.detail');
    Route::post('/student/{uId}/upload-passport', [\App\Http\Controllers\StudentController::class, 'uploadStudentPassport'])->name('students.upload-passport');

    Route::get('/add-result', [\App\Http\Controllers\PageViewController::class, 'addResult'])->name('results.add');
    Route::post('/add-result/{studentUId}', [\App\Http\Controllers\ResultController::class, 'storeStudentResult']);
    Route::get('/edit-result/{studentUId}', [\App\Http\Controllers\PageViewController::class, 'editStudentResult'])->name('editStudentResult');
    Route::patch('/edit-result/{studentUId}', [\App\Http\Controllers\ResultController::class, 'updateStudentResult']);
    Route::get('/manage-result', [\App\Http\Controllers\PageViewController::class, 'manageResult'])->name('results.manage');
    Route::get('/result-detail/{studentUId}', [\App\Http\Controllers\PageViewController::class, 'viewStudentResult'])->name('results.view');

    Route::get('/add-notice', [\App\Http\Controllers\PageViewController::class, 'addNotice'])->name('notices.add');
    Route::post('/add-notice', [\App\Http\Controllers\NoticeController::class, 'storeSchoolNotice']);
    Route::get('/notices/{refId}', [\App\Http\Controllers\PageViewController::class, 'noticeDetails'])->name('notices.detail');
    Route::delete('/notices/{refId}/delete', [\App\Http\Controllers\NoticeController::class, 'deleteSchoolNotice'])->name('notices.delete');
    Route::get('/manage-notice', [\App\Http\Controllers\PageViewController::class, 'manageNotice'])->name('notices.manage');

    Route::get('/help', [\App\Http\Controllers\PageViewController::class, 'help'])->name('help');
    Route::get('/profile', [\App\Http\Controllers\PageViewController::class, 'profile'])->name('profile');
    Route::patch('/profile', [\App\Http\Controllers\ProfileController::class, 'updateAdminProfile']);
    Route::patch('/profile/change-password', [\App\Http\Controllers\ProfileController::class, 'changePassword'])->name('profile.changePassword');

    Route::prefix('ajax-routes')->name('ajax.')->group(function () {
        Route::get('dashboard/payload', [\App\Http\Controllers\DashboardController::class, 'getDashboardData'])->name('dashboard.payload');
        Route::get('school-classes/all', [\App\Http\Controllers\SchoolClassController::class, 'getAllSchoolClass'])->name('school-classes');
        Route::get('school-classes/{refId}', [\App\Http\Controllers\SchoolClassController::class, 'getSingleSchoolClassByRefId'])->name('school-classes.single');
        Route::get('school-classes/{refId}/students', [\App\Http\Controllers\SchoolClassController::class, 'getSchoolClassStudents'])->name('school-classes.students');
        Route::get('school-classes/{refId}/results', [\App\Http\Controllers\ResultController::class, 'getClassResults'])->name('school-classes.results');
        Route::get('school-classes/{refId}/subject-combos', [\App\Http\Controllers\SubjectCombinationController::class, 'getSchoolClassSubjectCombos'])->name('school-classes.subject-combos');
        Route::get('year-groups/all', [\App\Http\Controllers\YearGroupController::class, 'getAllSchoolYearGroup'])->name('year-groups');

        Route::get('subjects/all', [\App\Http\Controllers\SubjectController::class, 'getAllSubjects'])->name('subjects');
        Route::get('subjects/{refId}', [\App\Http\Controllers\SubjectController::class, 'getSingleSubject'])->name('subjects.single');
        Route::delete('subjects/{refId}/delete', [\App\Http\Controllers\SubjectController::class, 'deleteSubject'])->name('subjects.single.delete');
        Route::get('subjects/class/{classRefId}', [\App\Http\Controllers\SubjectController::class, 'getSubjectsForClass'])->name('subjects.class');

        Route::get('subject-combinations/all', [\App\Http\Controllers\SubjectCombinationController::class, 'getAllSubjectCombination'])->name('subject-combos');

        Route::get('students/all', [\App\Http\Controllers\StudentController::class, 'getAllStudents'])->name('students');
        Route::get('students/{uid}', [\App\Http\Controllers\StudentController::class, 'getSingleStudent'])->name('students.single');
        Route::get('students/{uid}/results', [\App\Http\Controllers\ResultController::class, 'getStudentResults'])->name('students.results');
        Route::get('students/{uid}/result-detail', [\App\Http\Controllers\ResultController::class, 'getStudentResultsDetails'])->name('students.results-detail');

        Route::get('notices', [\App\Http\Controllers\NoticeController::class, 'getAllNotices'])->name('notices');
        Route::get('notices/{refId}', [\App\Http\Controllers\NoticeController::class, 'getSingleNotice'])->name('notices.single');

        Route::get('/profile-data', [\App\Http\Controllers\ProfileController::class, 'getSchoolAdminProfileData'])->name('profile');
    });
});

require __DIR__.'/auth.php';
require __DIR__.'/backoffice.php';

