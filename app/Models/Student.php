<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Monolog\Handler\selectErrorStream;

class Student extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('belong_to_school', function (Builder $builder) {
            if (auth()->check()) {
                return $builder->whereHas('user', function ($query) {
                    $query->where('school_id', auth()->user()->school->id);
                });
            }
        });
    }

    public function plainPassword()
    {
        return $this->hasOne(StudentPassword::class, 'student_id');
    }

    public function results()
    {
        return $this->hasMany(Result::class, 'student_id');
    }

    public function scopeFindByStudentId($query, $studentId)
    {
        return $query->where('reg_no', $studentId);
    }

    public function scopeFindByUId($query, $uid)
    {
        return $query->where('unique_id', $uid)->first();
    }

    public function scratchCards()
    {
        return $this->hasMany(ScratchCard::class, 'student_id');
    }

    public function studentClass()
    {
        return $this->belongsTo(SchoolClass::class, 'school_class_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
