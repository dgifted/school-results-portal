<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScratchCard extends Model
{
    use HasFactory;

    const MAX_CARD_USAGE_COUNT = 5;

    protected $guarded = ['id'];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
//            ->where('student_id', '!==', null);
    }

    public function scopeExpired($query)
    {
        return $query->whereHas('student.user.school', function ($q) {
            $q->where('max_scratch_cards_usage', '>=', self::MAX_CARD_USAGE_COUNT);
        });
    }

    public function scopeFindByRefId($query, $ref)
    {
        return $query->where('ref_id', $ref)->first();
    }

    public function scopeNotExpired($query)
    {
        return $query->doesntHave('student')
            ->orWhereHas('student.user.school', function ($q) {
                $q->where('max_scratch_cards_usage', '<', self::MAX_CARD_USAGE_COUNT);
            });
    }
}
