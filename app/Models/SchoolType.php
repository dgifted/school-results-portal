<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function SebastianBergmann\Type\returnType;

class SchoolType extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'in-active';

    protected $guarded = ['id'];

    public function schools()
    {
        return $this->hasMany(School::class, 'school_type_id');
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public function scopeSecondarySchool($query)
    {
        return $query->where('name', 'secondary');
    }

    public function yearGroups()
    {
        return $this->hasMany(YearGroup::class,'school_type_id');
    }
}
