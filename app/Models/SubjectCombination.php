<?php

namespace App\Models;

use App\Services\GeneralService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubjectCombination extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $guarded = ['id'];

    protected $appends = [
        'status_text'
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('belong_to_school', function (Builder $builder) {
            if (auth()->check()) {
                $activeAdminSchool = auth()->user()->school;
                return $builder->whereHas('schoolClass', function ($query) use ($activeAdminSchool) {
                    return $query->where('school_id', $activeAdminSchool->id);
                });
            }
        });
    }

    public function getStatusTextAttribute()
    {
        return (int)$this->getAttribute('status') === (int)GeneralService::STATUS_ACTIVE
            ? 'active'
            : 'inactive';
    }

    public function school()
    {
        return $this->schoolClass()->school;
    }

    public function schoolClass()
    {
        return $this->belongsTo(SchoolClass::class, 'school_class_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
