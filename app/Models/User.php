<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Services\GeneralService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use function Illuminate\Process\whenEmpty;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const GENDERS = ['male', 'female'];

    protected $appends = [
        'status_text',
        'avatar_path'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'surname',
        'email',
        'password',
        'school_id',
        'gender',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function admin()
    {
        return $this->hasOne(Admin::class);
    }

    public function getAvatarPathAttribute()
    {
        if (!is_null($this->getAttribute('avatar'))) {
            return asset('avatars/'. $this->getAttribute('avatar'));
        }

        return asset('assets/images/avatar.png');
    }

    public function getStatusTextAttribute()
    {
        return (int)$this->getAttribute('status') === (int)GeneralService::STATUS_ACTIVE
            ? 'active'
            : 'inactive';
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id');
    }

    public function scopeAdmin($query)
    {
        return $query->whereHas('admin', function ($q) {
            return $q->where('user_id', self::getAttribute('id'));
        });
    }

    public function scopeNotAdmin($query)
    {
        return $query->doesntHave('admin');
    }

    public function student()
    {
        return $this->hasOne(Student::class);
    }
}
