<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use function Ramsey\Uuid\Guid\build;

class Subject extends Model
{
    use HasFactory;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('belong_to_school', function ($builder) {
            if (auth()->check()) {
                $activeAdminSchool = auth()->user()->school;
                return $builder->whereHas('schools', function ($query) use ($activeAdminSchool) {
                    return $query->where('school_id', $activeAdminSchool->id);
                });
            }

            return null;
        });
    }

    public function results()
    {
        return $this->hasMany(Result::class, 'subject_id');
    }

    public function schools()
    {
        return $this->belongsToMany(School::class, 'school_subject', 'subject_id', 'school_id');
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId)->first();
    }

    public function subjectClassCombinations()
    {
        return $this->hasMany(SubjectCombination::class);
    }
}
