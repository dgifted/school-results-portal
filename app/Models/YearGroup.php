<?php

namespace App\Models;

use App\Services\GeneralService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YearGroup extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $appends = [
        'status_text'
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('belong_to_school', function (Builder $builder) {
            if (auth()->check()) {
                return $builder->whereHas('schools', function ($query) {
                    return $query->where('school_id', auth()->user()->school->id);
                });
            }

            return null;
        });
    }

    public function classes()
    {
        return $this->hasMany(SchoolClass::class, 'year_group_id');
    }

    public function getStatusTextAttribute()
    {
        return (int)$this->getAttribute('status') === (int)GeneralService::STATUS_ACTIVE
            ? 'active' : 'in active';
    }

    public function schools()
    {
        return $this->belongsToMany(
            School::class,
            'school_year_group',
            'year_group_id',
            'school_id');
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId)->first();
    }

    public function schoolType()
    {
        return $this->belongsTo(SchoolType::class, 'school_type_id');
    }
}
