<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    use HasFactory;

    const PENDING_APPROVAL = 0;
    const APPROVED = 1;
    const NOT_APPROVED = 2;

    protected $guarded = ['id'];

    protected $appends = ['approval_status'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('belong_to_school', function (Builder $builder) {
            return $builder->where('school_id', auth()->user()->school->id);
        });
    }

    public function getApprovalStatusAttribute()
    {
        if ($this->getAttribute('approved') === self::PENDING_APPROVAL)
            return 'pending';
        if ($this->getAttribute('approved') === self::APPROVED)
            return 'approved';
        if ($this->getAttribute('approved') === self::NOT_APPROVED)
            return 'cancelled';
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id');
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId)->first();
    }
}
