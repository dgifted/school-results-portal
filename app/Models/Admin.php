<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class Admin extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function plainPassword()
    {
        return $this->hasOne(AdminPassword::class, 'admin_id');
    }

    public function school()
    {
        return $this->user()->school;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
