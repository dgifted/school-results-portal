<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class School extends Model
{
    use HasFactory;

    const IS_GHOST = 1;
    const IS_NOT_GHOST = 0;

    protected $guarded = ['id'];

    public function admins()
    {
        return $this->hasManyThrough(
            Admin::class,
            User::class
        );
    }

    protected $appends = ['abbreviation', 'banner_path', 'logo_path', 'short_name'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('not_ghost', function (Builder $builder) {
//            if (auth()->user()->roles->first()->id != Role::superAdmin()->first()->id)
            return $builder->where('is_ghost', strval(self::IS_NOT_GHOST));
        });

        static::deleting(function ($school) {
            if (!is_null($school->logo)) {
                if (Storage::disk('logo')->exists($school->logo)) {
                    Storage::disk('logo')->delete($school->logo);
                }
            }

            if (!is_null($school->banner)) {
                if (Storage::disk('banner')->exists($school->banner)) {
                    Storage::disk('banner')->delete($school->banner);
                }
            }
        });
    }

    public function classes()
    {
        return $this->hasMany(SchoolClass::class, 'school_id');
    }

    function getAbbreviationAttribute()
    {
        if ($this->getAttribute('is_ghost') == self::IS_GHOST)
            return 'GS';
        $words = explode(' ', $this->getAttribute('name'));
        $firstLetters = '';
        foreach ($words as $word) {
            $firstLetters .= substr($word, 0, 1);
        }

        return $firstLetters;
    }

    function getBannerPathAttribute()
    {
        return !is_null($this->getAttribute('logo'))
            ? asset('banners/' . $this->getAttribute('banner'))
            : asset('banners/default_school_banner.jpg');
    }

    function getLogoPathAttribute()
    {
        return !is_null($this->getAttribute('logo'))
            ? asset('logos/' . $this->getAttribute('logo'))
            : asset('logos/default_school_logo.png');
    }

    function getShortNameAttribute()
    {
        return trim(explode('[', $this->getAttribute('name'))[0]);
    }

    public function notices()
    {
        return $this->hasMany(Notice::class, 'school_id');
    }

    public function schoolType()
    {
        return $this->belongsTo(SchoolType::class, 'school_type_id');
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId)->firstOrFail();
    }

    public function subjects()
    {
        return $this->belongsToMany(
            Subject::class,
            'school_subject',
            'school_id',
            'subject_id');
    }

    public function scopeGhostSchools($query)
    {
        return $query->withoutGlobalScope('not_ghost')->where('is_ghost', strval(self::IS_GHOST));
    }

    public function scopeHasNoAdmin($query)
    {
        return $query->admins()->count() == 0;
    }

    public function users()
    {
        return $this->hasMany(User::class, 'school_id');
    }

    public function yearGroups()
    {
        return $this->belongsToMany(
            YearGroup::class,
            'school_year_group',
            'school_id',
            'year_group_id');
    }
}
