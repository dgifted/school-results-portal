<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('belong_to_school', function ($builder) {
            if (auth()->check()) {
                $activeAdminSchool = auth()->user()->school;
                return $builder->whereHas('schools', function ($query) use ($activeAdminSchool) {
                    return $query->where('school_id', $activeAdminSchool->id);
                });
            }

            return null;
        });
    }

    public function results()
    {
        return $this->hasMany(Result::class, 'school_class_id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id');
    }

    public function schools()
    {
        return $this->belongsTo(School::class, 'school_id');
    }

    public function scopeFindByRefId($query, $refId)
    {
        return $query->where('ref_id', $refId)->first();
    }

    public function subjectCombos()
    {
        return $this->hasMany(SubjectCombination::class, 'school_class_id');
    }

    public function students()
    {
        return $this->hasMany(Student::class, 'school_class_id');
    }

    public function yearGroup()
    {
        return $this->belongsTo(YearGroup::class, 'year_group_id');
    }
}
