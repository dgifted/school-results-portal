<?php

namespace App\Models;

use App\Services\ResultService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $appends = ['grade', 'remarks'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('belong_to_school', function (Builder $builder) {
            if (auth()->check()) {
                return $builder->whereHas('student.user', function ($query) {
                    return $query->where('school_id', auth()->user()->school->id);
                });
            }
            return null;
        });
    }

    public function getGradeAttribute()
    {
        return (new ResultService)->getGrade($this->getAttribute('marks'));
    }

    public function getRemarksAttribute()
    {
        $shortGrade = explode(' ', $this->grade)[0];
        switch ($shortGrade) {
            case 'A': return 'Very Good';
            case 'C': return 'Good';
            case 'P': return 'Poor';
            case 'F': return 'Very Poor';
            default : return '';
        }
    }

    public function studentClass()
    {
        return $this->belongsTo(SchoolClass::class, 'school_class_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
