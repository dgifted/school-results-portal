<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpFoundation\Response;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     * @return Response
     * @throws AuthenticationException
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->check()) {
            $isSuperAdmin = auth()->user()->roles->filter(fn ($role) => $role->name == 'super')->count() > 0;

            if ($isSuperAdmin) {
                return $next($request);
            }

            throw new UnauthorizedException('You are not allowed to access this page.');
        }

        throw new AuthenticationException('Please login first.');
    }
}
