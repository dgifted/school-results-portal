<?php

namespace App\Http\Controllers;

use App\Models\Notice;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class NoticeController extends Controller
{
    public function getAllNotices()
    {
        $notices = Notice::all();
        return response()->json($notices);
    }

    public function getSingleNotice($refId)
    {
        $notice = Notice::findByRefId($refId);

        if (!$notice)
            throw new ModelNotFoundException('Notice not found.');

        return response()->json($notice);
    }

    public function storeSchoolNotice(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string','max:160'],
            'content' => ['required', 'string']
        ]);

        auth()->user()->school->notices()->create($data);

        return back()->with([
            'message' => 'Notice added awaiting to be approved.',
            'type' => 'success'
        ]);
    }

    public function deleteSchoolNotice($refId)
    {
        $notice = Notice::findByRefId($refId);

        if (!$notice)
            throw  new ModelNotFoundException('Notice not found.');

        $notice->delete();

        return back()->with([
            'message' => 'Notice deleted successfully.',
            'type' => 'success',
        ]);
    }
}
