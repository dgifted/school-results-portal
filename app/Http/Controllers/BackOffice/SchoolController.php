<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\SchoolType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SchoolController extends Controller
{
    public function showAllSchoolsPage()
    {
        $schools = School::with('schoolType')->latest()->get();

        return view('backoffice.manage_schools')->with([
            'schools' => $schools
        ]);
    }

    public function showSchoolDetailsPage($schoolRefId)
    {
        $school = School::with([
            'users.admin',
            'users.student' => fn ($q) => $q->withoutGlobalScopes(),
            'users.student.studentClass' => fn ($q) => $q->withoutGlobalScopes(),
            'users.student.studentClass.yearGroup' => fn ($q) => $q->withoutGlobalScopes(),
            'users.student.results' => fn ($q) => $q->withoutGlobalScopes(),
            'users.student.results.subject' => fn ($q) => $q->withoutGlobalScopes(),
            'classes' => fn ($q) => $q->withoutGlobalScopes(),
            'classes.yearGroup' => fn ($q) => $q->withoutGlobalScopes(),
            'subjects' => fn ($q) => $q->withoutGlobalScopes(),
            'notices' => fn ($q) => $q->withoutGlobalScopes()
        ])->findByRefId($schoolRefId);

        return view('backoffice.school_detail')->with([
           'school' => $school
        ]);
    }

    public function showSchoolUpdatePage($schoolRefId)
    {
        $school = School::with('schoolType')->findByRefId($schoolRefId);
        $schoolTypes = SchoolType::all();

        return view('backoffice.edit_school')->with([
            'school' => $school,
            'schoolTypes' => $schoolTypes
        ]);
    }

    public function updateSchool(Request $request, $schoolRefId)
    {
        $school = School::with('schoolType')->findByRefId($schoolRefId);

        if ($request->has('name') && !!$request->get('name')) {
            $school->name = $request->get('name');
        }
        if ($request->has('address') && !!$request->get('address')) {
            $school->address = $request->get('address');
        }
        if ($request->has('type') && !!$request->get('type')) {
            $school->school_type_id = $request->get('type');
        }
        if ($request->hasFile('logo')) {
            if (Storage::disk('logo')->exists($school->logo)) {
                Storage::disk('logo')->delete($school->logo);
            }
            $school->logo = Storage::disk('logo')->putFile('', $request->file('logo'));
        }
        if ($request->hasFile('banner')) {
            if (Storage::disk('banner')->exists($school->banner)) {
                Storage::disk('banner')->delete($school->banner);
            }
            $school->banner = Storage::disk('banner')->putFile('', $request->file('banner'));
        }

        if (!$school->isDirty()) {
            return back()->with([
                'message' => 'School was not updated. None of the school\'s attribute changed.',
                'type' => 'info'
            ]);
        }

        $school->save();
        return back()->with([
            'message' => 'School was updated successfully.',
            'type' => 'success'
        ]);
    }
}
