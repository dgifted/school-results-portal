<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\SchoolType;
use App\Models\YearGroup;
use App\Services\GeneralService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class YearGroupController extends Controller
{
    public function showYearGroupsPage()
    {
        $yearGroups = YearGroup::withoutGlobalScopes()->with([
            'schoolType:id,name'
        ])->get();

        $schoolTypes = SchoolType::all();

        return view('backoffice.year_groups')
            ->with([
                'yearGroups' => $yearGroups,
                'schoolTypes' => $schoolTypes
            ]);
    }

    public function showSingleYearGroupPage()
    {

    }

    public function storeYearGroup(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string'],
            'short_code' => ['required', 'string'],
            'school_type' => ['required', 'numeric'],
        ]);

        $yG = YearGroup::create([
            'name' => $data['name'],
            'short_code' => $data['short_code'],
            'school_type_id' => $data['school_type'],
            'ref_id' => GeneralService::generateReferenceId()
        ]);

        if (!$yG)
            return back()->with([
                'message' => 'Could not add year group at this time.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Year group added successfully.',
            'type' => 'success'
        ]);
    }

    public function editYearGroup($yGRefId)
    {
        $yearGroup = YearGroup::withoutGlobalScopes()->findByRefId($yGRefId);
        $schoolTypes = SchoolType::all();

        if (!$yearGroup)
            throw  new ModelNotFoundException('YearGroup not found.');

        return view('backoffice.edit_year_group')->with([
            'yearGroup' => $yearGroup,
            'schoolTypes' => $schoolTypes,
        ]);
    }

    public function updateYearGroup(Request $request, $yGRefId)
    {
        $yearGroup = YearGroup::withoutGlobalScopes()->findByRefId($yGRefId);

        if (!$yearGroup)
            throw  new ModelNotFoundException('YearGroup not found.');

        if ($request->has('name') && !!$request->get('name'))
            $yearGroup->name = $request->get('name');

        if ($request->has('short_code') &&!!$request->get('short_code'))
            $yearGroup->short_code = $request->get('short_code');

        if ($request->has('school_type') &&!!$request->get('school_type'))
            $yearGroup->school_type_id = $request->get('school_type');

        if(!$yearGroup->isDirty())
            return back()->with([
               'message' => 'Year group was not updated as none of its attributes were changed.',
                'type' => 'warning',
            ]);

        $yearGroup->save();

        return back()->with([
            'message' => 'Year group updated successfully.',
            'type' => 'success'
        ]);
    }

//    public function getYearGroupClasses($yearGroupRefId)
//    {
//        $yearGroup = YearGroup::with('classes')
//    }
}
