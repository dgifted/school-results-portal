<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Subject;
use Illuminate\Http\Request;
use function Symfony\Component\Mime\Header\all;

class DashboardController extends Controller
{
    public function getDashboardPayload()
    {
        $schools = School::latest()->get();
    }

    public function showDashboardPage()
    {
        $schools = School::withoutGlobalScope('belong_to_school')
            ->with([
                'users.admin',
                'users.student' => fn ($query) => $query->withoutGlobalScopes(),
                'classes' => fn ($query) => $query->withoutGlobalScopes()
            ])
            ->latest()
            ->get();

        $subjects = Subject::withoutGlobalScope('belong_to_school')->get();
        $allUsersCount = 0;
        $allClassesCount = 0;
        $schools->each(function ($school) use (&$allClassesCount, &$allUsersCount) {
            $allClassesCount += $school->classes->count();
            $allUsersCount += $school->users->count();
        });

        return view('backoffice.index')
            ->with([
                'schools' => $schools,
                'subjects' => $subjects,
                'allUsersCount' => $allUsersCount,
                'allClassesCount' => $allClassesCount,
            ]);
    }
}
