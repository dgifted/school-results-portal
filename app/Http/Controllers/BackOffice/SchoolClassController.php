<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\SchoolClass;
use App\Models\SchoolType;
use App\Models\YearGroup;
use App\Services\GeneralService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchoolClassController extends Controller
{
    public function getAllClassesForSchool($schoolRefId)
    {
        $school = School::with([
            'classes' => fn($q) => $q->withoutGlobalScopes(),
            'classes.yearGroup'
        ])->findByRefId($schoolRefId);

        return response()->json($school->classes);
    }

    public function showClassesPage()
    {
        $schools = School::with([
            'classes' => fn($q) => $q->withoutGlobalScopes(),
            'classes.yearGroup' => fn($q) => $q->withoutGlobalScopes(),
        ])->get();

        $schoolTypes = SchoolType::all();

        return view('backoffice.classes')->with([
            'schools' => $schools,
            'schoolTypes' => $schoolTypes,
        ]);
    }

    public function showAddClassPage($schoolRefId)
    {
        $school = School::withoutGlobalScopes()->findByRefId($schoolRefId);
        $yearGroups = YearGroup::withoutGlobalScopes()->get();

        return view('backoffice.add_class')->with([
            'school' => $school,
            'yearGroups' => $yearGroups
        ]);
    }

    public function storeClass(Request $request, $schoolRefId)
    {
        $school = School::withoutGlobalScopes()->findByRefId($schoolRefId);

        $data = $request->validate([
            'year_group' => ['required', 'numeric'],
            'name' => ['required', 'string', 'max:10'],
            'alias' => ['nullable', 'string', 'max:20'],
        ]);

        unset($request);

        DB::beginTransaction();
        try {
            $school->classes()->create([
                'year_group_id' => $data['year_group'],
                'name' => $data['name'],
                'alias' => $data['alias'],
                'ref_id' => GeneralService::generateReferenceId()
            ]);

            DB::commit();

            return back()->with([
                'message' => 'Class added successfully.',
                'type' => 'success',
            ]);
        } catch (\Throwable $e) {
            report($e);

            DB::rollBack();

            return back()->with([
                'message' => 'Class could not be added.',
                'type' => 'danger',
            ]);
        }

    }
}
