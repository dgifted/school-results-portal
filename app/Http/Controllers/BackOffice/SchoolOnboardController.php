<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\School;
use App\Models\SchoolType;
use App\Models\User;
use App\Models\YearGroup;
use App\Services\GeneralService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Psy\Util\Str;

class SchoolOnboardController extends Controller
{
    public function showOnboardPage()
    {
        $schoolTYpe = SchoolType::active()->get();
        $schools = School::with('schoolType')->latest()->get()->take(10);

        return view('backoffice.school_onboard')
            ->with([
                'schoolType' => $schoolTYpe,
                'schools' => $schools
            ]);
    }

    public function storeSchool(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'address' => ['nullable', 'string', 'max:255'],
            'logo' => ['nullable', 'file', 'max:200'],
            'banner' => ['nullable', 'file', 'max:1024'],
            'type' => ['required'],
        ]);

        if (!is_null($data['logo'])) {
            $data['logo'] = Storage::disk('logo')->putFile('', $data['logo']);
        }

        if (!is_null($data['banner'])) {
            $data['banner'] = Storage::disk('banner')->putFile('', $data['banner']);
        }

        DB::beginTransaction();
        try {
            $school = School::create([
                'name' => $data['name'],
                'address' => $data['address'],
                'school_type_id' => $data['type'],
                'logo' => $data['logo'],
                'banner' => $data['banner'],
                'ref_id' => GeneralService::generateReferenceId()
            ]);

            $yearGroupsId = YearGroup::withoutGlobalScopes()->get()->pluck('id')->toArray();

            $school->yearGroups()->attach($yearGroupsId);

            DB::commit();

            return back()->with([
                'message' => 'School added successfully. Proceed to creating admin account for the school <a href="' . route('backoffice.assign-admin', $school->ref_id) . '" class="btn btn-info btn-xs">Here</a>',
                'type' => 'success',
            ]);
        } catch (\Throwable $exception) {
            report($exception);
            DB::rollBack();

            return back()->with([
                'message' => 'Could not add school to database.',
                'type' => 'danger'
            ]);
        }
    }

    public function assignAdminToSchool($schoolRefId)
    {
        $school = School::with([
            'users' => fn ($q) => $q->has('admin'),
            'users.admin.plainPassword',
            'users.roles'])->findByRefId($schoolRefId);

        if (!$school)
            throw new ModelNotFoundException('School not found.');

        return view('backoffice.school_assign_admin')->with([
            'school' => $school
        ]);
    }

    public function storeSchoolAdmin(Request $request, $schoolRefId)
    {
        $school = School::findByRefId($schoolRefId);

        if (!$school)
            throw new ModelNotFoundException('School not found.');

        $data = $request->validate([
            'first_name' => ['required', 'string', 'max:20'],
            'surname' => ['required', 'string', 'max:20'],
            'email' => ['required', 'string', 'email', 'max:30', 'unique:users'],
            'password' => ['nullable', 'string', 'min:8'],
            'username' => ['required', 'string', 'unique:admins'],
            'gender' => ['nullable', 'string'],
            'passport' => ['nullable', 'file', 'max:200'],
        ]);

        DB::beginTransaction();
        try {
            if (!is_null($data['passport'])) {
                $data['avatar'] = Storage::disk('avatar')->putFile('', $data['passport']);
                unset($data['passport']);
            }

            if (!is_null($data['password'])) {
                $plainPassword = $data['password'];
                $data['password'] = Hash::make($data['password']);
            } else {
                $plainPassword = \Illuminate\Support\Str::random(8);
                $data['password'] = Hash::make($plainPassword);
            }

            $user = User::create([
                'first_name' => $data['first_name'],
                'surname' => $data['surname'],
                'email' => $data['email'],
                'password' => $data['password'],
                'gender' => $data['gender'],
                'school_id' => $school->id,
                'avatar' => $data['avatar'],
                'email_verified_at' => now()
            ]);

            $admin = $user->admin()->create([
                'first_name' => $data['first_name'],
                'last_name' => $data['surname'],
                'username' => $data['username'],
            ]);
            $admin->plainPassword()->create([
                'plain_text' => $plainPassword
            ]);
            $adminRole = Role::admin()->first();
            $user->roles()->attach($adminRole->id);

            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            report($exception);

            return back()->with([
                'message' => 'Could not add user to database.',
                'type' => 'danger'
            ]);
        }

        return back()->with([
            'message' => 'Admin successfully created for ' . $school->name,
            'type' => 'success'
        ]);
    }
}
