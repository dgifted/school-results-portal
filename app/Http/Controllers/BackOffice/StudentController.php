<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Services\StudentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    public function regenerateStudentsId($schoolRefId)
    {
        $school = School::with([
            'users' => fn($q) => $q->withoutGlobalScopes(),
            'users.student' => fn($q) => $q->withoutGlobalScopes(),
        ])->findByRefId($schoolRefId);

        DB::beginTransaction();
        try {
            foreach ($school->users as $user) {
                if (!is_null($user->student)) {
                    $user->student->withoutGlobalScopes()->update([
                        'reg_no' => StudentService::generateStudentId($user->student)
                    ]);
                }
            }
            DB::commit();
            return back()->with([
                'message' => 'Students IDs have been updated.',
                'type' => 'success'
            ]);
        } catch (\Exception $e) {
            report($e);
            DB::rollBack();
//            dd($e);
            return back()->with([
                'message' => 'Student IDs were not able to be updated. Please try again later.',
                'type' => 'danger'
            ]);
        }
    }
}
