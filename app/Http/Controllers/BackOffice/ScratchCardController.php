<?php

namespace App\Http\Controllers\BackOffice;

use App\Http\Controllers\Controller;
use App\Models\ScratchCard;
use App\Services\GeneralService;
use Illuminate\Http\Request;

class ScratchCardController extends Controller
{
    public function showScratchCardPage()
    {
        $scratchCards = ScratchCard::with([
            'student' => fn($q) => $q->withoutGlobalScopes(),
        ])->get();
        return view('backoffice.scratch-card-mgt')
            ->with('scratchCards', $scratchCards);
    }

    public function generateScratchCards()
    {
        if (!request()->input('quantity')) {
            return response()->json([
                'message' => 'Please enter a quantity above zero.',
                'type' => 'warning',
            ], 300);
        }
        $quantity = (int)\request()->input('quantity');

        //TODO: Move this block into a job class.
        $error = null;
        for ($i = 0; $i < $quantity; $i++) {
            try {
                ScratchCard::create([
                    'pin' => random_int(1000, 9999) .
                        '-' . random_int(1000, 9999) .
                        '-' . random_int(1000, 9999) .
                        '-' . random_int(1000, 9999),
                    'ref_id' => GeneralService::generateReferenceId()
                ]);

            } catch (\Exception $e) {
                report($e);
                $error = $e->getMessage();
            }
        }

        if (!is_null($error)) {
            return response()->json([
                'message' => $error,
                'type' => 'danger'
            ]);
        }

        return response()->json([
            'message' => 'Scratch cards generated successfully',
            'type' => 'success'
        ]);
    }

    public function deleteScratchCard($cardRedId) {
        $card = ScratchCard::findByRefId($cardRedId);

        if (!$card)
            return response()->json([
                'message' => 'Card not found',
                'type' => 'info'
            ]);

        $card->delete();

        return response()->json([
            'message' => 'Card deleted successfully.',
            'type' => 'success'
        ]);
    }
}
