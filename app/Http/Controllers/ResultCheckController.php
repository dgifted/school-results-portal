<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\SchoolClass;
use App\Models\ScratchCard;
use App\Models\Student;
use App\Services\ResultService;
use Illuminate\Http\Request;

class ResultCheckController extends Controller
{
    public function checkResult(Request $request)
    {
        $data = $request->validate([
            'rollId' => ['required', 'string'],
            'cardPin' => ['required', 'string'],
        ]);

        $targetStudent = Student::findByStudentId($data['rollId'])->first();

        if (!$targetStudent) {
            return back()->with([
                'message' => 'No student with the given ID was found.',
                'type' => 'danger'
            ]);
        }

        if ($targetStudent->results->count() == 0)
            return back()->with([
                'message' => 'You have no declared results. Check back later.',
                'type' => 'info'
            ]);

        $cardWithProvidedPin = ScratchCard::with('student')->where('pin', '=', $data['cardPin'])->first();

        if (!$cardWithProvidedPin) {
            return back()->with([
                'message' => 'Incorrect card PIN.',
                'type' => 'danger'
            ]);
        }

        if (!is_null($cardWithProvidedPin->student)) {
            if ((int)$cardWithProvidedPin->student->id !== (int)$targetStudent->id) {
                return back()->with([
                    'message' => 'Card has already been used by another student.',
                    'type' => 'danger'
                ]);
            }
        }

        if ($cardWithProvidedPin->usage_count >= ScratchCard::MAX_CARD_USAGE_COUNT) {
            return back()->with([
                'message' => 'Card usage limit of ' . ScratchCard::MAX_CARD_USAGE_COUNT . ' has been exceeded. Please purchase a new card.',
                'type' => 'warning'
            ]);
        }

        $student = $resultPayload = $this->getStudentResults($targetStudent, $cardWithProvidedPin);

        return view('landing.result-check-details')->with([
            'student' => $student,
        ]);
    }

    private function getStudentResults($student, $scratchCard)
    {
        // TODO: replace query with laravel "load" method instead of making a fresh request.
        $studentWithResultPayload = Student::with(['results.subject', 'studentClass.yearGroup', 'user.school'])
            ->findByUId($student->unique_id);

        $studentWithResultPayload->position = (new ResultService)->getStudentPosition(
            $studentWithResultPayload->studentClass,
            $studentWithResultPayload
        );

        $studentsInClass = $studentWithResultPayload->studentClass->students->count();
        $studentWithResultPayload->classSize = $studentsInClass;

        $scratchCard->usage_count += 1;
        if (is_null($scratchCard->student_id))
            $scratchCard->student_id = $student->id;
        $scratchCard->save();

        return $studentWithResultPayload;
    }
}
