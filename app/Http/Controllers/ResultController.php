<?php

namespace App\Http\Controllers;

use App\Models\Result;
use App\Models\SchoolClass;
use App\Models\Student;
use App\Services\ResultService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function getStudentResults($studentUId)
    {
        $student = Student::with('results.subject')->findByUId($studentUId);

        if (!$student)
            throw new ModelNotFoundException('Student not found.');

        return response()->json($student->results);
    }

    public function getClassResults($classRefId)
    {
        $class = SchoolClass::with('students.results')->findByRefId($classRefId);

        if (!$class)
            throw new ModelNotFoundException('Class not found.');

        return response()->json($class->students);
    }

    public function getStudentResultsDetails($studentUId)
    {
        $student = Student::with(['results.subject', 'studentClass.yearGroup', 'user.school'])
            ->findByUId($studentUId);
        $student->position = (new ResultService)->getStudentPosition($student->studentClass, $student);
        $studentsInClass = $student->studentClass->students->count();
        $student->classSize = $studentsInClass;

        if (!$student)
            throw new ModelNotFoundException('Student not found.');

        return response()->json($student);
    }

    public function storeStudentResult(Request $request, $studentUId)
    {
        $student = Student::with(['studentClass', 'results'])->findByUId($studentUId);
        if (!$student)
            throw new ModelNotFoundException('Student not found.');

        if ($student->results->count() > 0)
            return back()->with([
                'message' => 'Result has already been declared for ' . $student->first_name . ' ' . $student->surname,
                'type' => 'warning'
            ]);

        foreach ($request->all() as $input => $value) {
            if (is_numeric($input))
                Result::create([
                    'student_id' => $student->id,
                    'school_class_id' => $student->studentClass->id,
                    'subject_id' => $input,
                    'marks' => $value
                ]);
        }

        return back()->with([
            'message' => 'Result declared for ' . $student->first_name . ' ' . $student->surname,
            'type' => 'success'
        ]);
    }

    public function updateStudentResult(Request $request, $studentUId)
    {
        $student = Student::with(['results'])->findByUId($studentUId);

        if (!$student)
            throw new ModelNotFoundException('Student not found.');


        foreach ($request->all() as $input => $value) {
            if (is_numeric($input)) {
                $result = Result::find($input);
                if (!is_null($result) && !is_null($value)) {
                    $result->marks = $value;
                    $result->save();
                }
            }
        }

        return back()->with([
            'message' => 'Result updated for ' . $student->first_name . '' . $student->surname,
            'type' => 'success'
        ]);
    }
}
