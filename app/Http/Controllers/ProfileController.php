<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function getSchoolAdminProfileData()
    {
        $adminData = User::with('admin')->findOrFail(auth()->id());
        return response()->json($adminData);
    }

    public function updateAdminProfile(Request $request)
    {
        $adminUser = User::with('admin')->findOrFail(\auth()->id());

        if ($request->has('first_name') && !!$request->get('first_name')) {
            $adminUser->first_name = $request->get('first_name');
            $adminUser->admin->first_name = $request->get('first_name');

        }
        if ($request->has('surname') && !!$request->get('surname')) {
            $adminUser->surname = $request->get('surname');
            $adminUser->admin->last_name = $request->get('surname');
        }
        if ($request->has('email') && !!$request->get('email')) {
            $adminUser->email = $request->get('email');
        }
        if ($request->has('username') && !!$request->get('username')) {
            $adminUser->admin->username = $request->get('username');
        }
        if ($request->has('gender') && !!$request->get('gender')) {
            $adminUser->gender = $request->get('gender');
        }
        if ($request->hasFile('avatar')) {
            if (!is_null($adminUser->avatar)) {
                if (Storage::disk('avatar')->exists($adminUser->avatar)) {
                    Storage::disk('avatar')->delete($adminUser->avatar);
                }
            }
            $adminUser->avatar = Storage::disk('avatar')->putFile('', $request->file('avatar'));
        }

        if (!$adminUser->isDirty() && !$adminUser->admin->isDirty()) {
            return back()->with([
                'message' => 'No changes were made.',
                'type' => 'info'
            ]);
        }

        $adminUser->save();
        $adminUser->admin->save();

        return back()->with([
            'message' => 'Profile updated successfully.',
            'type' => 'success'
        ]);
    }

    public function changePassword(Request $request)
    {
        $data = $request->validate([
            'old_password' => ['required'],
            'new_password' => ['required', 'min:8'],
            'new_password_confirmation' => ['required']
        ]);

        $user = auth()->user();
        //Check if old password is valid
        if (!Hash::check($data['old_password'], $user->password)) {
            return back()->with([
                'message' => 'Current password is incorrect.',
                'type' => 'danger'
            ]);
        }

        if ($data['new_password'] !== $data['new_password_confirmation']) {
            return back()->with([
                'message' => 'Passwords do not match.',
                'type' => 'warning'
            ]);
        }

        $user->password = Hash::make($data['new_password']);
        $user->save();

        return back()->with([
            'message' => 'Password changed.',
            'type' => 'success'
        ]);
    }
}
