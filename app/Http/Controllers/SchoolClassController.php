<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSchoolClassRequest;
use App\Imports\SchoolClassesImport;
use App\Models\SchoolClass;
use App\Rules\SpreadsheetFile;
use App\Services\GeneralService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class SchoolClassController extends Controller
{
    public function getAllSchoolClass()
    {
        $classes = SchoolClass::with(['yearGroup'])->withCount('students')->get();
        return response()->json($classes, 200);
    }

    public function storeSchoolClass(StoreSchoolClassRequest $request)
    {
        $data = $request->validated();
        $school = auth()->user()->school;

        $classes = $school->classes()->with('yearGroup')->get();
        $classEntryExist = $this->checkClassEntryExist($classes, $data);
        if ($classEntryExist) {
            return back()->with([
                'message' => 'Class entry already exists.',
                'type' => 'warning'
            ]);
        }

        $data['ref_id'] = GeneralService::generateReferenceId();
        $newSchoolClass = $school->classes()->create($data);

        if (!$newSchoolClass)
            return back()->with([
                'message' => 'Could not create new class. Please try again.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Class created successfully.',
            'type' => 'success'
        ]);
    }


    public function bulkStudentClassImport(Request $request)
    {
        $data = $request->validate([
            'file' => ['required', new SpreadsheetFile]
        ]);

        DB::beginTransaction();
        try {
            //Delete already existing classes
            auth()->user()->school->classes->each(function ($class) {
                $class->delete();
            });

            Excel::import(new SchoolClassesImport, $data['file']);
            DB::commit();
            return back()->with([
                'message' => 'Classes imported successfully.',
                'type' => 'success'
            ]);

        } catch (\Throwable $e) {
            report($e);
            DB::rollBack();
            return back()->with([
                'message' => 'Classes could not be imported. Please try again.',
                'type' => 'danger'
            ]);
        }
    }

    public function getSingleSchoolClassByRefId($refId)
    {
        $class = SchoolClass::with('yearGroup')->findByRefId($refId);

        if (!$class)
            throw new ModelNotFoundException('Class not found.');

        return response()->json($class, 200);
    }

    public function getSchoolClassStudents($classRefId)
    {
        $class = SchoolClass::with('students')->findByRefId($classRefId);

        if (!$class)
            throw new ModelNotFoundException('Class not found.');

        return response()->json($class->students);
    }

    public function updateSchoolClass(Request $request, $refId)
    {
        $data = $request->validate([
            'year_group_id' => ['required', 'numeric'],
            'name' => ['required'],
            'alias' => ['nullable', 'string']
        ]);

        $classes = auth()->user()->school->classes;

        $classEntryExist = $this->checkClassEntryExist($classes, $data);

        if ($classEntryExist) {
            return back()->with([
                'message' => 'Class entry already exists.',
                'type' => 'warning'
            ]);
        }

        $class = SchoolClass::findByRefId($refId);

        if (!$class)
            return back()->with([
                'message' => 'Class not found. ',
                'type' => 'danger'
            ]);

        $class->year_group_id = $data['year_group_id'];
        $class->name = $data['name'];
        $class->alias = $data['alias'];
        $class->save();

        return back()->with([
            'message' => 'Class updated successfully.',
            'type' => 'success'
        ]);
    }

    private function checkClassEntryExist($classes, $data)
    {
        $classEntryExist = false;
        $classes->each(function ($class) use (&$classEntryExist, $data) {
            if (Str::lower($data['name']) == Str::lower($class->name) && (int)$data['year_group_id'] === (int)$class->yearGroup->id) {
                $classEntryExist = true;
            }
        });

        return $classEntryExist;
    }
}
