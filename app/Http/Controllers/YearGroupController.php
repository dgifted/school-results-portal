<?php

namespace App\Http\Controllers;

use App\Models\YearGroup;
use Illuminate\Http\Request;

class YearGroupController extends Controller
{
    public function getAllSchoolYearGroup()
    {
        $yearGroups = YearGroup::with('classes')->get();
        return response()->json($yearGroups, 200);
    }
}
