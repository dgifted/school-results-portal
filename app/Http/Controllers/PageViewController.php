<?php

namespace App\Http\Controllers;

class PageViewController extends Controller
{
    public function landingPage()
    {
        return view('landing.index_modded');
    }

    public function dashboard()
    {
        return view('dashboard.index');
    }

    public function addClasses()
    {
        return view('dashboard.add-classes');
    }

    public function editClass($refId)
    {
        return view('dashboard.edit-classes');
    }

    public function manageClasses()
    {
        return view('dashboard.manage-classes');
    }

    public function manageYearGroup()
    {
        return view('dashboard.manage-year-groups');
    }

    public function createSubject()
    {
        return view('dashboard.create-subject');
    }

    public function manageSubject()
    {
        return view('dashboard.manage-subject');
    }

    public function addSubjectCombination()
    {
        return view('dashboard.add-subject-comb');
    }

    public function manageSubjectCombination()
    {
        return view('dashboard.manage-subject-comb');
    }

    public function addStudent()
    {
        return view('dashboard.add-student');
    }

    public function editStudent($uid)
    {
        return view('dashboard.edit-student');
    }

    public function manageStudents()
    {
        return view('dashboard.manage-student');
    }

    public function studentDetails($uid)
    {
        return view('dashboard.student-detail');
    }

    public function addResult()
    {
        return view('dashboard.add-result');
    }

    public function editStudentResult($studentUId)
    {
        return view('dashboard.edit-result');
    }

    public function viewStudentResult($studentUId)
    {
        return view('dashboard.student-result-details');
    }

    public function manageResult()
    {
        return view('dashboard.manage-results');
    }

    public function addNotice()
    {
        return view('dashboard.add-notice');
    }

    public function manageNotice()
    {
        return view('dashboard.manage-notice');
    }

    public function noticeDetails($refId)
    {
        return view('dashboard.notice-detail');
    }

    public function help()
    {
        return view('dashboard.help');
    }

    public function profile()
    {
        return view('dashboard.profile');
    }

    public function checkResult()
    {
        return view('landing.result-check');
    }

    public function studentResultDetailsPage()
    {
        return view('landing.result-check-details');
    }

    public function schoolSignupPage()
    {
        return view('landing.signup_modded');
    }
}
