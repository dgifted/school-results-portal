<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentStoreRequest;
use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StudentController extends Controller
{
    public function getAllStudents()
    {
        $students = Student::with(['studentClass.yearGroup', 'user'])->get();
        return response()->json($students);
    }

    public function getSingleStudent($uId)
    {
        $student = Student::with([
            'user',
            'studentClass.yearGroup',
            'studentClass.subjectCombos' => function ($query) {
                $query->has('subject');
            },
            'studentClass.subjectCombos.subject'
        ])->findByUId($uId);

        if (!$student)
            throw new ModelNotFoundException('Student not found.');

        return response()->json($student);
    }

    public function storeStudent(StudentStoreRequest $request)
    {
        $data = $request->validated();
        $passwordString = Str::random(10);

        DB::beginTransaction();
        try {
            $user = User::create([
                'school_id' => auth()->user()->school->id,
                'first_name' => $data['first_name'],
                'surname' => $data['surname'],
                'email' => $data['email'],
                'gender' => $data['gender'],
                'password' => Hash::make($passwordString)
            ]);

            if (!$user)
                throw new \Exception('Error adding student. Please try again later. ', 500);

            $student = $user->student()->create([
                'unique_id' => Str::uuid(),
                'school_class_id' => $data['school_class_id'],
                'first_name' => $data['first_name'],
                'surname' => $data['surname'],
                'reg_no' => null,
                'roll_id' => random_int(1, 100),
                'dob' => $data['dob']
            ]);

            $student->plainPassword()->create([
                'plain_text' => $passwordString
            ]);

            DB::commit();

            return back()->with([
                'message' => 'Student added successfully.',
                'type' => 'success'
            ]);
        } catch (\Throwable $exception) {
            report($exception);
            DB::rollBack();
        }

        return back()->with([
            'message' => 'Could not add student',
            'type' => 'danger'
        ]);
    }

    public function updateStudent(Request $request, $uid)
    {
        $student = Student::findByUId($uid);

        if (!$student)
            throw new ModelNotFoundException('Student not found.');

        $user = $student->user;

        DB::beginTransaction();
        try {
            if ($request->has('first_name') && !is_null($request->get('first_name'))) {
                $student->first_name = $request->get('first_name');
                $user->first_name = $request->get('first_name');
            }

            if ($request->has('surname') && !is_null($request->get('surname'))) {
                $student->surname = $request->get('surname');
                $user->surname = $request->get('surname');
            }

            if ($request->has('email') && !is_null($request->get('email')))
                $user->email = $request->get('email');

            if ($request->has('roll_id') && !is_null($request->get('roll_id')))
                $student->roll_id = $request->get('roll_id');

            if ($request->has('dob') && !is_null($request->get('dob')))
                $student->dob = $request->get('dob');

            if ($request->has('gender') && !is_null($request->get('gender')))
                $user->gender = $request->get('gender');

            if ($request->has('school_class_id') && !is_null($request->get('school_class_id')))
                $student->school_class_id = $request->get('school_class_id');

            if (!$user->isDirty() && !$student->isDirty())
                return back()->with([
                    'message' => 'Student not modified. No attribute was changed.',
                    'type' => 'info'
                ]);

            if ($user->isDirty())
                $user->save();
            if ($student->isDirty())
                $student->save();

            DB::commit();
            return back()->with([
                'message' => 'Student information successfully updated.',
                'type' => 'success'
            ]);
        } catch (\Throwable $exception) {
            report($exception);
            DB::rollBack();
        }

        return back()->with([
            'message' => 'An unexpected error occurred. Please try again.',
            'type' => 'danger'
        ]);
    }

    public function uploadStudentPassport(Request $request, $studentUid)
    {
        $student = Student::with('user')->findByUId($studentUid);

        if (!$student)
            throw new ModelNotFoundException('Student not found.');

        $request->validate([
            'passport' => ['required', 'file', 'max:300']
        ]);

        if (!$request->hasFile('passport'))
            return back()->with([
                'message' => 'Please choose a valid image file.',
                'type' => 'danger'
            ]);

        try {
            if (!is_null($student->user->avatar)) {
                if (Storage::disk('avatar')->exists($student->user->avatar)) {
                    Storage::disk('avatar')->delete($student->user->avatar);
                }
            }

            $student->user->update([
                'avatar' => Storage::disk('avatar')->putFile('', $request->file('passport'))
            ]);

            return back()->with([
                'message' => 'Passport uploaded successfully.',
                'type' => 'success'
            ]);
        } catch (\Throwable $e) {
            report($e);
            return back()->with([
                'message' => 'Passport upload failed.',
                'type' => 'danger'
            ]);
        }
    }
}
