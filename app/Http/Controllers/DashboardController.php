<?php

namespace App\Http\Controllers;

use App\Models\Result;
use App\Models\SchoolClass;
use App\Models\Student;
use App\Models\Subject;
use App\Traits\Responsable;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getDashboardData()
    {
        $totalStudents = Student::query()->count();
        $totalSubjects = Subject::query()->count();
        $totalClasses = SchoolClass::query()->count();
        $totalDeclaredResults = Result::query()->count();

        $recentDeclaredResults = Result::with(['student.studentClass.yearGroup'])
            ->latest()
            ->get()
            ->take(10);

        $recentDeclaredResultsGrouped = $recentDeclaredResults->groupBy('student');

        return response()->json([
            'students'=> $totalStudents,
            'subjects' => $totalSubjects,
            'classes' => $totalClasses,
            'declaredResults' => $totalDeclaredResults,
            'declaredResultsList' => $recentDeclaredResults,
            'groupedResults' => $recentDeclaredResultsGrouped
        ], 200);
    }
}
