<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubjectStoreRequest;
use App\Imports\SchoolClassesImport;
use App\Imports\SubjectsImport;
use App\Models\SchoolClass;
use App\Models\Subject;
use App\Rules\SpreadsheetFile;
use App\Services\GeneralService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;

class SubjectController extends Controller
{
    public function getAllSubjects()
    {
        $subjects = Subject::all()
            ->each(function ($subject) {
                $subject->checked = false;
            });
        return response()->json($subjects);
    }

    public function getSingleSubject($refId)
    {
        $subject = Subject::findByRefId($refId);

        if (!$subject)
            throw new ModelNotFoundException('Subject not found.');

        return response()->json($subject);
    }

    public function storeSubject(SubjectStoreRequest $request)
    {
        $data = $request->validated();

        if ($this->checkSubjectExists($data['name']))
            return back()->with([
                'message' => 'Subject already exists',
                'type' => 'warning'
            ]);

        $newSubject = Subject::create([
            'ref_id' => GeneralService::generateReferenceId(),
            'name' => $data['name'],
            'code' => $data['code']
        ]);

        if (!$newSubject)
            return back()->with([
                'message' => 'Could not add subject. Please try again.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Subject added successfully.',
            'type' => 'success'
        ]);

    }

    public function bulkSubjectsImport(Request $request)
    {
        $data = $request->validate([
            'file' => ['required', new SpreadsheetFile]
        ]);

        DB::beginTransaction();
        try {
            auth()->user()->school->subjects->each(function ($subject) {
                $subject->delete();
            });

            Excel::import(new SubjectsImport, $data['file']);
            DB::commit();
            return back()->with([
                'message' => 'Subjects imported successfully.',
                'type' => 'success'
            ]);
        } catch (\Throwable $e) {
            report($e);
            DB::rollBack();
            return back()->with([
                'message' => 'Subjects could not be imported. Please try again.',
                'type' => 'danger'
            ]);
        }
    }

    public function getSubjectsForClass($classRefId)
    {
        $class = SchoolClass::with('subjectCombos.subject')->findByRefId($classRefId);

        if (!$class)
            throw new ModelNotFoundException('Class not found.');

        return response()->json($class->subjectCombos);
    }

    public function deleteSubject($refId)
    {
        $subject = Subject::with(['results', 'subjectClassCombinations'])->findByRefId($refId);

        if (!$subject)
            throw new ModelNotFoundException('Subject not found.');

        DB::beginTransaction();
        try {
            $subject->results->each(function ($result) {
                $result->delete();
            });
            $subject->subjectClassCombinations->each(function ($combo) {
                $combo->delete();
            });
            $subject->delete();
            DB::commit();
            return back()->with([
                'message' => 'Subject deleted.',
                'type' => 'success'
            ]);
        } catch (\Throwable $e) {
            report($e);
            DB::rollback();

            return back()->with([
                'message' => 'Subject could not be deleted.',
                'type' => 'danger'
            ]);
        }


    }

    private function checkSubjectExists($subName)
    {
        $subjectFound = false;
        $subs = Subject::all()->pluck('name')->toArray();
        foreach ($subs as $sub) {
            if (stripos($sub, $subName) !== false)
                $subjectFound = true;
        }

        return $subjectFound;
    }
}
