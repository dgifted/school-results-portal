<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubjectCombinationStoreRequest;
use App\Models\SchoolClass;
use App\Models\SubjectCombination;
use App\Services\GeneralService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class SubjectCombinationController extends Controller
{
    public function getAllSubjectCombination()
    {
        $subCombos = SubjectCombination::has('subject')->with(['schoolClass.yearGroup', 'subject'])->get();
        return response()->json($subCombos);
    }

    public function storeSubjectCombination(SubjectCombinationStoreRequest $request)
    {
        $data = $request->validated();

        DB::beginTransaction();
        $schoolClass = SchoolClass::with(['subjectCombos'])->where('id', intval($data['school_class_id']))->first();
        $subCombosArray = !!$schoolClass ? $schoolClass->subjectCombos->pluck('subject_id')->toArray() : array();

        try {
            foreach ($data['subjects'] as $subject_id) {
                if (!array_search($subject_id, $subCombosArray))
                    SubjectCombination::create([
                        'school_class_id' => $data['school_class_id'],
                        'subject_id' => $subject_id
                    ]);
            }
            DB::commit();

            return back()->with([
                'message' => 'Subject combinations added successfully.',
                'type' => 'success',
            ]);
        } catch (\Throwable $e) {
            report($e);
            DB::rollBack();

            return back()->with([
                'message' => 'Subject combination could not be added. Please contact the administrator.',
                'type' => 'danger'
            ]);
        }
    }

    public function getSchoolClassSubjectCombos($classId)
    {
        $schoolClass = SchoolClass::with(['subjectCombos'])->findOrFail($classId);

        if (!$schoolClass)
            throw  new ModelNotFoundException('School class not found.');
//        dd($schoolClass);

        return response()->json($schoolClass->subjectCombos);
    }

    public function toggleSubComboStatus($id)
    {
        $subCombo = SubjectCombination::findOrFail($id);
        $subCombo->status === GeneralService::STATUS_ACTIVE
            ? $subCombo->status = GeneralService::STATUS_INACTIVE
            : $subCombo->status = GeneralService::STATUS_ACTIVE;
        $subCombo->save();

        return back()->with([
            'message' => 'Subject combination status change to ' . $subCombo->status_text . '.',
            'type' => $subCombo->status === GeneralService::STATUS_ACTIVE ? 'success' : 'info'
        ]);
    }
}
