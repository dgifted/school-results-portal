<?php

namespace App\Http\Controllers;

use App\Models\SchoolSignUp;
use Illuminate\Http\Request;

class SchoolSignUpController extends Controller
{
    public function storeSchoolSignUp(Request $request)
    {
        $data = $request->validate([
            'school_name' => ['required', 'string', 'max:50'],
            'email'=> ['nullable', 'email', 'max:50'],
            'phone' => ['required', 'string', 'max:15'],
            'address' => ['required', 'string', 'max:50'],
            'website_exist' => ['nullable', 'string'],
            'website_address' => ['nullable', 'string', 'max:30'],
            'message' => ['nullable', 'string', 'max:255'],
        ]);

        $schoolAddress = SchoolSignUp::create($data);
        if (!$schoolAddress)
            return back()->with([
                'message' => 'Sorry we are having issues with your request. Please try again.',
                'type' => 'danger'
            ]);

        return back()->with([
            'message' => 'Your request to onboard your school has been received successfully.',
            'type' => 'success'
        ]);
    }
}
