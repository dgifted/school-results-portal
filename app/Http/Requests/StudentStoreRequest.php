<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:20'],
            'surname' => ['required', 'string', 'max:20'],
//            'roll_id' => ['required'],
            'school_class_id' => ['required'],
            'email' => ['nullable', 'email'],
            'dob' => ['nullable'],
            'gender' => ['nullable'],
        ];
    }
}
