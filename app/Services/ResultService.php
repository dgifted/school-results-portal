<?php

namespace App\Services;

use App\Models\SchoolClass;
use App\Models\Student;

class ResultService
{
    public function getStudentPosition(SchoolClass $schoolClass, Student $student)
    {
        $students = $schoolClass->students()->with(['results'])->get()
            ->each(function ($_student) {
                $_student->total_score = $this->getStudentTotalScore($_student);
            })->sortByDesc('total_score');
        $studentsIdsToArray = $students->pluck('id')->toArray();

        return array_search($student->id, $studentsIdsToArray) + 1;
    }

    public function getGrade($mark)
    {
        switch (doubleval($mark)) {
            case $mark <= 39:
                return 'F (FAIL)';
            case ($mark >= 40 && $mark <= 54):
                return 'P (PASS)';
            case ($mark >= 55 && $mark <=69):
                return 'C (CREDIT)';
            case ($mark >= 70):
                return 'A (DISTINCTION)';
        }
    }

    private function getStudentTotalScore(Student $student)
    {
        if ($student->results()->count() > 0) {
            $score = 0;
            $student->results()->each(function ($result) use (&$score) {
                $score += doubleval($result->marks);
            });
            return $score;
        }

        return 0;
    }
}
