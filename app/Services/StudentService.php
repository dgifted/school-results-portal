<?php

namespace App\Services;

use App\Models\Student;
use Illuminate\Support\Str;

class StudentService {
    public static function generateStudentId(Student $student)
    {
        $school = $student->user->school;
        $studentsCount = $school->users()->has('student')->count();
        $currentStudentsCount = $studentsCount + 1;

        $currentStudentsCountString = '';
        if ($currentStudentsCount < 100 && $currentStudentsCount >= 10)
            $currentStudentsCountString = '0' . $currentStudentsCount;
        if ($currentStudentsCount < 10)
            $currentStudentsCountString = '00' . $currentStudentsCount;

//        return $school->abbreviation . $school->id . '-' . $currentStudentsCountString;
        return strtoupper(Str::random(4)) . $school->id . '-' . $currentStudentsCountString;
    }
}
