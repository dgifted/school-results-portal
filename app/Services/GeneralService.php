<?php

namespace App\Services;

use Illuminate\Support\Str;

class GeneralService
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public static function generateReferenceId($length = 32)
    {
        return Str::random($length);
    }

}
