<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class SpreadsheetFile implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param string $attribute
     * @param mixed $value
     * @param Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $allowedMimeTypes = [
            'application/vnd.ms-excel', // .xls
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // .xlsx
            'application/vnd.oasis.opendocument.spreadsheet', //.ods
            'text/csv', // .csv
            'text/tab-separated-values' // .tsv
        ];

        if (!in_array($value->getMimeType(), $allowedMimeTypes))
        {
            $fail('The :attribute must be a valid spreadsheet file.');
        }
    }
}
