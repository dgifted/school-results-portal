<?php

namespace App\Imports;

use App\Models\Subject;
use App\Services\GeneralService;
use Illuminate\Auth\AuthenticationException;
use Maatwebsite\Excel\Concerns\ToModel;

class SubjectsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws AuthenticationException
     */
    public function model(array $row)
    {
        if (!auth()->check())
            throw new AuthenticationException('You must login first.');

        return new Subject([
            'ref_id' => GeneralService::generateReferenceId(),
            'name' => $row[0],
            'code' => $row[1] ?? null,
        ]);
    }
}
