<?php

namespace App\Imports;

use App\Models\SchoolClass;
use App\Services\GeneralService;
use Illuminate\Auth\AuthenticationException;
use Maatwebsite\Excel\Concerns\ToModel;

class SchoolClassesImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws AuthenticationException
     */
    public function model(array $row)
    {
        if (!auth()->check())
            throw new AuthenticationException('You must login first.');

        $yearGroupRow = $row[0];
        $yearGroupExploded = explode(' ', $yearGroupRow);
        if (count($yearGroupExploded) > 1)
            $yearGroup = implode('', $yearGroupExploded);
        else
            $yearGroup = $yearGroupExploded[0];

        switch (strtolower($yearGroup)) {
            case 'jss1':
                $yearGroupId = 1;
                break;
            case 'jss2':
                $yearGroupId = 2;
                break;
            case 'jss3':
                $yearGroupId = 3;
                break;
            case 'ss1':
                $yearGroupId = 4;
                break;
            case 'ss2':
                $yearGroupId = 5;
                break;
            case 'ss3':
                $yearGroupId = 6;
                break;
            default:
                $yearGroupId = 0;
        }

        if ($yearGroupId == 0)
            throw new \InvalidArgumentException('Invalid year group.');

        return new SchoolClass([
            'school_id' => auth()->user()->school->id,
            'year_group_id' => $yearGroupId,
            'ref_id' => GeneralService::generateReferenceId(),
            'name' => $row[1],
            'alias' => $row[2] ?? null,
        ]);
    }
}
