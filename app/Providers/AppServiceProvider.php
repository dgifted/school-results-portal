<?php

namespace App\Providers;

use App\Models\SchoolClass;
use App\Models\User;
use App\Observers\SchoolClassObserver;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //Send current user with school information to some blade partials
        View::composer([
            'dashboard.includes.sidebar',
            'dashboard.includes.topnav',
            'dashboard.includes.footer',
            'backoffice.includes.header',
            'backoffice.includes.sidebar',
            ], function ($view) {
            $view->with('user', User::with(['admin', 'school'])->findOrFail(auth()->id()));
        });

        //Register observer classes for some models
//        SchoolClass::observe(SchoolClassObserver::class);
    }
}
