<?php

namespace App\Observers;

use App\Models\Student;
use App\Services\StudentService;

class StudentObserver
{
    public $afterCommit = true;

    /**
     * Handle the Student "created" event.
     * @param Student $student
     */
    public function created(Student $student): void
    {
        $student->reg_no = StudentService::generateStudentId($student);
        $student->save();
    }

    /**
     * Handle the Student "updated" event.
     */
    public function updated(Student $student): void
    {
        //
    }

    /**
     * Handle the Student "deleted" event.
     */
    public function deleted(Student $student): void
    {
        $student->user->delete();
    }

    /**
     * Handle the Student "restored" event.
     */
    public function restored(Student $student): void
    {
        //
    }

    /**
     * Handle the Student "force deleted" event.
     */
    public function forceDeleted(Student $student): void
    {
        //
    }
}
