<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta
        name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <link
        rel="shortcut icon"
        href="{{asset('assets/images/favicon.ico')}}"
        type="image/ico"
    />
    <!-- Libs CSS -->

    <link
        href="{{asset('assets/backoffice/libs/bootstrap-icons/font/bootstrap-icons.css')}}"
        rel="stylesheet"
    />
    <link href="{{asset('assets/backoffice/libs/dropzone/dist/dropzone.css')}}" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@20..48,100..700,0..1,-50..200"/>
    <link
        href="{{asset('assets/backoffice/libs/prismjs/themes/prism-okaidia.css')}}"
        rel="stylesheet"
    />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('assets/backoffice/css/theme.min.css')}}"/>
    <title>@yield('title') | {{ config('app.name') }}</title>

    <style>
        .material-symbols-outlined {font-variation-settings: 'FILL' 0, 'wght' 400, 'GRAD' 0, 'opsz' 20 /*'opsz' 48*/ }[x-cloak] {display: none !important;}.my-actions {margin: 0 2em;}.order-1 {order: 1;}.order-2 {order: 2;}.order-3 {order: 3;}.right-gap {margin-right: auto;}
    </style>
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.12.1/dist/cdn.min.js"></script>
    @yield('styles')
</head>
<body class="bg-light">
<div id="db-wrapper">
    @include('backoffice.includes.sidebar')
    <div id="page-content">
        @include('backoffice.includes.header')
        @yield('dashboard-background')
        <div class="container-fluid px-6 py-4">
            @yield('content')
        </div>
    </div>
</div>
<script src="{{asset('vendors/clipboard.js-master/dist/clipboard.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.8/clipboard.min.js"></script>--}}
<script src="{{asset('assets/backoffice/libs/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/feather-icons/dist/feather.min.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/prismjs/prism.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/dropzone/dist/min/dropzone.min.js')}}"></script>
<script src="{{asset('assets/backoffice/libs/prismjs/plugins/toolbar/prism-toolbar.min.js')}}"></script>
{{--<script--}}
{{--    src="{{asset('assets/backoffice/libs/prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard.min.js')}}"></script>--}}

@yield('scripts')
</body>
</html>
