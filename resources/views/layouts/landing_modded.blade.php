@php
    $route = \Route::currentRouteName()
@endphp

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | {{ config('app.name') }}</title>
{{--    <link rel="icon" type="image/x-icon" href="{{asset('assets/landing/images/favicon.png')}}">--}}
    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/ico"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/landing/css/style.css')}}">
    @yield('styles')
</head>
<body>
{{--<header>--}}
{{--    <div class="topbar d-none d-md-block">--}}
{{--        <div class="container">--}}
{{--            <div class="top-sec d-grid">--}}
{{--                <ul class="d-flex list-unstyled m-0 left-info">--}}
{{--                    <li>--}}
{{--                        <span class="fa fa-phone text-white me-2"></span><a href="tel:+(91) 000 0000">(+91) 0000 0000--}}
{{--                            00</a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="#help" class="d-md-block d-none">Help Center</a>--}}
{{--                    </li>--}}
{{--                    <li class="">--}}
{{--                        <a href="mailto:mayuri.infospace@gmail.com"--}}
{{--                           class="d-md-block d-none">mayuri.infospace@gmail.com</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <div>--}}
{{--                    <ul class="d-flex  list-unstyled align-items-center m-0 right-info">--}}
{{--                        <li>--}}
{{--                            <a href="#"><span class="fab fa-facebook-f"></span></a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#"><span class="fab fa-twitter"></span></a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#"><span class="fab fa-instagram"></span></a>--}}
{{--                        </li>--}}
{{--                        <li class="mr-0">--}}
{{--                            <a href="#"><span class="fab fa-linkedin-in"></span></a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <nav class="navbar navbar-expand-lg bg-white">--}}
{{--        <div class="container">--}}
{{--            <a class="navbar-brand" href="{{ route('home') }}"><img src="{{asset('assets/images/logo.png')}}" style="height: 70px"></a>--}}
{{--            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"--}}
{{--                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"--}}
{{--                    aria-expanded="false" aria-label="Toggle navigation">--}}
{{--                <span class="navbar-toggler-icon"></span>--}}
{{--            </button>--}}
{{--            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">--}}
{{--                <ul class="navbar-nav me-auto mb-2 mb-lg-0">--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{ $route == 'home' ? 'active' : '' }}" aria-current="page" href="{{ route('home') }}">Home</a>--}}
{{--                    </li>--}}

{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{ $route == 'results.check' ? 'active' : '' }}" href="{{ route('results.check') }}">Check Result</a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link {{ $route == 'login' ? 'active' : '' }}" href="{{ route('login') }}">Admin</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--                <form class="d-flex" role="search">--}}
{{--                    <button class="btn btn-danger" type="submit" href="javascript:void(0)">Search</button>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </nav>--}}
{{--</header>--}}
@yield('content')
{{--<footer class="footer pt-5">--}}
{{--    <div class="container py-5">--}}
{{--        <div class="row footer-top-29">--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-1 pr-lg-5">--}}
{{--                <div class="footer-logo mb-4">--}}
{{--                    <a class="navbar-brand" href="{{ route('home') }}">--}}
{{--                        <img style="height: 70px" src="{{asset('assets/images/logo.png')}}" alt=""></a>--}}
{{--                </div>--}}
{{--                <p>Mayuri K. is working in PHP, PDO, Python and Laravel. Soon she is going to launch an online course on--}}
{{--                    PHP projects for beginners. You can contact her for any Project development, either academic or--}}
{{--                    commercial Projects.</p>--}}
{{--                <div class="main-social-footer-29 mt-4">--}}
{{--                    <a href="#facebook" class="facebook"><span class="fab fa-facebook-f"></span></a>--}}
{{--                    <a href="#twitter" class="twitter"><span class="fab fa-twitter"></span></a>--}}
{{--                    <a href="#instagram" class="instagram"><span class="fab fa-instagram"></span></a>--}}
{{--                    <a href="#linkedin" class="linkedin"><span class="fab fa-linkedin-in"></span></a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-2 mt-sm-0 mt-5">--}}

{{--                <ul>--}}
{{--                    <h6 class="footer-title-29">Navigation</h6>--}}
{{--                    <li><a href="about.html">About Us</a></li>--}}
{{--                    <li><a href="blog.html"> Blog posts</a></li>--}}
{{--                    <li><a href="services.html">Services</a></li>--}}
{{--                    <li><a href="contact.html">Contact us</a></li>--}}
{{--                    <li><a href="components.html"> Components</a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            <div class="col-lg-2 col-md-6 col-sm-5 col-6 footer-list-29 footer-3 mt-lg-0 mt-5">--}}
{{--                <h6 class="footer-title-29">Resources</h6>--}}
{{--                <ul>--}}
{{--                    <li><a href="#traning">Marketing Plans</a></li>--}}
{{--                    <li><a href="#marketplace">Digital Services</a></li>--}}
{{--                    <li><a href="#experts">Interior Design</a></li>--}}
{{--                    <li><a href="#platform">Product Selling</a></li>--}}
{{--                    <li><a href="#marketplace">Digital Services</a></li>--}}
{{--                </ul>--}}

{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-7 footer-list-29 footer-4 mt-lg-0 mt-5">--}}
{{--                <h6 class="footer-title-29">Contact Info </h6>--}}
{{--                <p>Head office : Solution Soft Office Ameze Lane Nsukka.</p>--}}
{{--                <p class="mt-2">Phone : <a href="tel:(+234) 8030847991">(+234)8030847991</a></p>--}}
{{--                <p class="mt-2">Email : <a href="mailto:contact@justusali.online">contact@justusali.online</a></p>--}}
{{--                <p class="mt-2">Support : <a href="mailto:contact@justusali.online">contact@justusali.online</a></p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- copyright -->--}}
{{--    <section class="w3l-copyright text-center">--}}
{{--        <div class="container">--}}
{{--            <p class="copy-footer-29">Copyright &copy; {{ config('app.name') }} {{ now()->year }}</p>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--</footer>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
        crossorigin="anonymous"></script>
{{--<script src="{{ asset('assets/landing/js/sweet-alert/sweetalert.min.js')}}"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@yield('scripts')
</body>
</html>
