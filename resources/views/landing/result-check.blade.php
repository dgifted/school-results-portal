@extends('layouts.landing')
@section('title', 'Welcome')
@section('styles')
    <style>
        .page-header {
            width: 100%;
            background: transparent url('/assets/landing/images/result-hero-banner.jpg') no-repeat center center;
        }

        .page-header > .content {
            min-height: 200px !important;
            background-color: rgba(0, 0, 0, 0.7);
        }

        .page-header > .content > h3 {
            font-size: 60px;
            line-height: 70px;
            font-weight: 700;
        }
    </style>
@stop

@section('content')
    <section class="page-header">
        <div class=" content d-flex flex-column align-items-center">
            <h3 class="title-big mx-0 text-white">Check Result</h3>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row py-lg-5">
                <div class="col-12 col-sm-10 offset-lg-2">
                    <div class="">
                        <h3 class=" text-capitalize">Enter your student ID and scratch card PIN to proceed.</h3>
                        <p class="mb-5">If you need help with recovering your student id please follow this <a href="#">link.</a></p>
                    </div>

                    @if(session('message'))
                        <div class="alert alert-{{ session('type') ?? 'info' }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    <form action="{{ route('results.check') }}" method="post" style="max-width: 700px">
{{--                    <form action="{{ route('results.check') }}" method="post" >--}}
                        @csrf
                        <div class="alert alert-info" role="alert">
                            <small>All fields are required.</small>
                        </div>

                        <div class="row mb-3">
                            <label class="col-md-3 col-form-label text-lg-end" for="rollId">
                                Student ID<span class="text-danger">*</span>:
                            </label>
                            <div class="col-md-9">
                                <input
                                    class="form-control {{ $errors->has('rollId') ? 'is-invalid' : '' }}"
                                    id="rollId"
                                    type="text"
                                    name="rollId"
                                    required/>
                                @error('rollId')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label class="col-md-3 col-form-label text-lg-end" for="cardPin">
                                Card PIN<span class="text-danger">*</span>:
                            </label>
                            <div class="col-md-9">
                                <input
                                    class="form-control {{ $errors->has('cardPin') ? 'is-invalid' : '' }}"
                                    id="cardPin"
                                    type="text"
                                    name="cardPin"
                                    required/>
                                @error('cardPin')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                                <hr class="my-4"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-9 offset-md-3 pt-lg-3">
                                <button class="btn btn-sm btn-cta-light">Check Result</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
