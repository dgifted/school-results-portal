@extends('layouts.landing')
@section('title', 'Welcome')

@section('content')
    <section class="main-slider">
        <div class="owl-carousel owl-theme" id="slider">
            <div class="item">
                <div class=" banner-view banner-top1 d-flex align-items-center justify-content-center text-center">
                    <div class="banner-info-bg">
                        <!-- <h6>We are Creative</h6> -->
                        <h5>Result Checking {{ config('app.name') }}</h5>
                        <p class="mt-md-4 mt-3">Get the most of reduction in your team’s operating creates amazing UI/UX
                            experiences. </p>
                        <a class="btn btn-style btn-cta mt-sm-5 mt-4 mr-2" href=""> Learn More</a>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class=" banner-view banner-top2 d-flex align-items-center justify-content-center text-center">
                    <div class="banner-info-bg">
                        <h6>We are Creative</h6>
                        <h5>World IT Service company </h5>
                        <p class="mt-md-4 mt-3">Get the most of reduction in your team’s operating creates amazing UI/UX
                            experiences. </p>
                        <a class="btn btn-style btn-cta mt-sm-5 mt-4 mr-2" href=""> Learn More</a>
                    </div>
                </div>
            </div>


        </div>

    </section>
    <section class="py-5 about">
        <div class="container">
            <div class="row ">
                <div class="col-md-6 ">
                    <h3 class="title-big mx-0">Notice Board</h3>
{{--                    <marquee direction="up" onmouseover="this.stop();" onmouseout="this.start();" class="mt-5">--}}
{{--                        <ul class="">--}}
{{--                            <?php $sql = "SELECT * from tblnotice";--}}
{{--                            $query = $dbh->prepare($sql);--}}
{{--                            $query->execute();--}}
{{--                            $results = $query->fetchAll(PDO::FETCH_OBJ);--}}
{{--                            $cnt = 1;--}}
{{--                            if ($query->rowCount() > 0) {--}}
{{--                            foreach ($results as $result) { ?>--}}
{{--                            <li><a href="notice-details.php?nid=<?php echo htmlentities($result->id); ?>"--}}
{{--                                   target="_blank"--}}
{{--                                   class="text-dark"><?php echo htmlentities($result->noticeTitle); ?></li>--}}
{{--                            <?php }--}}
{{--                            } ?>--}}

{{--                        </ul>--}}
{{--                    </marquee>--}}
                </div>
                <div class="col-md-6">
                    <img src="{{asset('assets/landing/images/about.jpg')}}" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section class="py-5 service">
        <div class="container">
            <h3 class="title-big text-center pb-5">Courses offered by Institute</h3>
            <div class="row ">
                <div class="col-md-4">
                    <div class="grids5-info">
                        <a href="#service" class="d-block zoom"><img src="{{asset('assets/landing/images/s1.jpg')}}" alt=""
                                                                     class="img-fluid service-image"></a>
                        <div class="blog-info">
                            <span class="fa fa-desktop"></span>
                            <h4><a href="#service">Work Expertise &amp; Leadership</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="grids5-info">
                        <a href="#service" class="d-block zoom"><img src="{{asset('assets/landing/images/s4.jpg')}}" alt=""
                                                                     class="img-fluid service-image"></a>
                        <div class="blog-info">
                            <span class="fa fa-cogs"></span>
                            <h4><a href="#service">Digital Product Development</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="grids5-info">
                        <a href="#service" class="d-block zoom"><img src="{{asset('assets/landing/images/s3.jpg')}}" alt=""
                                                                     class="img-fluid service-image"></a>
                        <div class="blog-info">
                            <span class="fa fa-line-chart"></span>
                            <h4><a href="#service">Business Software Development</a></h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5">
        <div class="container py-md-5 py-sm-4 py-2">
            <div class="row align-items-center">
                <div class="col-lg-7">
                    <div class="bottom-info">
                        <div class="header-section">
                            <h3 class="title-big mx-0">Start improving your knowledge today! Join Us</h3>
                            <p class="mt-3 pr-lg-5">Lorem ipsum dolor sit amet elit. Velit beatae
                                rem ullam dolore nisi esse quasi. Integer sit amet. Lorem ipsum dolor sit
                                amet elit.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 text-lg-end">
                    <a href="#learn" class="btn btn-style btn-outline-dark mt-lg-0 mt-md-5 mt-4 me-2">Learn More </a>
                    <a href="contact.html" class="btn btn-style btn-cta-light mt-lg-0 mt-md-5 mt-4">Contact us</a>
                </div>
            </div>
        </div>
    </section>
    <section class="py-5" id="services">
        <div class="features-main py-lg-5 py-md-3">
            <div class="container">
                <div class="header-section text-center mx-auto">
                    <h3 class="title-big">To design and deliver the innovative products.</h3>
                </div>
                <div class="row features mt-lg-4">
                    <div class="col-lg-4 col-md-6 feature-grid">
                        <div class="feature-body service1"><a href="#url">
                                <div class="feature-img">
                                    <span class="fa fa-sun" aria-hidden="true"></span>
                                </div>
                            </a>
                            <div class="feature-info mt-4"><a href="#url">
                                    <h3 class="feature-titel mb-3">Augmented Reality </h3>
                                    <p class="feature-text">Lorem ipsum dolor sit amet elit et. Debitis nam, minima iste
                                        ipsum.
                                    </p>
                                </a><a href="#read" class="read mt-4 d-block"> Read More</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-6 feature-grid">
                        <div class="feature-body service2"><a href="#url">
                                <div class="feature-img">
                                    <span class="fa fa-wrench icon-fea" aria-hidden="true"></span>
                                </div>
                            </a>
                            <div class="feature-info mt-4"><a href="#url">
                                    <h3 class="feature-titel mb-3">Deep Expertise</h3>
                                    <p class="feature-text">Lorem ipsum dolor sit amet elit et. Debitis nam, minima iste
                                        ipsum.
                                    </p>
                                </a><a href="#read" class="read mt-4 d-block"> Read More</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4 col-md-6 feature-grid">
                        <div class="feature-body service3"><a href="#url">
                                <div class="feature-img">
                                    <span class="fa fa-flask" aria-hidden="true"></span>
                                </div>
                            </a>
                            <div class="feature-info mt-4"><a href="#url">
                                    <h3 class="feature-titel mb-3">Software development</h3>
                                    <p class="feature-text">Lorem ipsum dolor sit amet elit et. Debitis nam, minima iste
                                        ipsum.
                                    </p>
                                </a><a href="#read" class="read mt-4 d-block"> Read More</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="customers py-md-4 py-3">
        <div class="container">
            <div class="owl-carousel owl-theme" id="customer">
                <div class="item"><img src="{{asset('assets/landing/images/logo2.png')}}"></div>
                <div class="item"><img src="{{asset('assets/landing/images/logo3.png')}}"></div>
                <div class="item"><img src="{{asset('assets/landing/images/logo4.png')}}"></div>
            </div>
        </div>
    </section>
@endsection
