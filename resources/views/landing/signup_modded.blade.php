@extends('layouts.landing_modded')
@section('title', 'Choose your destination')

@section('styles')
    <style>
        ._container {
            background: transparent url('/assets/landing/images/result-hero-banner.jpg') no-repeat center center !important;
            /*height: 100vh;*/
            min-height: 100vh;
            padding: 2rem 0;
            position: relative;
        }

        .bg-dimmer {
            position: absolute;
            background-color: rgba(111, 143, 98, .8);
            width: 100vw;
            min-height: 100vh;
            top: 0;
            left: 0;
        }

        ._container > .foreground {
            width: 100vw;
            /*min-height: 100%;*/
            display: flex;
            flex-direction: column;
            justify-content: center;
            flex-wrap: wrap;
            align-items: center;
            align-content: center;
            gap: 4rem;
            margin: 0;
            padding: 0;
            z-index: 2;
            position: absolute;
            top: 0;
            left: 0;
        }

        .menu-container {
            display: flex;
            /*justify-content: space-between;*/
            justify-content: center;
            flex-wrap: wrap;
            align-items: center;
            align-content: center;
            gap: 2rem;
            min-width: 500px;
            /*background-color: #1a202c;*/
        }

        .session-message {
            font-size: 18px;
            color: #fff;
            opacity: 1;
        }

        .session-message a {
            display: inline-block;
            padding: 5px 10px;
            border: 1px solid transparent;
            background-color: #1D430A;
            color: #fff;
        }

        .session-message a:hover {
            text-decoration: none;
        }

        #signup-form {
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            width: 100%;
            gap: 20px;
        }

        #signup-form div {
            width: 100%;
            display: flex;
            flex-direction: column;
            gap: 5px;
        }

        #signup-form input, #signup-form select, #signup-form textarea, #signup-form button {
            display: block;
            border: transparent;
            /*min-width: 200px;*/
            padding: 5px;
            border-radius: 5px;
            width: 100%;
        }

        #signup-form input:focus, #signup-form select:focus, #signup-form textarea:focus {
            border: 1px solid #0c5460;
        }

        #signup-form button {
            border: 1px solid transparent;
            background-color: #1D430A;
            color: #fff;
        }

        #signup-form button:hover {
            background-color: #24560b;
        }

        ._footer {
            position: absolute;
            display: flex;
            justify-content: center;
            bottom: 0;
            left: 0;
            right: 0;
            width: 100vw;
            /*background-color: #000000;*/
        }

        ._footer * {
            color: #ffffff;
            font-size: 1rem;
        }

        @media screen and (max-width: 532px) {
            ._container {
                min-height: 160vh;
            }

            .bg-dimmer {
                min-height: 160vh;
            }

            .menu-container {
                flex-direction: column;
                justify-content: center;
                flex-wrap: wrap;
                align-items: center;
                align-content: center;
                overflow-y: scroll;
                padding-bottom: 40px;
                min-width: 300px;
            }

            .menu-item {
                min-height: 100px;
                width: 200px;
                font-size: 1.5rem;
            }

            .menu-item .info-badge {
                font-size: 10px;
                font-weight: 500;
            }

            ._footer {
                position: fixed;
                display: flex;
                justify-content: center;
                bottom: 0;
                left: 0;
                right: 0;
                width: 100vw;
                z-index: 100;
                /*background-color: #000000;*/
            }
        }
    </style>
@stop

@section('content')
    <section class="">
        <div class="_container">
            <div class="bg-dimmer">&nbsp;</div>

            <div class="foreground">
                <div class="text-center">
                    <img src="{{ asset('assets/images/logo.png') }}" style="width: 150px;" alt="logo"/>
                </div>
                <div class="menu-container">
                    @if(session('message'))
                        <p class="session-message">{{ session('message') }}</p>
                        @if(session('type' == 'danger'))
                            <p class="session-message"><a href="{{ route('school.signup') }}">Show form</a></p>
                        @else
                        @endif
                    @else
                        <form action="{{ route('school.signup') }}" method="post" id="signup-form">
                            @csrf
                            <div>
                                <input type="text" name="school_name" placeholder="School name" required max="50"/>
                                @error('school_name')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div>
                                <input type="email" name="email" placeholder="Email address" max="50"/>
                                @error('email')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div>
                                <input type="tel" name="phone" placeholder="Mobile number" required max="15"/>
                                @error('phone')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div>
                                <input type="text" name="address" placeholder="School Address" required max="50"/>
                                @error('address')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div>
                                <select name="website_exist">
                                    <option value="">Do you have a school website</option>
                                    <option>Yes</option>
                                    <option>No</option>
                                </select>
                                @error('website_exist')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div>
                                <input type="text" name="website_address" placeholder="Website Address" max="30"/>
                                @error('website_address')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div>
                                <textarea name="message" placeholder="Any additional information?" rows="5"
                                          maxlength="250"></textarea>
                                @error('message')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>

                            <button type="submit">Submit</button>
                        </form>
                    @endif
                </div>
            </div>

            <div class="_footer">
                <p class="text-center">&copy; Copyright {{ now()->year }} edupoint.com.ng</p>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
        });
    </script>
@endsection
