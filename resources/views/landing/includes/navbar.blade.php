<div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <a class="nav-link {{ $route == 'home' ? 'active' : '' }}" aria-current="page" href="{{ route('home') }}">Home</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ $route == 'results.check' ? 'active' : '' }}" href="{{ route('results.check') }}">Check Result</a>
        </li>
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link {{ $route == 'login' ? 'active' : '' }}" href="{{ route('login') }}">Admin</a>--}}
{{--        </li>--}}
        <li class="nav-item ">
            <a class="nav-link beta" href="#">CBT</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link beta" href="#">Lesson Planner</a>
        </li>
        <li class="nav-item">
            <a class="nav-link beta" href="#">Timetable</a>
        </li>
        <li class="nav-item">
            <a class="nav-link beta" href="#">Calender</a>
        </li>
    </ul>
    <form class="d-flex" role="search">
        {{--                    <button class="btn btn-danger" type="submit" href="javascript:void(0)">Search</button>--}}
    </form>
</div>
