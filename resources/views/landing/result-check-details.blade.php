@extends('layouts.landing')
@section('title', 'Student result details')
@section('styles')
    <style>
        .page-header {
            width: 100%;
            background: transparent url('/assets/landing/images/result-hero-banner.jpg') no-repeat center center;
        }

        .page-header > .content {
            min-height: 200px !important;
            background-color: rgba(0, 0, 0, 0.7);
        }

        .page-header > .content > h3 {
            font-size: 60px;
            line-height: 70px;
            font-weight: 700;
        }

        table {
            border-collapse: collapse;
            border: 1px solid black;
        }

        td {
            padding: 0.5rem 1em;
            border: 1px solid rgba(20, 30, 30, 0.5);
            font-size: 1rem;
        }

        .rotate-90 {
            display: inline-block;
            transform: rotate(-90deg) !important;
        }

        .xtra p {
            display: flex;
            align-items: flex-end;
            gap: 10px;
        }

        .xtra p .underline {
            flex-grow: 1;
            border-bottom: 1px solid #8b8b8b;
        }
    </style>
    <link
        href="{{ asset('assets/landing/css/result-sheet-print.css') }}"
        rel="stylesheet"
        type="text/css"
        media="print"
    />
@stop

@section('content')
    <section class="page-header">
        <div class=" content d-flex flex-column align-items-center">
            <h3 class="title-big mx-0 text-white">Result Detail</h3>
        </div>
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row py-lg-5">
                <div class="col-12 text-center mb-3">
                    <button class="btn btn-primary btn-sm" onclick="window.print();" id="cta-print">Print Result Slip
                    </button>
                </div>
                <div class="col-12 col-xl-10 offset-xl-1">
                    <div class="d-flex flex-column justify-content-between align-items-stretch">
                        <div class="d-flex flex-column align-items-center w-100 text-uppercase" id="school-info">
                            <h2 class="font-weight-bolder my-0">
                                {{ $student->user->school->short_name }}
                            </h2>
                            <h3 class="font-weight-normal my-0">{{ $student->user->school->address }}</h3>
                            <h3 class="font-weight-normal my-0">Termly Report.</h3>
                        </div>

                        <div class="row mt-5 w-100" id="student-info">
                            <div class="col-12 col-md-6 d-flex flex-column align-items-start">
                                <p class="font-weight-bold">Name of Student:
                                    <span
                                        class="text-uppercase">{{ $student->first_name }} {{ $student->surname }}</span>
                                </p>
                                <p class="font-weight-bold">Class:
                                    <span class="text-uppercase">
                                    {{ $student->studentClass->yearGroup->short_code }} {{ $student->studentClass->name }}
                                </span>
                                </p>
                                <p class="font-weight-bold">Number in Class:
                                    <span class="text-uppercase">{{ $student->classSize }}</span></p>
                                <p class="font-weight-bold">Position in Class:
                                    <span><em style="margin-right: -5px;">{{ $student->position }}</em>
                                    @switch(intval($student->position))
                                            @case(1)
                                            <i>st</i>
                                            @break
                                            @case(2)
                                            <i>nd</i>
                                            @break
                                            @case(3)
                                            <i>rd</i>
                                            @break
                                            @default
                                            <i>th</i>
                                        @endswitch
                                            </span>
                                    Out of <span class="text-uppercase">{{ $student->classSize }}</span>
                                </p>
                                <p class="font-weight-bold">Next term begins:
                                    <span class="text-uppercase">N/A</span></p>
                            </div>

                            <div class="col-12 col-md-6 p-0 d-flex justify-content-md-end align-items-start">
                                <img
                                    src="{{ $student->user->avatar_path }}"
                                    class="img-thumbnail img-responsive" width="150" height="50"
                                    alt="{{ $student->user->first_name }} {{ $student->user->surname }}"
                                    title="{{ $student->user->first_name }} {{ $student->user->surname }}"
                                />
                            </div>
                        </div>

                        <table class="my-4">
                            <tbody>
                            <tr>
                                <td>
                                    <div>
                                        <h4 class="text-uppercase font-weight-bolder">Keys to grade</h4>
                                        <p class="d-flex justify-content-between my-0 font-weight-bold">
                                            <span>A (Distinction)</span>
                                            <span>70% & above</span>
                                        </p>
                                        <p class="d-flex justify-content-between my-0 font-weight-bold">
                                            <span>C (Credit)</span>
                                            <span>>=55% <70%</span>
                                        </p>
                                        <p class="d-flex justify-content-between my-0 font-weight-bold">
                                            <span>P (Pass)</span>
                                            <span>>=40% <55%</span>
                                        </p>
                                        <p class="d-flex justify-content-between my-0 font-weight-bold">
                                            <span>F (Fail)</span>
                                            <span><40%</span>
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <span class="rotate-90 font-weight-bold">Total score</span>
                                </td>
                                <td>
                                    <span class="rotate-90 font-weight-bold">Grade (ACPF)</span>
                                </td>
                                <td>
                                    <span class="rotate-90 font-weight-bold">Remarks</span>
                                </td>
                            </tr>
                            @foreach($student->results as $result)
                                <tr>
                                    <td>{{ $result->subject->name }}</td>
                                    <td>{{ $result->marks }}</td>
                                    <td>{{ $result->grade }}</td>
                                    <td>{{ $result->remarks }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="xtra py-5">
                            <p><span>Form teacher's comments: </span>
                                <span class="underline"></span>
                            </p>
                            <p><span> Form teacher's Name:</span>
                                <span class="underline"></span>
                            </p>
                            <p><span>Form teacher's Signature:</span>
                                <span class="underline"></span>
                            </p>
                            <p><span>Date: </span>
                                <span class="underline"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
