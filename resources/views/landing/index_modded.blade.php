@extends('layouts.landing_modded')
@section('title', 'Choose your destination')

@section('styles')
    <style>
        ._container {
            background: transparent url('/assets/landing/images/result-hero-banner.jpg') no-repeat center center !important;
            /*height: 100vh;*/
            min-height: 100vh;
            padding: 2rem 0;
            position: relative;
        }

        .bg-dimmer {
            position: absolute;
            background-color: rgba(111, 143, 98, .8);
            width: 100vw;
            min-height: 100vh;
            top: 0;
            left: 0;
        }

        ._container > .foreground {
            width: 100vw;
            min-height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            flex-wrap: wrap;
            align-items: center;
            align-content: center;
            gap: 10px;
            margin: 0;
            padding: 0;
            z-index: 2;
            position: absolute;
            top: 0;
            left: 0;
        }

        .menu-container {
            display: flex;
            /*justify-content: space-between;*/
            justify-content: center;
            flex-wrap: wrap;
            align-items: center;
            align-content: center;
            gap: 2rem;
        }


        .menu-item {
            min-height: 200px;
            width: 260px;
            border: 1px solid #ffffff;
            border-radius: 10px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            color: #fff;
            font-size: 2rem;
            font-weight: bold;
            text-decoration: none;
            background-color: rgba(29, 67, 11, .7);
            transition: transform 100ms ease-in-out;
            transform: translateY(0);
            position: relative;
        }

        .menu-item:hover {
            color: #fff;
            transform: translateY(-2px);
            box-shadow: -2px 10px 14px 0px rgba(29, 67, 11, 0.59);
            -webkit-box-shadow: -2px 10px 14px 0px rgba(29, 67, 11, 0.59);
            -moz-box-shadow: -2px 10px 14px 0px rgba(29, 67, 11, 0.59);
        }

        .menu-item .info-badge {
            position: absolute;
            top: 0;
            right: 0;
            border-radius: 15px;
            background-color: gray;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            font-size: 12px;
            padding: 5px 10px;
        }

        .menu-item .description {
            font-size: 15px;
            font-weight: 500;
            color: #fff;
            padding: 5px 10px;
            text-align: center;
        }

        ._footer {
            position: absolute;
            display: flex;
            justify-content: center;
            bottom: 0;
            left: 0;
            right: 0;
            width: 100vw;
            /*background-color: #000000;*/
        }

        ._footer * {
            color: #ffffff;
            font-size: 1rem;
        }

        @media screen and (max-width: 532px) {
            ._container {
                /*min-height: 160vh;*/
                min-height: 260vh;
                /*background-clip: content-box;*/
                /*background-repeat: repeat-y;*/
                /*background-position: top right;*/
                /*background-color: #0c0c0c;*/
            }

            .bg-dimmer {
                /*min-height: 160vh;*/
                min-height: 260vh;
            }

            .menu-container {
                flex-direction: column;
                justify-content: center;
                flex-wrap: wrap;
                align-items: center;
                align-content: center;
                overflow-y: scroll;
                padding-bottom: 40px;
            }

            .menu-item {
                min-height: 100px;
                width: 200px;
                font-size: 1.5rem;
            }

            .menu-item .info-badge {
                font-size: 10px;
                font-weight: 500;
            }

            ._footer {
                position: fixed;
                display: flex;
                justify-content: center;
                bottom: 0;
                left: 0;
                right: 0;
                width: 100vw;
                z-index: 100;
                /*background-color: #000000;*/
            }
        }
    </style>
@stop

@section('content')
    <section class="">
        <div class="_container">
            <div class="bg-dimmer">&nbsp;</div>

            <div class="foreground">
                <div class="text-center">
                    <img src="{{ asset('assets/images/logo.png') }}" style="width: 150px;" alt="logo"/>
                </div>
                <p class="text-center"><a class="btn btn-success text-uppercase" href="{{ route('school.signup') }}">Sign up</a></p>

                <div class="menu-container">
                    <a href="{{ route('results.check') }}" class="menu-item">
                        Check result<br>
                        <p class="description">Obtain precise and error-free student results every term with our system.
                            Additionally, our platform automatically generates your annual result</p>
                    </a>
                    <a href="{{ route('login') }}" class="menu-item">
                        Portal<br/>
                        <p class="description">Effortlessly manage student admissions, attendance, staff, scheduling,
                            academic assessment, finances, and communication with parents and stakeholders.</p>
                    </a>
                    <a href="#" class="menu-item beta">
                        CBT<br/>
                        <span class="info-badge">Beta</span>
                        <p class="description">Enhance your students' performance in both internal and external
                            examinations with our computer-based testing that's compatible with mobile devices.</p>
                    </a>
                    <a href="#" class="menu-item beta">
                        Timetable<br>
                        <span class="info-badge">Beta</span>
                        <p class="description">Optimize your school's scheduling efficiency with our time table,
                            designed to streamline timetables and class schedules, ensuring a seamless academic
                            experience.</p>
                    </a>
                    <a href="#" class="menu-item beta">
                        Calender<br/>
                        <span class="info-badge">Beta</span>
                        <p class="description">Elevate your school's organization with our 'Calendar' feature in the
                            School Management SaaS platform, simplifying event scheduling and planning.</p>
                    </a>
                    <a href="#" class="menu-item beta">
                        Free website<br/>
                        <p class="description">Create your free school website with our user-friendly feature in
                            the {{ config('app.name') }} platform, unlocking online presence without the cost.</p>
                    </a><br/>
                </div>
            </div>

            <div class="_footer">
                <p class="text-center">&copy; Copyright {{ now()->year }} edupoint.com.ng</p>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        document.addEventListener('DOMContentLoaded', () => {
            const menuLinks = document.querySelectorAll('.menu-item');

            function betaLinkClickHandler(evt) {
                evt.preventDefault();
                Swal.fire({
                    title: 'Click the link to Sign up your school.',
                    html: `<a href="{{ route('school.signup') }}" class="btn btn-success btn-sm">Sign up</a>`,
                    confirmButtonText: 'Cancel',
                    customClass: {
                        actions: 'my-actions',
                        cancelButton: 'order-1 right-gap',
                        confirmButton: 'order-2',
                        denyButton: 'order-3',
                    }
                })
            }

            menuLinks.forEach(function (element) {
                element.addEventListener('click', betaLinkClickHandler);
            });

        });
    </script>
@endsection
