@extends('layouts.backoffice')
@section('title', 'Edit school information')
@section('styles')
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold">Update school info</h3>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="row">
            <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                <strong>Success!</strong> <br>
                {!! session('message') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Please fix the following error(s)</strong> <br>
                <ul>
                    @foreach($errors->all() as $msg)
                        <li>{{ $msg }}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">school</span>
                        Edit school data
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('backoffice.schools.edit', $school->ref_id) }}"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')

                        <div class="mb-3">
                            <label for="name" class="form-label">School Name</label>
                            <input id="name"
                                   type="text"
                                   class="form-control"
                                   name="name"
                                   value="{{ $school->name }}"
                                   autofocus
                            />
                        </div>

                        <div class="mb-3">
                            <label for="address" class="form-label">School Address</label>
                            <input id="address"
                                   type="text"
                                   class="form-control"
                                   name="address"
                                   value="{{ $school->address }}"
                            />
                        </div>

                        <div class="mb-3">
                            <label for="type" class="form-label text-md-right">School Type</label>
                            <select
                                id="type"
                                class="form-select"
                                name="type"
                                style="max-width: 300px;"
                            >
                                <option value=""></option>
                                @foreach($schoolTypes as $type)
                                    <option value="{{ $type->id }}"
                                            @if($type->id == $school->school_type_id) selected @endif>
                                        {{ ucwords($type->name) }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="logo" class="form-label text-md-right">School Logo</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="logo"
                                           type="file"
                                           class="d-none"
                                           name="logo"
                                           accept="image/*"
                                    />
                                    <button
                                        type="button"
                                        class="btn btn-info"
                                        id="logoButton"
                                        onclick="document.getElementById('logo').click();"
                                    >Choose Image
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    @if(!is_null($school->logo))
                                        <img src="{{ $school->logo_path }}"
                                             alt=""
                                             class="img-thumbnail"
                                             width="200"
                                             height="200"
                                             id="currentPreview">
                                    @endif
                                    <img src="" alt="" width="200" height="200" id="preview" class="d-none">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="banner" class="form-label text-md-right">School Banner</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="banner"
                                           type="file"
                                           class="d-none"
                                           name="banner"
                                           accept="image/*"
                                    />
                                    <button
                                        type="button"
                                        class="btn btn-info"
                                        id="bannerButton"
                                        onclick="document.getElementById('banner').click();"
                                    >Choose Image
                                    </button>
                                </div>

                                <div class="col-md-6">

                                    @if(!is_null($school->banner))
                                        <img src="{{ $school->banner_path }}"
                                             alt=""
                                             class="img-thumbnail"
                                             width="320"
                                             height="150"
                                             id="currentBannerPreview">
                                    @endif
                                    <img src="" alt="" width="320" height="150" id="previewBanner" class="d-none">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3 mt-10">
                            <button class="btn btn-primary" type="submit">Update School</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        const logoButton = document.getElementById('logo');
        const previewImage = document.getElementById('preview');
        const currentPreviewImage = document.getElementById('currentPreview');
        const bannerButton = document.getElementById('banner');
        const currentPreviewBanner = document.getElementById('currentBannerPreview');

        logoButton.addEventListener('change', function () {
            const logo = logoButton.files[0];
            const reader = new FileReader();
            reader.onload = function (e) {
                previewImage.classList.remove('d-none');
                currentPreviewImage.classList.add('d-none');
                previewImage.src = e.target.result;
                previewImage.classList.add('img-thumbnail');
            };
            reader.readAsDataURL(logo);
        });

        bannerButton.addEventListener('change', function () {
            const banner = bannerButton.files[0];
            const reader = new FileReader();
            reader.onload = function (e) {
                previewBanner.classList.remove('d-none');
                currentPreviewBanner.classList.add('d-none');
                previewBanner.src = e.target.result;
                previewBanner.classList.add('img-thumbnail');
            };
            reader.readAsDataURL(banner);
        })
    </script>
@endsection
