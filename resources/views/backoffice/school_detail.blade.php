@extends('layouts.backoffice')
@section('title', $school->short_name . ' details')
@section('styles')
@stop


@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold">School information</h3>
            </div>
        </div>
    </div>
    <section x-cloak x-data="pageData">
        <div class="row align-items-center">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                <!-- Bg -->
                <div
                    class="pt-20 rounded-top"
                    style="
                        background: url({{ $school->banner_path }})
                        no-repeat;
                        background-size: cover;
                        "
                ></div>
                <div class="bg-white rounded-bottom smooth-shadow-sm">
                    <div
                        class="d-flex align-items-center justify-content-between pt-4 pb-6 px-4"
                    >
                        <div class="d-flex align-items-center">
                            <!-- avatar -->
                            <div
                                class="avatar-xxl avatar-indicators avatar-online me-2 position-relative d-flex justify-content-end align-items-end mt-n10"
                            >
                                <img
                                    src="{{ $school->logo_path }}"
                                    class="avatar-xxl rounded-circle border border-4 border-white-color-40"
                                    alt=""
                                />
                            </div>
                            <!-- text -->
                            <div class="lh-1">
                                <h2 class="mb-0">{{ $school->short_name }}</h2>
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('backoffice.assign-admin', $school->ref_id) }}"
                               class="btn btn-outline-primary d-none d-md-block">Add school Admin</a>
                        </div>
                    </div>
                    <!-- nav -->
                    <ul class="nav nav-lt-tab px-4" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                :class="{'active': tabIndex == 0}"
                                @click="setActiveTab(0)"
                                href="#">Classes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               :class="{'active': tabIndex == 1}"
                               @click="setActiveTab(1)"
                               href="#">Subjects</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link"
                               :class="{'active': tabIndex == 2}"
                               @click="setActiveTab(2)"
                               href="#">Students</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               :class="{'active': tabIndex == 3}"
                               @click="setActiveTab(3)"
                               href="#">Declared Results</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               :class="{'active': tabIndex == 4}"
                               @click="setActiveTab(4)"
                               href="#">Notices </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        @if(session('message'))
            <div class="row mt-6">
                <div class="col-12">
                    <div class="alert alert-{{ session('type') ?? 'warning' }} alert-dismissible fade show" role="alert">
                        {{ session('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>
        @endif

        <div class="row mt-6">
            <div class="col-md-12 col-12">
                <template x-if="tabIndex == 0">
                    <div class="card">
                        <!-- card header  -->
                        <div class="card-header bg-white py-4">
                            <h4 class="mb-0">Classes</h4>
                        </div>
                        <!-- table  -->
                        <div class="table-responsive">
                            <table class="table text-nowrap mb-0">
                                <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th>Class Name</th>
                                    <th>Year Group</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($school->classes->count() > 0)
                                    @foreach($school->classes as $class)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td class="align-middle">{{ $class->name }}</td>
                                            <td class="align-middle">{{ $class->yearGroup->short_code }}</td>
                                            <td class="align-middle"><span class="badge bg-success">Active</span></td>
                                            <td class="align-middle text-dark">
                                                <button class="btn btn-sm btn-danger">Deactivate</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <em class="text-center">No class in the record.</em>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- card footer  -->
                        {{--                    <div class="card-footer bg-white text-center">--}}
                        {{--                        <a href="#" class="link-primary">View All Projects</a>--}}
                        {{--                    </div>--}}
                    </div>
                </template>

                <template x-if="tabIndex == 1">
                    <div class="card">
                        <!-- card header  -->
                        <div class="card-header bg-white py-4">
                            <h4 class="mb-0">Subjects</h4>
                        </div>
                        <!-- table  -->
                        <div class="table-responsive">
                            <table class="table text-nowrap mb-0">
                                <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th>Subject Name</th>
                                    <th>Subject Code</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($school->subjects->count() > 0)
                                    @foreach($school->subjects as $subject)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td class="align-middle">{{ $subject->name }}</td>
                                            <td class="align-middle">{{ $subject->code ?? 'N/A'}}</td>
                                            <td class="align-middle"><span class="badge bg-success">Active</span></td>
                                            <td class="align-middle text-dark">
                                                <button class="btn btn-sm btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <em class="text-center">No subject in the record.</em>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- card footer  -->
                    </div>
                </template>

                <template x-if="tabIndex == 2">
                    <div class="card">
                        <!-- card header  -->
                        <div class="card-header bg-white py-4">
                            <div class="d-flex justify-content-between align-items-center">
                                <h4 class="mb-0">Students</h4>
{{--                                <a class="btn btn-sm btn-info text-capitalize d-inline-flex align-items-center"--}}
{{--                                   href="{{ route('backoffice.students.regenerate-ids', $school->ref_id) }}"--}}
{{--                                   href="javascript:alert('Will soon be implemented.')"--}}
{{--                                > Regenerate Students IDs</a>--}}
                            </div>
                        </div>
                        <!-- table  -->
                        <div class="table-responsive">
                            <table class="table text-nowrap mb-0">
                                <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th>Student Name</th>
                                    <th>Subject Class</th>
                                    <th>Student Id</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($school->users->count() > 0)
                                    @foreach($school->users as $user)
                                        @if(!is_null($user->student))
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td class="align-middle">{{ $user->first_name }} {{ $user->surname }}</td>
                                                <td class="align-middle">
                                                    {{ $user->student->studentClass->yearGroup->short_code }}&nbsp;
                                                    {{ $user->student->studentClass->name }}
                                                </td>
                                                <td class="align-middle"><span>{{ $user->student->reg_no }}</span></td>
                                                <td class="align-middle text-dark">
                                                    <button class="btn btn-sm btn-success">Promote</button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <em class="text-center">No student in the record.</em>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </template>

                <template x-if="tabIndex == 3">
                    <div class="card">
                        <!-- card header  -->
                        <div class="card-header bg-white py-4">
                            <h4 class="mb-0">Declared Results</h4>
                        </div>
                        <!-- table  -->
                        <div class="table-responsive">
                            <table class="table text-nowrap mb-0">
                                <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th>Student Name</th>
                                    <th>Subject</th>
                                    <th>Marks</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($school->users->count() > 0)
                                    @foreach($school->users as $user)
                                        @if(!is_null($user->student))
                                            @if($user->student->results->count() > 0)
                                                @foreach($user->student->results as $result)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td class="align-middle">{{ $user->first_name }} {{ $user->surname }}</td>
                                                        <td class="align-middle">
                                                            {{ $result->subject->name }}
                                                        </td>
                                                        <td class="align-middle"><span>{{ $result->marks }}</span></td>
                                                        <td class="align-middle text-dark">
                                                            <button class="btn btn-sm btn-danger">Delete</button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4">
                                                        <em class="text-center">No declared results in the record.</em>
                                                    </td>
                                                </tr>
                                            @endif
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <em class="text-center">No student in the record.</em>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </template>

                <template x-if="tabIndex == 4">
                    <div class="card">
                        <!-- card header  -->
                        <div class="card-header bg-white py-4">
                            <h4 class="mb-0">School Notices</h4>
                        </div>
                        <!-- table  -->
                        <div class="table-responsive">
                            <table class="table text-nowrap mb-0">
                                <thead class="table-light">
                                <tr>
                                    <th>#</th>
                                    <th>Notice Title</th>
                                    <th>Approval Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($school->notices->count() > 0)
                                    @foreach($school->notices as $notice)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td class="align-middle">{{ $notice->title }}</td>
                                            <td class="align-middle">
                                                @switch($notice->approved)
                                                    @case(\App\Models\Notice::APPROVED)
                                                    <span class="badge bg-success">Approved</span>
                                                    @break
                                                    @case(\App\Models\Notice::NOT_APPROVED)
                                                    <span class="badge bg-warning">Cancelled</span>
                                                    @break
                                                    @default
                                                    <span class="badge bg-warning">Pending</span>
                                                @endswitch
                                            </td>
                                            <td class="align-middle text-dark">
                                                @if($notice->approved == \App\Models\Notice::PENDING_APPROVAL)
                                                    <button class="btn btn-sm btn-success">Approve</button>
                                                    <button class="btn btn-sm btn-warning">Decline</button>
                                                @endif
                                                <button class="btn btn-sm btn-info">Preview</button>
                                                <button class="btn btn-sm btn-danger">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <em class="text-center">No notice in the record.</em>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </template>
            </div>
        </div>
    </section>

@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                tabIndex: 0,
                setActiveTab(index) {
                    this.tabIndex = index;
                }
            }));
        });
    </script>
@endsection
