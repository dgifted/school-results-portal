@extends('layouts.backoffice')
@section('title', 'Manage schools')
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold">Manage schools</h3>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="row">
            <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                <strong>Success!</strong> <br>
                {!! session('message') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">school</span>
                        View Schools
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered table-responsive" id="schoolsTable">
                        <thead class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>School Name</th>
                            <th>School Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($schools as $school)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <span class="d-inline-flex flex-sm-column flex-md-row justify-content-start">
                                        <img src="{{ $school->logo_path }}" alt="" width="50" height="50" class="me-sm-2 img-thumbnail">
                                        {{ $school->short_name }}
                                    </span>
                                </td>
                                <td>{{ ucwords($school->schoolType->name) }} school</td>
                                <td class="text-right">
                                    <a href="{{ route('backoffice.schools.edit', $school->ref_id) }}"
                                       class="btn btn-sm btn-primary" title="Edit school">
                                        <span class="material-symbols-outlined">edit</span>
                                    </a>
                                    <a href="{{ route('backoffice.schools.details', $school->ref_id) }}"
                                       class="btn btn-sm btn-info" title="View school">
                                        <span class="material-symbols-outlined m-0">info</span>
                                    </a>
{{--                                    <a href="javascript:void(0)" class="btn btn-sm btn-danger" title="Delete school">--}}
{{--                                        <span class="material-symbols-outlined">delete_forever</span>--}}
{{--                                    </a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot class="thead-light">
                        <tr>
                            <th>#</th>
                            <th>School Name</th>
                            <th>School Type</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#schoolsTable').DataTable({
                // "paging": true,
                // "lengthChange": true,
                // "searching": true,
                // "ordering": true,
                // "info": true,
                // "autoWidth": true,
                // "processing": true,
                // serverSide: true,
            });
        });
    </script>
@endsection
