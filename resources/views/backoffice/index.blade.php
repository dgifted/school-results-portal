@extends('layouts.backoffice')
@section('title', 'Backoffice - Dashboard')
@section('styles')
@stop

@section('dashboard-background')
    <div class="bg-primary pt-10 pb-21 w-100"></div>
@stop
@section('content')
    <div class="container-fluid mt-n22 px-6">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <!-- Page header -->
                <div>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="mb-2 mb-lg-0">
                            <h3 class="mb-0 text-white">Schools</h3>
                        </div>
                        <div>
                            <a href="{{ route('backoffice.onboard-school') }}"
                               class="btn btn-white d-inline-flex align-items-center">
                                <span class="material-symbols-outlined me-1">add</span>
                                <span>Onboard New School</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-12 mt-6">
                <!-- card -->
                <div class="card">
                    <!-- card body -->
                    <div class="card-body">
                        <!-- heading -->
                        <div
                            class="d-flex justify-content-between align-items-center mb-3"
                        >
                            <div>
                                <h4 class="mb-0">Schools</h4>
                            </div>
                            <div
                                class="icon-shape icon-md bg-light-primary text-primary rounded-2"
                            >
                                <i class="bi bi-briefcase fs-4"></i>
                            </div>
                        </div>
                        <!-- project number -->
                        <div>
                            <h1 class="fw-bold">{{ $schools->count() }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-12 mt-6">
                <!-- card -->
                <div class="card">
                    <!-- card body -->
                    <div class="card-body">
                        <!-- heading -->
                        <div
                            class="d-flex justify-content-between align-items-center mb-3"
                        >
                            <div>
                                <h4 class="mb-0">Classes</h4>
                            </div>
                            <div
                                class="icon-shape icon-md bg-light-primary text-primary rounded-2"
                            >
                                <i class="bi bi-list-task fs-4"></i>
                            </div>
                        </div>
                        <!-- project number -->
                        <div>
                            <h1 class="fw-bold">{{ $allClassesCount }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-12 mt-6">
                <!-- card -->
                <div class="card">
                    <!-- card body -->
                    <div class="card-body">
                        <!-- heading -->
                        <div
                            class="d-flex justify-content-between align-items-center mb-3"
                        >
                            <div>
                                <h4 class="mb-0">Users</h4>
                            </div>
                            <div
                                class="icon-shape icon-md bg-light-primary text-primary rounded-2"
                            >
                                <i class="bi bi-people fs-4"></i>
                            </div>
                        </div>
                        <!-- project number -->
                        <div>
                            <h1 class="fw-bold">{{ $allUsersCount }}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-12 col-12 mt-6">
                <!-- card -->
                <div class="card">
                    <!-- card body -->
                    <div class="card-body">
                        <!-- heading -->
                        <div
                            class="d-flex justify-content-between align-items-center mb-3"
                        >
                            <div>
                                <h4 class="mb-0">Subjects</h4>
                            </div>
                            <div
                                class="icon-shape icon-md bg-light-primary text-primary rounded-2"
                            >
                                <i class="bi bi-bullseye fs-4"></i>
                            </div>
                        </div>
                        <!-- project number -->
                        <div>
                            <h1 class="fw-bold">{{ $subjects->count() }}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row  -->
        <div class="row mt-6">
            <div class="col-md-12 col-12">
                <!-- card  -->
                <div class="card">
                    <!-- card header  -->
                    <div class="card-header bg-white py-4">
                        <h4 class="mb-0 text-capitalize">Recently added schools</h4>
                    </div>
                    <!-- table  -->
                    <div class="table-responsive">
                        <table class="table text-nowrap mb-0">
                            <thead class="table-light">
                            <tr>
                                <th>School name</th>
                                <th>No of Students</th>
                                <th>Status</th>
                                <th>Add on</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($schools as $school)
                                <tr>
                                    <td>
                                        <img
                                            src="{{ $school->logo_path }}"
                                            class="img-thumbnail"
                                            alt="{{ $school->short_name }}"
                                            width="30"
                                        />
                                        {{ $school->short_name }}
                                    </td>
                                    <td>
                                        N/A
                                    </td>
                                    <td>
                                        <span class="badge bg-success">Active</span>
                                    </td>
                                    <td>
                                        {{ $school->created_at->toFormattedDateString() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- card footer  -->
                    <div class="card-footer bg-white text-center">
                        <a href="{{ route('backoffice.schools') }}" class="link-primary">View all schools</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- row  -->
    </div>
@stop

@section('scripts')
    <script>

    </script>
@endsection
