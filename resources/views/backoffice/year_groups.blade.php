@extends('layouts.backoffice')
@section('title', 'Year groups')
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold text-capitalize">View & manage school year groups</h3>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="row">
            <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                <strong>Success!</strong> <br>
                {{ session('message') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Please fix the following error(s)</strong> <br>
                <ul>
                    @foreach($errors->all() as $msg)
                        <li>{{ $msg }}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-4 col-md-3 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">box_add</span>
                        Add year group
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('backoffice.year-groups') }}">
                        @csrf

                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input id="name"
                                   type="text"
                                   class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                   name="name"
                                   value=""
                                   required
                                   autofocus
                            />
                        </div>

                        <div class="mb-3">
                            <label for="short_code" class="form-label">Short code</label>
                            <input id="short_code"
                                   type="text"
                                   class="form-control {{ $errors->has('short_code') ? 'is-invalid' : '' }}"
                                   name="short_code"
                                   value=""
                                   autofocus
                                   required
                            />
                        </div>

                        <div class="mb-3">
                            <label for="school_type" class="form-label text-md-right">School type</label>
                            <select
                                id="school_type"
                                class="form-select {{ $errors->has('school_type') ? 'is-invalid' : '' }}"
                                name="school_type"
                                required
                            >
                                <option value="">Choose one</option>
                                @foreach($schoolTypes as $type)
                                    <option value="{{ $type->id }}">{{ ucwords($type->name) }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3 mt-10">
                            <button class="btn btn-primary" type="submit">Add year group</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-9 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">diversity_2</span>
                        Year Groups
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-sm" id="yearGroupsTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Short Code</th>
                            <th>Status</th>
                            <th>School Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($yearGroups as $group)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $group->name }}</td>
                                <td>{{ $group->short_code }}</td>
                                <td>
                                    <span class="badge bg-success">{{ $group->status_text }}</span>
                                </td>
                                <td>{{ ucwords($group->schoolType->name) }}</td>
                                <td>
                                    <a href="{{ route('backoffice.year-groups.edit', $group->ref_id) }}" class="btn btn-info btn-sm">
                                        <span class="material-symbols-outlined">edit</span>
                                    </a>
                                    <a href="#" class="btn btn-danger btn-sm">
                                        <span class="material-symbols-outlined">toggle_off</span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Short Code</th>
                            <th>Status</th>
                            <th>School Type</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap.min.js"></script>

    <script>
        $('#yearGroupsTable').DataTable();
    </script>
@endsection
