@extends('layouts.backoffice')
@section('title', 'Add a new school')
@section('styles')
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold">Onboard school</h3>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="row">
            <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                <strong>Success!</strong> <br>
                {!! session('message') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Please fix the following error(s)</strong> <br>
                <ul>
                    @foreach($errors->all() as $msg)
                        <li>{{ $msg }}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-6 col-md-6 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">school</span>
                        Add a new school
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('backoffice.onboard-school') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="mb-3">
                            <label for="name" class="form-label">School Name</label>
                            <input id="name"
                                   type="text"
                                   class="form-control {{ $errors->has('name')?'is-invalid' : '' }}"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required
                                   autofocus
                            />
                        </div>

                        <div class="mb-3">
                            <label for="address" class="form-label">School Address</label>
                            <input id="address"
                                   type="text"
                                   class="form-control"
                                   name="address"
                                   value="{{ old('address') }}"
                                   autofocus
                            />
                        </div>

                        <div class="mb-3">
                            <label for="type" class="form-label text-md-right">School Type</label>
                            <select
                                id="type"
                                class="form-select"
                                name="type"
                            >
                                <option value=""></option>
                                @foreach($schoolType as $type)
                                    <option value="{{ $type->id }}">{{ ucwords($type->name) }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="logo" class="form-label text-md-right">School Logo</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="logo"
                                           type="file"
                                           class="d-none"
                                           name="logo"
                                           accept="image/*"
                                    />
                                    <button
                                        type="button"
                                        class="btn btn-info"
                                        id="logoButton"
                                        onclick="document.getElementById('logo').click();"
                                    >Choose Image
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    <img src="" alt="" width="200" height="200" id="preview" class="d-none">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="banner" class="form-label text-md-right">School Banner</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="banner"
                                           type="file"
                                           class="d-none"
                                           name="banner"
                                           accept="image/*"
                                    />
                                    <button
                                        type="button"
                                        class="btn btn-info"
                                        id="bannerButton"
                                        onclick="document.getElementById('banner').click();"
                                    >Choose Image
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    <img src="" alt="" width="320" height="150" id="previewBanner" class="d-none">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3 mt-10">
                            <button class="btn btn-primary" type="submit">Add School</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">list</span>
                        Recently Added School
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-responsive table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>School Name</th>
                            <th>Address</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $count = 0
                        @endphp
                        @foreach($schools as $school)
                            <tr>
                                <td>{{ ++$count }}</td>
                                <td>
                                    <span class="d-inline-flex justify-content-between">
                                        <img src="{{ $school->logo_path }}" width="50" height="50"
                                             alt="{{ $school->name }}" class="me-2">
                                        {{ $school->short_name }}
                                    </span>

                                </td>
                                <td>{{ $school->address }}</td>
                                <td>{{ ucfirst($school->schoolType->name) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        const logoButton = document.getElementById('logo');
        const previewImage = document.getElementById('preview');
        const bannerButton = document.getElementById('banner');
        const previewBanner = document.getElementById('previewBanner');

        logoButton.addEventListener('change', function () {
            const logo = logoButton.files[0];
            const reader = new FileReader();
            reader.onload = function (e) {
                previewImage.removeAttribute('class');
                previewImage.src = e.target.result;
                previewImage.addAttribute('class', 'img-thumbnail');
            };
            reader.readAsDataURL(logo);
        });

        bannerButton.addEventListener('change', function () {
            const banner = bannerButton.files[0];
            const reader = new FileReader();
            reader.onload = function (e) {
                previewBanner.removeAttribute('class');
                previewBanner.src = e.target.result;
                previewBanner.addAttribute('class', 'img-thumbnail');
            };
            reader.readAsDataURL(banner);
        })
    </script>
@endsection
