@extends('layouts.backoffice')
@section('title', 'Update year group')
@section('styles')
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold">Update year group</h3>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="row">
            <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                <strong>Success!</strong> <br>
                {!! session('message') !!}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Please fix the following error(s)</strong> <br>
                <ul>
                    @foreach($errors->all() as $msg)
                        <li>{{ $msg }}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">edit</span>
                        Edit year group
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-8 col-lg-6">
                            <form method="POST" action="{{ route('backoffice.year-groups.edit', $yearGroup->ref_id) }}">
                                @csrf
                                @method('PATCH')

                                <div class="mb-3">
                                    <label for="name" class="form-label">Name</label>
                                    <input id="name"
                                           type="text"
                                           class="form-control"
                                           name="name"
                                           value="{{ $yearGroup->name }}"
                                           autofocus
                                    />
                                </div>

                                <div class="mb-3">
                                    <label for="short_code" class="form-label">Short code</label>
                                    <input id="short_code"
                                           type="text"
                                           class="form-control"
                                           name="short_code"
                                           value="{{ $yearGroup->short_code }}"
                                    />
                                </div>

                                <div class="mb-3">
                                    <label for="school_type" class="form-label text-md-right">School type</label>
                                    <select
                                        id="school_type"
                                        class="form-select"
                                        name="school_type"
                                        style="max-width: 300px;"
                                    >
                                        <option value=""></option>
                                        @foreach($schoolTypes as $type)
                                            <option value="{{ $type->id }}"
                                                    @if($type->id == $yearGroup->school_type_id) selected @endif>
                                                {{ ucwords($type->name) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="mb-3 mt-10">
                                    <button class="btn btn-primary" type="submit">Update YearGroup</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        const logoButton = document.getElementById('logo');
        const previewImage = document.getElementById('preview');
        const currentPreviewImage = document.getElementById('currentPreview');
        const bannerButton = document.getElementById('banner');
        const currentPreviewBanner = document.getElementById('currentBannerPreview');

        logoButton.addEventListener('change', function () {
            const logo = logoButton.files[0];
            const reader = new FileReader();
            reader.onload = function (e) {
                previewImage.classList.remove('d-none');
                currentPreviewImage.classList.add('d-none');
                previewImage.src = e.target.result;
                previewImage.classList.add('img-thumbnail');
            };
            reader.readAsDataURL(logo);
        });

        bannerButton.addEventListener('change', function () {
            const banner = bannerButton.files[0];
            const reader = new FileReader();
            reader.onload = function (e) {
                previewBanner.classList.remove('d-none');
                currentPreviewBanner.classList.add('d-none');
                previewBanner.src = e.target.result;
                previewBanner.classList.add('img-thumbnail');
            };
            reader.readAsDataURL(banner);
        })
    </script>
@endsection
