@extends('layouts.backoffice')
@section('title', 'Add school class')
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@stop

@section('content')
    <div x-cloak x-data="pageData">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="border-bottom pb-4 mb-4">
                    <h3 class="mb-0 fw-bold text-capitalize">Add &amp; Manage Cards</h3>
                </div>
            </div>
        </div>

        @if(session('message'))
            <div class="row">
                <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                    <strong>Info!</strong> <br>
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="row">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Please fix the following error(s)</strong> <br>
                    <ul>
                        @foreach($errors->all() as $msg)
                            <li>{{ $msg }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="row">

                    <div class="col-12">
                        <div class="card mb-3">
                            <div class="card-header d-flex align-items-center justify-content-between">
                                <h4 class="mb-0 text-uppercase">
                                    <span class="d-inline-flex align-items-center">
                                        <span class="material-symbols-outlined me-2">diversity_2</span>
                                        Scratch cards list
                                    </span>
                                </h4>
                                <button
                                    class="btn btn-sm btn-primary d-flex align-items-center"
                                    type="button" id="more"
                                    @click="handleCardGeneration"
                                ><span class="material-symbols-outlined">step_over</span>Generate Cards
                                </button>
                            </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table table-hover table-sm " id="scratchCardsTable">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Pin</th>
                                                <th>Usage</th>
                                                <th>Used by</th>
                                                <th>Used last</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($scratchCards as $card)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td style="max-width: 100px">
                                                        <div class="d-inline-flex align-items-center">
                                                            <input type="text" value="{{ $card->pin }}"
                                                                   class="border-0 bg-light p-2"
                                                                   id="{{ $card->pin }}" readonly
                                                            />
                                                            <button type="button"
                                                                    class="btn btn-sm btn-link copyPin"
                                                                    :data-pin-value="`{{ $card->pin }}`"
                                                                    @click="copyPIN($el.dataset.pinValue)"
                                                            >
                                                            <span class="material-symbols-outlined"
                                                                  title="Copy PIN to clipboard"
                                                            >content_copy</span>
                                                            </button>
                                                        </div>
                                                    </td>
                                                    <td>{{ $card->usage_count }}</td>
                                                    <td>
                                                        @if(!is_null($card->student))
                                                            {{$card->student->first_name . ' ' . $card->student->surname}}
                                                        @else
                                                            Not used
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!is_null($card->student))
                                                            {{$card->updated_at->toFormattedDateString()}}
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <button
                                                            type="button"
                                                            class="btn btn-danger btn-sm d-flex align-items-center"
                                                            :data-ref-id="`{{ $card->ref_id }}`"
                                                            @click="deleteScratchCard($el.dataset.refId)"
                                                            title="Delete Scratch Card"
                                                        ><span class="material-symbols-outlined">delete</span>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('vendors/clipboard.js-master/dist/clipboard.min.js')}}"></script>

    <script>
        $('#scratchCardsTable').DataTable();
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: false,
                error: null,
                csrfToken: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
                clipboardCopy: new ClipboardJS('.copyPin'),

                init() {
                    console.log('clipboard: ', this.clipboardCopy);
                },
                async handleCardGeneration() {
                    const qty = prompt('How many cards do you want to generate?');

                    if (qty !== null) {
                        try {
                            const resp = await fetch(`/backoffice/create-scratch-cards?quantity=${qty}`, {
                                headers: {'Content-Type': 'application/json'},
                            });

                            if (!resp.ok)
                                throw new Error('Error creating scratchcards');

                            const result = await resp.json();
                            alert(result.message);
                            location.reload();
                        } catch (e) {
                            alert(e.message);
                        }
                    } else {
                        alert('Operation cancelled');
                    }
                },

                async deleteScratchCard(cardRefId) {
                    Swal.fire({
                        title: 'Do you want to proceed? This operation is irreversible.',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        denyButtonText: 'No',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    })
                        .then(result => {
                            if (result.isConfirmed) {
                                this.sendCardDetailsForDelete(cardRefId);
                            } else {
                                alert('Operation cnancelled.');
                                location.reload();
                            }
                        });
                },
                async sendCardDetailsForDelete(cardRefId) {

                    try {
                        const resp = await fetch(`/backoffice/delete-scratch-card/${cardRefId}`, {
                            method: 'DELETE',
                            headers: {
                                'Content-Type': 'application/json',
                                'X-CSRF-TOKEN': this.csrfToken,
                            }
                        });

                        if (!resp.ok)
                            throw new Error('Could not delete card.');

                        const result = await resp.json();
                        alert('Scratch card deleted');
                        location.reload();
                    } catch (e) {
                        alert('Error while deleting scratch card. Reason: \n' + e);
                    }
                },

                copyPIN(pinValue) {
                    console.log('pinvalue: ' + pinValue);
                    this.clipboardCopy.on('success', (e) => {
                        console.log('EVENT: ' + e);
                        this.displayAlert('PIN copied successfully.');
                    });
                },

                displayAlert(message) {
                    Swal.fire({
                        title: message,
                        denyButtonText: 'Close',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    });
                }
            }));
        });
    </script>
@endsection
