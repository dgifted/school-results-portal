@php
    $route = \Route::currentRouteName()
@endphp

<nav class="navbar-vertical navbar">
    <div class="nav-scroller">
        <!-- Brand logo -->
        <a class="navbar-brand" href="javascript:void(0)">
            {{--            <img src="{{asset('assets/backoffice/images/brand/logo/logo.svg')}}" alt=""/>--}}
            <h2 class="text-white">{{ config('app.name') }}</h2>
        </a>
        <!-- Navbar nav -->
        <ul class="navbar-nav flex-column" id="sideNavbar">
            <li class="nav-item">
                <a class="nav-link has-arrow {{ $route == 'backoffice.home' ? 'active' : '' }}"
                   href="{{ route('backoffice.home') }}">
                    <i data-feather="home" class="nav-icon icon-xs me-2"></i>
                    Dashboard
                </a>
            </li>

            <li class="nav-item">
                <a
                    class="nav-link has-arrow collapsed"
                    href="javascript:void(0)"
                    data-bs-toggle="collapse"
                    data-bs-target="#navPages"
                    aria-expanded="false"
                    aria-controls="navPages"
                >
                    <i data-feather="layers" class="nav-icon icon-xs me-2"> </i>
                    School Management
                </a>

                <div id="navPages" class="collapse {{
                                        $route == 'backoffice.onboard-school' ||
                                        $route == 'backoffice.assign-admin' ||
                                        $route == 'backoffice.schools' ||
                                        $route == 'backoffice.schools.edit' ||
                                        $route == 'backoffice.schools.details' ? 'show' : ''
                                        }}" data-bs-parent="#sideNavbar">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link {{ $route == 'backoffice.onboard-school' ? 'active' : '' }}"
                               href="{{ route('backoffice.onboard-school') }}">
                                Onboard School
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ $route == 'backoffice.schools' ? 'active' : '' }}"
                               href="{{ route('backoffice.schools') }}">
                                Manage Schools
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="nav-item">
                <a
                    class="nav-link has-arrow collapsed"
                    href="javascript:void(0)"
                    data-bs-toggle="collapse"
                    data-bs-target="#navPages1"
                    aria-expanded="false"
                    aria-controls="navPages"
                >
                    <i data-feather="layers" class="nav-icon icon-xs me-2"> </i>
                    Class & YearGroup
                </a>

                <div id="navPages1" class="collapse {{
                                        $route == 'backoffice.year-groups' ||
                                        $route == 'backoffice.year-groups.edit' ||
                                        $route == 'backoffice.school-classes' ||
                                        $route == 'backoffice.school-classes.add'
                                         ? 'show' : ''
                                        }}" data-bs-parent="#sideNavbar">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link {{ $route == 'backoffice.year-groups' ? 'active' : '' }}"
                               href="{{ route('backoffice.year-groups') }}">
                                Year Groups
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                    $route == 'backoffice.school-classes' ||
                                    $route == 'backoffice.school-classes.add' ? 'active' : ''
                                }}"
                               href="{{ route('backoffice.school-classes') }}">
                                Classes
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="nav-item">
                <a
                    class="nav-link has-arrow collapsed"
                    href="javascript:void(0)"
                    data-bs-toggle="collapse"
                    data-bs-target="#navPages2"
                    aria-expanded="false"
                    aria-controls="navPages"
                >
                    <i data-feather="layers" class="nav-icon icon-xs me-2"> </i>
                    Scratch Cards
                </a>

                <div id="navPages2" class="collapse {{
                                        $route == 'backoffice.scratch-cards.manage'
                                         ? 'show' : ''
                                        }}" data-bs-parent="#sideNavbar">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link {{ $route == 'backoffice.scratch-cards.manage' ? 'active' : '' }}"
                               href="{{ route('backoffice.scratch-cards.manage') }}">
                                Manage Cards
                            </a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link {{ $route == 'backoffice.schools' ? 'active' : '' }}"--}}
{{--                               href="{{ route('backoffice.schools') }}">--}}
{{--                                Manage Cards--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>
