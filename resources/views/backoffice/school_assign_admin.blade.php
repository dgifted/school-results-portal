@extends('layouts.backoffice')
@section('title', 'Add school admin')
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="border-bottom pb-4 mb-4">
                <h3 class="mb-0 fw-bold">Create school admin for
                    <small class="text-info">{{ $school->short_name }}</small></h3>
            </div>
        </div>
    </div>

    @if(session('message'))
        <div class="row">
            <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                <strong>Success!</strong> <br>
                {{ session('message') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    @if($errors->any())
        <div class="row">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Please fix the following error(s)</strong> <br>
                <ul>
                    @foreach($errors->all() as $msg)
                        <li>{{ $msg }}</li>
                    @endforeach
                </ul>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-4 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">school</span>
                        Add school administrator
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('backoffice.assign-admin', $school->ref_id) }}"
                          enctype="multipart/form-data">
                        @csrf

                        <div class="mb-3">
                            <label for="first_name" class="form-label">First Name</label>
                            <input id="first_name"
                                   type="text"
                                   class="form-control {{ $errors->has('first_name')?'is-invalid' : '' }}"
                                   name="first_name"
                                   value="{{ old('first_name') }}"
                                   required
                                   autofocus
                            />
                        </div>

                        <div class="mb-3">
                            <label for="surname" class="form-label">Surname</label>
                            <input id="surname"
                                   type="text"
                                   class="form-control {{ $errors->has('surname')?'is-invalid' : '' }}"
                                   name="surname"
                                   value="{{ old('surname') }}"
                                   autofocus
                                   required
                            />
                        </div>

                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input id="email"
                                   type="email"
                                   class="form-control {{ $errors->has('email')?'is-invalid' : '' }}"
                                   name="email"
                                   value="{{ old('email') }}"
                                   autofocus
                                   required
                            />
                        </div>

                        <div class="mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input id="username"
                                   type="text"
                                   class="form-control {{ $errors->has('username')?'is-invalid' : '' }}"
                                   name="username"
                                   value="{{ old('username') }}"
                                   autofocus
                                   required
                            />
                        </div>

                        <div class="mb-3">
                            <label for="password" class="form-label">Admin Password</label>
                            <input id="password"
                                   type="text"
                                   class="form-control mb-1"
                                   name="password"
                                   value="{{ old('password') }}"
                                   autofocus
                            />
                            <button id="passwordGenerate"
                                    type="button"
                                    class="btn btn-primary btn-sm me-3"
                                    onclick="const pw = generateString(8); document.getElementById('password').value = pw;"
                            >Generate password
                            </button>
                            <small class="text-muted">If left empty, a password will be auto generated</small>
                        </div>

                        <div class="mb-3">
                            <label for="gender" class="form-label text-md-right">Gender</label>
                            <select
                                id="gender"
                                class="form-select"
                                name="gender"
                            >
                                <option value=""></option>
                                <option value="female">Female</option>
                                <option value="male">Male</option>

                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="passport" class="form-label text-md-right">Upload Passport</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input id="passport"
                                           type="file"
                                           class="d-none"
                                           name="passport"
                                           accept="image/*"
                                    />
                                    <button
                                        type="button"
                                        class="btn btn-info"
                                        id="passportButton"
                                        onclick="document.getElementById('passport').click();"
                                    >Choose Image
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    <img src="" alt="" width="200" height="200" id="preview" class="d-none">
                                </div>
                            </div>
                        </div>

                        <div class="mb-3 mt-10">
                            <button class="btn btn-primary" type="submit">Add admin</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                        <span class="material-symbols-outlined me-2">group</span>
                        School Admins
                    </h4>
                </div>
                <div class="card-body">
                    <table class="table table-hover table-sm" id="usersTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Password</th>
                            <th>Passport</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($school->users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ $user->first_name }} {{ $user->surname }}
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>{{ ucfirst($user->gender) }}</td>
                                <td>{{ $user->admin->plainPassword->plain_text }}</td>
                                <td>
                                    <img
                                        src="{{ $user->avatar_path }}"
                                        alt="{{ $user->first_name }}"
                                        width="40"
                                        class="img-thumbnail"
                                    />
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Password</th>
                            <th>Passport</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            @stop

            @section('scripts')
                <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
                <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap.min.js"></script>
                <script src="//cdn.datatables.net/1.13.4/js/dataTables.tableTools.min.js"></script>
                <script src="//cdn.datatables.net/1.13.4/js/dataTables.tableTools.bootstrap.min.js"></script>

                <script>
                    $('#usersTable').DataTable();
                    const passportButton = document.getElementById('passport');
                    const previewImage = document.getElementById('preview');

                    passportButton.addEventListener('change', function () {
                        const passport = passportButton.files[0];
                        const reader = new FileReader();
                        reader.onload = function (e) {
                            previewImage.classList.remove('d-none');
                            previewImage.src = e.target.result;
                            previewImage.classList.add('img-thumbnail');
                        };
                        reader.readAsDataURL(passport);
                    });

                    function generateString(length) {
                        var result = '';
                        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

                        for (var i = 0; i < length; i++) {
                            var randomIndex = Math.floor(Math.random() * characters.length);
                            result += characters.charAt(randomIndex);
                        }

                        return result;
                    }
                </script>
@endsection
