@extends('layouts.backoffice')
@section('title', 'School classes')
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@stop

@section('content')
    <div
        x-cloak
        x-data="pageData"
    >
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="border-bottom pb-4 mb-4">
                    <h3 class="mb-0 fw-bold text-capitalize">View & manage school classes</h3>
                </div>
            </div>
        </div>

        @if(session('message'))
            <div class="row">
                <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> <br>
                    {{ session('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        @if($errors->any())
            <div class="row">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Please fix the following error(s)</strong> <br>
                    <ul>
                        @foreach($errors->all() as $msg)
                            <li>{{ $msg }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="row">

                    <div class="col-12">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h4 class="mb-0 text-uppercase d-flex align-items-center justify-content-between">
                                    <span class="d-inline-flex align-items-center">
                                        <span class="material-symbols-outlined me-2">diversity_2</span>
                                        School Classes
                                    </span>
                                    <a :href="newClassButtonLink"
                                       class="btn btn-primary btn-sm d-inline-flex align-items-center">
                                        <span class="material-symbols-outlined me-1">add</span>
                                        Add class</a>
                                </h4>
                            </div>

                            <div class="card-body">
                                <form id="schoolSelectForm">
                                    <div class="form-group row align-items-center">
                                        <label for="school" class="col-12 col-md-3 text-start text-sm-end">Choose
                                            school:</label>
                                        <div class="col-12 col-md-9">
                                            <select
                                                class="form-select"
                                                id="school"
                                                name="school"
                                                x-model="selectedSchool"
                                                :change="getSchoolClasses()"
                                            >
                                                <option value="">Choose one</option>
                                                @foreach($schools as $school)
                                                    <option
                                                        value="{{ $school->ref_id }}">{{ $school->short_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <template x-if="!!classes">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="mb-0 text-uppercase d-flex align-items-center">
                                        <span class="material-symbols-outlined me-2">diversity_2</span>
                                        Classes
                                    </h4>
                                </div>
                                <div class="card-body">
                                    <table class="table table-hover table-sm" id="schoolClassesTable">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Class Name</th>
                                            <th>Year Group</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <template x-for="(cl, idx) in classes" :key="cl.id">
                                            <tr>
                                                <td x-text="idx+1"></td>
                                                <td x-text="`${cl.year_group.short_code} ${cl.name}`"></td>
                                                <td x-text="cl.year_group.short_code"></td>
                                                <td>
                                                    <span class="badge bg-success">Active</span>
                                                </td>
                                                <td>
                                                    <a href="javascript:void(0)" class="btn btn-info btn-sm">
                                                        <span class="material-symbols-outlined">info_i</span>
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm">
                                                        <span class="material-symbols-outlined">delete</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        </template>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Class Name</th>
                                            <th>Year Group</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap.min.js"></script>

    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: false,
                error: null,
                schoolTypes: null,
                yearGroups: null,
                schools: null,
                selectedSchool: null,
                classes: null,
                dataTableRef: null,
                newClassButtonLink: '#',

                async init() {
                    try {
                        this.schools = {!! json_encode($schools) !!};
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                    this.$watch('selectedSchool', () => {
                        this.newClassButtonLink = `/backoffice/school-classes/${this.selectedSchool}/add`;
                        this.getSchoolClasses();
                    });
                },

                getSchoolClasses() {
                    if (!!this.selectedSchool) {
                        // this.classes = null;
                        this.classes = this.schools.find(s => s.ref_id === this.selectedSchool).classes || null;

                        if (!this.dataTableRef) {
                            this.dataTableRef = $(document).ready(function () {
                                $('#schoolClassesTable').DataTable();
                            });
                        }
                    }
                },
            }));
        });
    </script>
@endsection
