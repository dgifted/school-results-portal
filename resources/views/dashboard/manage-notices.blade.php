@extends('layouts.master')
@section('title', 'Manage classes')
@section('styles')
@stop

@section('content')
    <div class="row"
         x-cloak
         x-data="classesData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2>View classes info</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <template x-if="!loading">
                                <div class="card-box table-responsive">
                                    <table
                                        id="datatable-checkbox"
                                        class="table table-striped table-bordered bulk_action table-sm"
                                        style="width:100%">
                                        <thead>
                                        <tr>
                                            <th @click="sort('year_group')">YearGroup</th>
                                            <th @click="sort('name')">Class Name</th>
                                            <th @click="sort('students_count')">Number of Students</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <template x-for="cl in pagedClasses" :key="cl.id">
                                            <tr>
                                                <td x-text="cl?.year_group?.name"></td>
                                                <td x-text="cl?.year_group?.short_code + cl?.name"></td>
                                                <td x-text="cl?.students_count"></td>
                                                <td>
                                                    <button type="button"
                                                            class="btn btn-info btn-sm"
                                                            @click="window.location.href = '/admin/edit-class/' + $el.dataset.class"
                                                            :data-class="cl?.ref_id"
                                                    >
                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </template>
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <p class="text-dark">
                                            Showing <span x-text="indexEnd"></span> of <span x-text="classes?.length"></span> items
                                        </p>
                                        <div>
                                            <button class="btn btn-sm btn-outline-info" @click="prevPage">Prev</button>
                                            <button class="btn btn-sm btn-outline-info" @click="nextPage">Next</button>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('classesData', () => ({
                loading: true,
                classes: null,
                sortCol: null,
                sortAsc: false,
                pageSize: 10,
                currPage: 1,
                indexStart: 0,
                indexEnd: 10,

                async init() {
                    try {
                        let resp = await fetch('ajax-routes/school-classes/all');
                        this.classes = await resp.json();
                    } catch (e) {

                    } finally {
                        this.loading = false;
                    }

                },
                nextPage() {
                    if((this.currPage * this.pageSize) < this.classes.length) this.currPage++;
                },
                prevPage() {
                    if (this.currPage > 1) this.currPage--;
                },
                sort(col) {
                    alert(`clicked ${col} column.`);
                    if(this.sortCol === col) this.sortAsc = !this.sortAsc;
                    this.sortCol = col;
                    this.classes.sort((a, b) => {
                        if(a[this.sortCol] < b[this.sortCol]) return this.sortAsc?1:-1;
                        if(a[this.sortCol] > b[this.sortCol]) return this.sortAsc?-1:1;
                        return 0;
                    });
                },
                get pagedClasses() {
                    if(this.classes) {
                        return this.classes.filter((row, index) => {
                            this.indexStart = (this.currPage-1)*this.pageSize;
                            this.indexEnd = this.currPage*this.pageSize;
                            if(index >= this.indexStart && index < this.indexEnd) return true;
                        })
                    } else return [];
                }
            }));
        });
    </script>
@endsection
