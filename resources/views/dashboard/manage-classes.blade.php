@extends('layouts.master')
@section('title', 'Manage classes')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Manage Classes</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="classesData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="d-flex justify-content-between align-items-center w-100 my-0">
                        <span>View classes info</span>
                        <a href="{{ route('classes.add') }}" class="btn btn-sm btn-info">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Add class
                        </a>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <template x-if="!loading">
                                <div class="card-box table-responsive">
                                    <table
                                        id="classesTable"
                                        class="table table-striped table-bordered bulk_action table-sm"
                                        style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>YearGroup</th>
                                            <th>Class Name</th>
                                            <th>Number of Students</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <template x-for="cl in classes" :key="cl.id">
                                            <tr>
                                                <td x-text="cl?.year_group?.name"></td>
                                                <td x-text="cl?.year_group?.short_code + cl?.name"></td>
                                                <td x-text="cl?.students_count"></td>
                                                <td>
                                                    <button type="button"
                                                            class="btn btn-info btn-sm"
                                                            @click="window.location.href = '/admin/edit-class/' + $el.dataset.class"
                                                            :data-class="cl?.ref_id"
                                                    >
                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </template>
                                        </tbody>
                                    </table>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('classesData', () => ({
                loading: true,
                classes: null,
                error: null,
                dataTableRef: null,

                async init() {
                    try {
                        let resp = await fetch('ajax-routes/school-classes/all');
                        this.classes = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
                $nextTick() {
                    if (!this.loading) {

                    }
                        // this.dataTableRef = new DataTable('#classesTable', {});
                },
            }));
        });
        const dTable = new DataTable('#classesTable', {});
    </script>
@endsection
