@extends('layouts.master')
@section('title', 'Manage year groups')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">View YearGroups</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>View year groups info</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12"
                             x-cloak
                             x-data="yearGroupsData"
                        >
                            <div class="card-box table-responsive">
                                <template x-if="loading">
                                    <div
                                        class="w-100 h-100 d-flex flex-column justify-content-center align-items-center">
                                        <div class="spinner-border text-secondary" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                </template>
                                <template x-if="!loading">
                                    <table
                                        id="datatable"
                                        class="table table-striped table-bordered bulk_action table-sm"
                                        style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Position</th>
                                            <th>Short Code</th>
                                            {{--                                            <th>Action</th>--}}
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <template x-for="yG in yearGroups" :key="yG.id">
                                            <tr>
                                                {{--                                                <td>&nbsp;</td>--}}
                                                <td x-text="yG.name"></td>
                                                <td>
                                                <span
                                                    x-text="yG.status_text.toUpperCase()"
                                                    class="badge badge-pill"
                                                    :class="yG.status_text == 'active' ? 'badge-success' : 'badge-danger'"
                                                >
                                                </span>
                                                </td>
                                                <td x-text="yG.id"></td>
                                                <td x-text="yG.short_code"></td>
                                                {{--                                                <td>--}}
                                                {{--                                                    <button type="button"--}}
                                                {{--                                                            class="btn btn-info btn-sm"--}}
                                                {{--                                                            @click="console.log($el.dataset.yearGroup)"--}}
                                                {{--                                                            :data-yearGroup="yG.id"--}}
                                                {{--                                                    >--}}
                                                {{--                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>--}}
                                                {{--                                                    </button>--}}
                                                {{--                                                </td>--}}
                                            </tr>
                                        </template>
                                        </tbody>
                                        <tfoot>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Position</th>
                                        <th>Short Code</th>
                                        </tfoot>
                                    </table>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('yearGroupsData', () => ({
                loading: true,
                yearGroups: null,
                error: null,

                async init() {
                    try {
                        let resp = await fetch('/admin/ajax-routes/year-groups/all');

                        if (!resp.ok)
                            throw new Error('We could not get year groups at the moment. Please try again.');

                        this.yearGroups = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
                $nextTick() {
                    new DataTable('#datatable', {});
                }
            }));
        });
    </script>
@endsection
