@extends('layouts.master')
@section('title', 'Result info')
@section('styles')
    <style>
        table {
            border-collapse: collapse;
            border: 1px solid black;
        }

        td {
            padding: 0.5rem 1em;
            border: 1px solid rgba(20, 30, 30, 0.5);
            font-size: 1rem;
        }

        .rotate-90 {
            display: inline-block;
            transform: rotate(-90deg) !important;
        }

        .xtra p {
            display: flex;
            align-items: flex-end;
            gap: 10px;
        }

        .xtra p .underline {
            flex-grow: 1;
            border-bottom: 1px solid #8b8b8b;
        }
    </style>
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Result Details</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="pageData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <div class="d-flex justify-content-between">
                        <h2 class="text-capitalize">
                        <span x-text="`${studentWithResult?.first_name} ${studentWithResult?.surname}'s`"
                              class="text-success text-uppercase"
                        ></span>
                            <span>termly result</span>
                        </h2>
                        <button
                            type="button"
                            class="btn btn-sm btn-outline-info"
                            @click="history.back()"
                        >Back to results
                        </button>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12"

                        >
                            <template x-if="!loading">
                                <div class="d-flex flex-column justify-content-between align-items-stretch">
                                    <div class="d-flex flex-column align-items-center w-100 text-uppercase">
                                        <h1 class="font-weight-bolder my-0"
                                            x-text="studentWithResult.user.school.short_name"></h1>
                                        <h3 class="font-weight-normal my-0"
                                            x-text="studentWithResult.user.school.address"></h3>
                                        <h3 class="font-weight-normal my-0">Termly Report.</h3>
                                    </div>

                                    <div class="mt-5 d-flex flex-column align-items-center w-100"
                                         style="font-size: 1rem;">
                                        <p class="font-weight-bold">Name of Student:
                                            <span
                                                x-text="`${studentWithResult.first_name} ${studentWithResult.surname}`"
                                                class="text-uppercase"
                                            ></span></p>
                                        <p class="font-weight-bold">Class:
                                            <span
                                                x-text="`${studentWithResult.student_class.year_group.short_code} ${studentWithResult.student_class.name}`"
                                                class="text-uppercase"
                                            ></span></p>
                                        <p class="font-weight-bold">Number in Class:
                                            <span class="text-uppercase"
                                                  x-text="`${studentWithResult.classSize}`"></span></p>
                                        <p class="font-weight-bold">Position in Class:
                                            <span>
                                                <em x-text="`${studentWithResult.position}`" style="margin-right: -5px;"></em>
                                                <template x-if="studentWithResult.position == 1"><i>st</i></template>
                                                <template x-if="studentWithResult.position == 2"><i>nd</i></template>
                                                <template x-if="studentWithResult.position == 3"><i>rd</i></template>
                                                <template x-if="studentWithResult.position > 3"><i>th</i></template>
                                            </span>
                                            Out of<span class="text-uppercase"
                                                        x-text="` ${studentWithResult.classSize}`"></span></p>
                                        {{--                                        <p class="font-weight-bold">Next term begins:--}}
                                        {{--                                            <span class="text-uppercase">30-07-2023</span></p>--}}

                                    </div>

                                    <table class="my-4">
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div>
                                                    <h4 class="text-uppercase font-weight-bolder">Keys to grade</h4>
                                                    <p class="d-flex justify-content-between my-0 font-weight-bold">
                                                        <span>A (Distinction)</span>
                                                        <span>70% & above</span>
                                                    </p>
                                                    <p class="d-flex justify-content-between my-0 font-weight-bold">
                                                        <span>C (Credit)</span>
                                                        <span>>=55% <70%</span>
                                                    </p>
                                                    <p class="d-flex justify-content-between my-0 font-weight-bold">
                                                        <span>P (Pass)</span>
                                                        <span>>=40% <54%</span>
                                                    </p>
                                                    <p class="d-flex justify-content-between my-0 font-weight-bold">
                                                        <span>F (Fail)</span>
                                                        <span><40%</span>
                                                    </p>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="rotate-90 font-weight-bold">Total score</span>
                                            </td>
                                            <td>
                                                <span class="rotate-90 font-weight-bold">Grade (ACPF)</span>
                                            </td>
                                            <td>
                                                <span class="rotate-90 font-weight-bold">Remarks</span>
                                            </td>
                                        </tr>
                                        <template x-for="(result, idx) in studentWithResult.results" :key="result.id">
                                            <tr>
                                                <td x-text="result.subject.name"></td>
                                                <td x-text="result.marks"></td>
                                                <td x-text="result.grade"></td>
                                                <td x-text="result.remarks"></td>
                                            </tr>
                                        </template>
                                        </tbody>
                                    </table>
                                    <div class="xtra py-5">
                                        <p><span>Form teacher's comments: </span>
                                            <span class="underline"></span>
                                        </p>
                                        <p><span> Form teacher's Name:</span>
                                            <span class="underline"></span>
                                        </p>
                                        <p><span>Form teacher's Signature:</span>
                                            <span class="underline"></span>
                                        </p>
                                        <p><span>Date: </span>
                                            <span class="underline"></span>
                                        </p>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                studentWithResult: null,
                studentUId: null,

                async init() {
                    const pathname = location.pathname;
                    const splicedPathname = pathname.split('/');
                    const uId = splicedPathname[splicedPathname.length - 1];
                    this.studentUId = uId;

                    try {
                        const resp = await fetch(`/admin/ajax-routes/students/${uId}/result-detail`);

                        if (!resp.ok)
                            throw new Error('Could not retrieve student\'s result.');

                        this.studentWithResult = await resp.json();

                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });
    </script>
@endsection
