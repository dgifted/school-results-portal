@extends('layouts.master')
@section('title', 'Update class info')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Edit Class</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="classData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Update student class info</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12 col-md-6"

                        >
                            <template x-if="!loading">
                                <form :action="`/admin/edit-class/${schoolRefId}`" method="post">
                                    @csrf
                                    @method('PATCH')

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="yearGroup">Choose YearGroup<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control"
                                                    id="yearGroup"
                                                    name="year_group_id"
                                                    required
                                            >
                                                <option>Choose option</option>
                                                <template x-for="yG in yearGroups;" :key="yG.ref_id">
                                                    <option :value="yG.id"
                                                            x-text="yG.name + ' [' + yG.short_code + ']'"
                                                            :selected="yG.id == schoolClass?.year_group?.id"
                                                    ></option>
                                                </template>
                                            </select>
                                            @error('year_group_id')
                                            <p class="text-danger pt-0 text-left form-error">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="name">Name<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                                   name="name"
                                                   id="name"
                                                   placeholder="ex. A"
                                                   required
                                                   :value="schoolClass.name.toUpperCase()"
                                            />
                                            @error('name')
                                            <p class="text-danger pt-0 text-left form-error">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="field item form-group mb-5">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="alias">Alias</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control"
                                                   name="alias"
                                                   id="alias"
                                                   placeholder="ex. JZ 1"
                                                   :value="schoolClass.alias"
                                            />
                                        </div>
                                    </div>

                                    <div class="ln_solid pt-3">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('classData', () => ({
                loading: true,
                error: null,
                yearGroups: null,
                schoolClass: null,
                schoolRefId: null,

                async init() {
                    const pathname = location.pathname;
                    const splicedPathname = pathname.split('/');
                    const ref = splicedPathname[splicedPathname.length - 1];
                    this.schoolRefId = ref;

                    try {
                        const [_yearGroups, _schoolClass] = await Promise.all([
                            await (await fetch('/admin/ajax-routes/year-groups/all')).json(),
                            await (await fetch(`/admin/ajax-routes/school-classes/${ref}`)).json()
                        ]);

                        this.yearGroups = _yearGroups;
                        this.schoolClass = _schoolClass;

                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });
    </script>
@endsection
