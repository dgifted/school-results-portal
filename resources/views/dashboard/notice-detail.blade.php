@extends('layouts.master')
@section('title', 'Add notice')
@section('styles')
    <link href="{{ asset('vendors/summernote/summernote-bs4.min.css') }}" rel="stylesheet"/>
    <style type="text/css">
        .note-editable.card-block {
            min-height: 300px;
        }
    </style>
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Notice Detail</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div x-cloak x-data="pageData">
        @if(session('message'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                        <strong>Info!</strong> <br>
                        {{ session('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="d-flex justify-content-between align-items-center w-100 my-0">
                            <span class="text-dark">Notice detail</span>
                            <span>
                                <a class="btn btn-sm btn-info" href="javascript:void(0);">Edit notice</a>
                                <a class="btn btn-sm btn-primary" href="{{ route('notices.add') }}">Add notice</a>
                            </span>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row mb-4">
                            <div class="col-12">
                                <h3 x-text="notice.title"></h3>
                                <hr/>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div x-html="notice.content"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('vendors/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#content').summernote();
        });

        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                notice: null,

                async init() {
                    const pathname = location.pathname;
                    const splicedPathname = pathname.split('/');
                    const ref = splicedPathname[splicedPathname.length - 1];

                    try {
                        const resp = await fetch(`/admin/ajax-routes/notices/${ref}`);

                        if (!resp.ok)
                            throw new Error('Could not get notice details at the moment.');

                        this.notice = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });

    </script>
@endsection
