@extends('layouts.master')
@section('title', 'Add notice')
@section('styles')
    <link href="{{ asset('vendors/summernote/summernote-bs4.min.css') }}" rel="stylesheet"/>
    <style type="text/css">
        .note-editable.card-block {
            min-height: 300px;
        }
    </style>
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Add Notice</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div>
        @if(session('message'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                        <strong>Success!</strong> <br>
                        {{ session('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Add school notice</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <form action="{{ route('notices.add') }}" method="post">
                            @csrf

                            <div class="form-group row mb-4">
                                <label for="title" class="col-12 col-md-4 text-md-right">
                                    Notice title<span class="text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-8">
                                    <input type="text"
                                           class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                                           name="title"
                                           id="title" required value="{{ old('title') }}"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="content" class="col-12 col-md-4 text-md-right">
                                    Notice message<span class="text-danger">*</span>
                                </label>
                                <div class="col-12 col-md-8">
                                            <textarea type="text"
                                                      class="{{ $errors->has('title') ? 'is-invalid' : '' }}"
                                                      name="content"
                                                      id="content"
                                            ></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-sm">Post Notice</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('vendors/summernote/summernote-bs4.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('#content').summernote();
        });
        // document.addEventListener('alpine:init', () => {
        //     Alpine.data('pageData', () => ({
        //         loading: true,
        //         error: null,
        //         notices: null,
        //
        //         async init() {
        //             try {
        //                 const resp = await fetch(`/admin/ajax-routes/notices`);
        //
        //                 if (!resp.ok)
        //                     throw new Error('Could not fetch school notices.');
        //
        //                 this.notices = await resp.json();
        //                 console.log('notices: ', this.notices);
        //             } catch (e) {
        //                 this.error = e;
        //             } finally {
        //                 this.loading = false
        //             }
        //         }
        //     }));
        // });
        //Initialize the summernote plugin

    </script>
@endsection
