@extends('layouts.master')
@section('title', 'Add class')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Add Class</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    @if($errors->any())
        <div class="row">
            <div class="col-12">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <em>Please fix the following errors.</em><br/>
                    <ul>
                        @foreach($errors->all() as $msg)
                            <li>{{ $msg }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="classData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <div class="div d-flex justify-content-between align-items-center">
                        <h2 class="">
                            Create student class
                        </h2>

                        <form class="d-inline" method="post"
                              action="{{ route('classes.import') }}"
                              @submit="submitAndUploadFile"
                              enctype="multipart/form-data"
                              x-ref="fileForm"
                        >
                            @csrf
                            <input
                                type="file"
                                name="file"
                                class="d-none"
                                id="filePicker"
                                x-ref="filePicker"
                                @change="filePicked"
                            />
                            <button type="button" class="btn btn-sm btn-info"
                                    @click="openFilePicker"
                            >
                                <i class="fa fa-download" aria-hidden="true"></i> Import from excel sheet
                            </button><button
                                class="btn btn-sm btn-link-light"
                                @click="popInstructions" type="button"
                                title="Read instructions on how to format your file."
                            >
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12 col-md-6"

                        >
                            <template x-if="!loading">
                                <form action="{{ route('classes.add.post') }}" method="post">
                                    @csrf

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="yearGroup">Choose YearGroup<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control"
                                                    id="yearGroup"
                                                    name="year_group_id"
                                                    required
                                                    value="{{ old('year_group_id') }}"
                                            >
                                                <option>Choose option</option>
                                                <template x-for="yG in yearGroups;"
                                                          :key="yG.ref_id">
                                                    <option :value="yG.id"
                                                            x-text="yG.name + ' [' + yG.short_code + ']'"></option>
                                                </template>
                                            </select>
                                            @error('year_group_id')
                                            <p class="text-danger pt-0 text-left form-error">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="name">Name<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                                   name="name"
                                                   id="name"
                                                   placeholder="ex. A"
                                                   required
                                                   value="{{ old('name') }}"
                                            />
                                            @error('name')
                                            <p class="text-danger pt-0 text-left form-error">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="field item form-group mb-5">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="alias">Alias</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control"
                                                   name="alias"
                                                   id="alias"
                                                   placeholder="ex. JZ 1"
                                            />
                                        </div>
                                    </div>

                                    <div class="ln_solid pt-3">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Submit</button>
                                                <button type='reset' class="btn btn-success" onclick="">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('dashboard.includes.modals.classes-import-instructions')
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('classData', () => ({
                loading: true,
                error: null,
                yearGroups: null,
                pickedFile: null,

                async init() {
                    try {
                        const resp = await fetch('/admin/ajax-routes/year-groups/all');

                        if (!resp.ok)
                            throw new Error('We could not get the school year groups.');

                        this.yearGroups = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
                popInstructions() {
                    $('#classesImportInstructions').modal('show');
                },
                openFilePicker() {
                    this.$refs.filePicker.click();
                },
                filePicked() {
                    Swal.fire({
                        title: 'Do you want to proceed ? Already added classes will be removed.',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        denyButtonText: 'No',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    })
                        .then((result) => {
                            console.log('Alert result: ', result);
                            if (result.isConfirmed) {
                                this.submitAndUploadFile();
                            } else {
                                window.location.reload();
                            }
                        });
                },
                submitAndUploadFile() {
                    const form = this.$refs.fileForm;
                    form.submit();
                }
            }));
        });
    </script>
@endsection
