@extends('layouts.master')
@section('title', 'User profile')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Admin Profile</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="pageData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Account & Password.</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <ol>
                                @foreach($errors->all() as $msg)
                                    <li>{{ $msg }}</li>
                                @endforeach
                            </ol>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12 col-md-6"

                        >
                            <template x-if="!loading">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#tab_content1" id="profile-tab" role="tab" data-toggle="tab"
                                               aria-expanded="true">Account Info</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content2" role="tab" id="password-tab" data-toggle="tab"
                                               aria-expanded="false">
                                                Change password
                                                @if(session('type') == 'danger' || session('type') == 'warning')
                                                    <i class="fa fa-exclamation-circle text-{{ session('type') }}"
                                                       aria-hidden="true"></i>
                                                @endif
                                            </a>

                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane active " id="tab_content1"
                                             aria-labelledby="profile-tab">
                                            <form action="{{ route('profile') }}" method="post"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                @method('PATCH')

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="first_name">First name</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input class="form-control"
                                                               name="first_name"
                                                               id="first_name"
                                                               type="text"
                                                               :value="profileData.first_name"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="surname">Surname</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input class="form-control"
                                                               name="surname"
                                                               id="surname"
                                                               type="text"
                                                               :value="profileData.surname"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="email">Email</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input class="form-control"
                                                               name="email"
                                                               id="email"
                                                               type="email"
                                                               :value="profileData.email"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="username">Username</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input class="form-control"
                                                               name="username"
                                                               id="username"
                                                               type="text"
                                                               :value="profileData.admin?.username"
                                                        />
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="gender">Gender</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <select class="form-control"
                                                                name="gender"
                                                                id="gender"
                                                        >
                                                            <option value="">Select</option>
                                                            <option value="male"
                                                                    :selected="profileData?.gender == 'male'">Male
                                                            </option>
                                                            <option value="female"
                                                                    :selected="profileData?.gender == 'female'">
                                                                Female
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="avatar">Profile picture</label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input class="d-none"
                                                               name="avatar"
                                                               id="avatar"
                                                               type="file"
                                                               accept="image/*"
                                                               @change="displayPictureFile"
                                                               x-ref="avatar"
                                                        />
                                                        <img class="img-thumbnail"
                                                             width="120"
                                                             id="initial-image"
                                                             :src="profileData?.avatar_path"
                                                             :alt="profileData?.first_name"
                                                        />
                                                        <img class="img-thumbnail d-none"
                                                             width="120"
                                                             id="preview-image"
                                                             :src="profileData?.first_name"
                                                        />
                                                        <button class="btn btn-primary btn-sm ml-1"
                                                                type="button"
                                                                @click="$refs.avatar.click()"
                                                                x-text="!!profileData.avatar ? 'Change profile picture' : 'Upload profile picture'"
                                                        ></button>
                                                    </div>
                                                </div>

                                                <div class="ln_solid pt-3">
                                                    <div class="form-group">
                                                        <div class="col-md-6 offset-md-3">
                                                            <button type='submit' class="btn btn-primary">Submit
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                             aria-labelledby="password-tab">
                                            <form action="{{ route('profile.changePassword') }}" method="post">
                                                @csrf
                                                @method('PATCH')

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="old_password">Current password <span
                                                            class="text-danger">*</span></label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input
                                                            class="form-control {{ $errors->has('old_password') ? 'is-invalid' : '' }}"
                                                            name="old_password"
                                                            id="old_password"
                                                            type="password"
                                                            required
                                                            value=""
                                                        />
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="new_password">New password <span
                                                            class="text-danger">*</span></label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input
                                                            class="form-control {{ $errors->has('new_password') ? 'is-invalid' : '' }}"
                                                            name="new_password"
                                                            id="new_password"
                                                            type="password"
                                                            required
                                                            value=""
                                                        />
                                                    </div>
                                                </div>

                                                <div class="field item form-group mb-4">
                                                    <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                           for="new_password_confirmation">Confirm password <span
                                                            class="text-danger">*</span></label>
                                                    <div class="col-md-6 col-sm-6">
                                                        <input
                                                            class="form-control {{ $errors->has('new_password_confirmation') ? 'is-invalid' : '' }}"
                                                            name="new_password_confirmation"
                                                            id="new_password_confirmation"
                                                            type="password"
                                                            required
                                                            value=""
                                                        />
                                                    </div>
                                                </div>

                                                <div class="ln_solid pt-3">
                                                    <div class="form-group">
                                                        <div class="col-md-6 offset-md-3">
                                                            <button type='submit' class="btn btn-primary">Submit
                                                            </button>
{{--                                                            | <a href="{{ route('password.reset') }}" class="btn btn-link">Forgor password</a>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                profileData: null,

                async init() {
                    try {
                        const resp = await fetch('/admin/ajax-routes/profile-data');

                        if (!resp.ok)
                            throw new Error('Error fetching profile data.');

                        this.profileData = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
                displayPictureFile(event) {
                    const originalPicture = document.getElementById('initial-image');
                    const newPicture = document.getElementById('preview-image');

                    const profilePicture = event.target.files[0];
                    console.log('File: ', profilePicture);

                    const reader = new FileReader();
                    reader.onload = function (evt) {
                        originalPicture.classList.add('d-none');
                        newPicture.src = evt.target.result;
                        newPicture.classList.remove('d-none');
                    }
                    reader.readAsDataURL(profilePicture);
                }
            }));
        });
    </script>
@endsection
