@extends('layouts.master')
@section('title', 'Create subject')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Create Subject</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row" x-clock x-data="pageData">
        <div class="col-12">
            <div class="x_panel">
                <div class="x_title">
                    <div class="div d-flex justify-content-between align-items-center">
                        <h2>Create subject</h2>

                        <form class="d-inline" method="post"
                              action="{{ route('subjects.import') }}"
                              enctype="multipart/form-data"
                              x-ref="fileForm">
                            @csrf
                            <input
                                type="file"
                                name="file"
                                class="d-none"
                                id="filePicker"
                                x-ref="filePicker"
                                @change="filePicked"
                            />
                            <button type="button" class="btn btn-sm btn-info"
                                    @click="openFilePicker"
                            >
                                <i class="fa fa-download" aria-hidden="true"></i> Import from excel sheet
                            </button>
                            <button
                                class="btn btn-sm btn-link-light"
                                @click="popInstructions" type="button"
                                title="Read instructions on how to format your file."
                            >
                                <i class="fa fa-question-circle" aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <form action="{{ route('subjects.create') }}" method="post">
                                @csrf

                                <div class="field item form-group mb-4">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align" for="name">Subject Name<span
                                            class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                               name="name"
                                               id="name"
                                               placeholder="ex. English"
                                               value="{{ old('name') }}"
                                               required
                                        />
                                        @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="field item form-group mb-4">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align" for="code">Subject
                                        Code</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}"
                                               name="code"
                                               id="code"
                                               placeholder="ex. Eng."
                                               value="{{ old('code') }}"
                                        />
                                        @error('code')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="ln_solid pt-3">
                                    <div class="form-group">
                                        <div class="col-md-6 offset-md-3">
                                            <button type='submit' class="btn btn-primary">Submit</button>
                                            <button type='reset' class="btn btn-success">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('dashboard.includes.modals.subjects-import-instructions')
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                popInstructions() {
                    $('#subjectsImportInstructions').modal('show');
                },
                openFilePicker() {
                    this.$refs.filePicker.click();
                },
                filePicked() {
                    Swal.fire({
                        title: 'Do you want to proceed? Already added subjects will be removed.',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        denyButtonText: 'No',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    })
                        .then((result) => {
                            if (result.isConfirmed) {
                                this.submitAndUploadFile();
                            } else {
                                window.location.reload();
                            }
                        });
                },
                submitAndUploadFile() {
                    const form = this.$refs.fileForm;
                    form.submit();
                }
            }));
        });
    </script>
@endsection
