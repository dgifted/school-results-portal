@extends('layouts.master')
@section('title', 'Manage students')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Manage Students</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="d-flex justify-content-between align-items-center w-100 my-0">
                        <span>View students info</span>
                        <a href="{{ route('students.add') }}" class="btn btn-sm btn-info">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Add student</a>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>
                    <div class="row">
                        <div class="col-sm-12">
                            <template x-if="!loading">
                                <div class="card-box table-responsive">
                                    <table id="datatable-checkbox"
                                           class="table table-striped table-bordered table-sm bulk_action"
                                           style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Student Name</th>
                                            <th>Student ID</th>
                                            <th>Class</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <template x-for="(st, idx) in students" :key="st.id">
                                                <tr>
                                                    <td x-text="idx+1"></td>
                                                    <td x-text="`${st.first_name} ${st.surname}`"></td>
                                                    <td x-text="st.reg_no"></td>
                                                    <td x-text="`${st.student_class.year_group.short_code} ${st.student_class.name}`"></td>
                                                    <td>
                                                        <span class="badge py-2 px-3"
                                                              :class="st.user.status_text == 'active' ? 'badge-success' : 'badge-danger'"
                                                              x-text="st.user.status_text.toUpperCase()"
                                                        ></span>
                                                    </td>
                                                    <td>
                                                        <button type="button"
                                                                class="btn btn-sm btn-info"
                                                                :title="`View ${st.first_name}'s detail`"
                                                                :data-uid="st.unique_id"
{{--                                                                @click="location.href=`/admin/edit-student/${$el.dataset.uid}`"--}}
                                                                @click="location.href=`/admin/student-detail/${$el.dataset.uid}`"
                                                        >
                                                            <i class="fa fa-list" aria-hidden="true"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </template>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Student Name</th>
                                            <th>Student ID</th>
                                            <th>Class</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                students: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/students/all`);

                        if (!resp.ok)
                            throw new Error('Could not get students.');

                        this.students = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });
    </script>
@endsection
