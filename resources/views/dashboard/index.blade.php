@extends('layouts.master')
@section('title', 'Dashboard')
@section('styles')

@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Dashboard</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div
        x-cloak
        x-data="pageData"
    >
        <template x-if="loading">
            @include('dashboard.includes.loading')
        </template>

        <template x-if="!loading">
            <div>
                <div class="row">
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6">
                        <div class="tile-stats">
                            <div class="icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="count" x-text="payload?.students"></div>
                            <h3>Total students</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-book"></i></div>
                            <div class="count" x-text="payload?.subjects"></div>
                            <h3>Enlisted Subjects</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6">
                        <div class="tile-stats">
                            <div class="icon">
                                <i class="fa fa-home"></i>
                            </div>
                            <div class="count" x-text="payload?.classes"></div>
                            <h3>Enlisted classes</h3>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-list-alt"></i></div>
                            <div class="count" x-text="payload?.declaredResults"></div>
                            <h3>Declared results</h3>
                        </div>
                    </div>
                </div>
                <div class="row" style="display: block;">
                    <div class="col-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Recent declared Results</h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Student Name</th>
                                        <th>Reg Number</th>
                                        <th>Class</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <template x-if="payload?.declaredResultsList?.length">
                                        <template x-for="(result, idx) in payload?.declaredResultsList">
                                            <tr>
                                                <td x-text="idx+1"></td>
                                                <td x-text="`${result.student.first_name} ${result.student.surname}`"></td>
                                                <td x-text="result.student.reg_no"></td>
                                                <td x-text="`${result.student.student_class.year_group.short_code} ${result.student.student_class.name}`"></td>
                                                <td x-text="formatDate(result.created_at)"></td>
                                                <td>
                                                    <span class="badge badge-success my-1 mx-2">
                                                        Active
                                                    </span>
                                                </td>
                                            </tr>
                                        </template>
                                    </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </template>

    </div>

@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: false,
                error: null,
                payload: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/dashboard/payload`);

                        if (!resp.ok)
                            throw  new Error('Could not get necessary information.');

                        this.payload = await resp.json();
                        // console.log('Payload: ', this.payload.groupedResults);
                        // for (groupedResult in this.payload.groupedResults) {
                        //     console.log('grouped result, ', groupedResult);
                        //     // for (gR in groupedResult) {
                        //     //     log('gR: ', gR);
                        //     // }
                        // }
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
                formatDate(dateString) {
                    const date = Date.parse(dateString);
                    const dDate = new Date(date);
                    return dDate.toISOString().substring(0, 10)
                }
            }));
        });
    </script>
@endsection
