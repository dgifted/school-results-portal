@extends('layouts.master')
@section('title', 'Student\'s details')
@section('styles')
    <style>
        .user_data > li {
            margin-bottom: 1rem !important;
        }

        .user_data > li > * {
            font-size: 15px;
        }
    </style>
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Student Detail</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="pageData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        <span x-text="`${student?.first_name} ${student?.surname}`" class="text-uppercase"></span><span>'s details</span>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Please fix the following errors.</strong>
                            <ul>
                                @foreach($errors->all() as $msg)
                                    <li>{{ $msg }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <template x-if="!!student">
                        <div class="row">
                            <div class="col-md-3 col-sm-3  profile_left">
                                <div class="profile_img">
                                    <div id="crop-avatar">
                                        <!-- Current avatar -->
                                        <img class="img-responsive avatar-view img-thumbnail"
                                             style="max-width: 200px;"
                                             :src="`${student.user.avatar_path}`"
                                             :alt="`${student.first_name} ${student.surname}`"
                                             :title="`${student.first_name} ${student.surname}`">
                                    </div>
                                </div>
                                <h3 x-text="`${student.first_name} ${student.surname}`"></h3>

                                <ul class="list-unstyled user_data">
                                    <li>
                                        <i class="fa fa-bars user-profile-icon"></i>
                                        <strong>Class:</strong>
                                        <span class=""
                                              x-text="`${student.student_class.year_group.short_code} ${student.student_class.name}`"></span>
                                    </li>

                                    <li>
                                        <i class="fa fa-venus-mars user-profile-icon"></i>
                                        <strong>Gender:</strong>
                                        <span class="text-capitalize"
                                              x-text="`${student.user?.gender || 'N/A'}`"></span>
                                    </li>

                                    <li class="m-top-xs">
                                        <i class="fa fa-cube user-profile-icon"></i>
                                        <strong>Student ID:</strong>
                                        <span class="" x-text="`${student.reg_no}`"></span>
                                    </li>
                                </ul>

                                <form class=""
                                      :action="`/admin/student/${studentUId}/upload-passport`"
                                      method="post"
                                      x-ref="passportUploadForm"
                                      enctype="multipart/form-data"
                                >
                                    @csrf

                                    <input type="file"
                                           accept="image/*"
                                           class="d-none"
                                           x-ref="passportUpload"
                                           name="passport"
                                           @change="$refs.passportUploadForm.submit()"
                                           id="passport"/>
                                    <button
                                        type="button"
                                        class="btn btn-success"
                                        @click="$refs.passportUpload.click()"
                                    >
                                        <template x-if="!student.user.avatar">
                                        <span><i class="fa fa-upload"
                                                 aria-hidden="true"></i> Upload passport</span>
                                        </template>
                                        <template x-if="!!student.user.avatar">
                                        <span><i class="fa fa-file-image-o"
                                                 aria-hidden="true"></i> Change passport</span>
                                        </template>
                                    </button>
                                </form>

                                <br/>
                                <a
                                    class="btn btn-primary"
                                    :data-uid="student.unique_id"
                                    :href="`/admin/edit-student/${$el.dataset.uid}`"
                                >
                                    <i class="fa fa-edit m-right-xs"></i> Edit Profile</a>

                                <br/>
                            </div>
                            <div class="col-md-9 col-sm-9 ">
                                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                        <li role="presentation" class="">
                                            <a href="#tab_content1" id="home-tab"
                                               role="tab" data-toggle="tab"
                                               aria-expanded="true">Subjects</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content2" role="tab" id="profile-tab"
                                               data-toggle="tab" aria-expanded="false">Declared
                                                Results</a>
                                        </li>
                                        <li role="presentation" class="">
                                            <a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab"
                                               aria-expanded="false">Profile</a>
                                        </li>
                                    </ul>
                                    <div id="myTabContent" class="tab-content">
                                        <div role="tabpanel" class="tab-pane active " id="tab_content1"
                                             aria-labelledby="home-tab">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Subject name</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <template x-if="!!student.student_class.subject_combos.length">
                                                    <template
                                                        x-for="(sCombo, idx) in student.student_class.subject_combos">
                                                        <tr>
                                                            <td x-text="idx+1"></td>
                                                            <td x-text="sCombo.subject.name"></td>
                                                        </tr>
                                                    </template>
                                                </template>

                                                <template x-if="!student.student_class.subject_combos.length">
                                                    <tr>
                                                        <td colspan="2">
                                                            <strong>No subject assigned to this student's
                                                                class.</strong>
                                                        </td>
                                                    </tr>
                                                </template>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                             aria-labelledby="profile-tab">
                                            <template x-if="!declaredResults.length">
                                                <h2>No declared results for this student.</h2>
                                            </template>

                                            <template x-if="!!declaredResults.length">
                                                <ul class="list-group">
                                                    <template x-for="(result, idx) in declaredResults"
                                                              :key="result.id.toString()">
                                                        <li class="list-group-item">
                                                            <strong
                                                                x-text="result.subject.name"
                                                                class="d-inline-flex"
                                                                style="min-width: 180px"
                                                            ></strong>
                                                            <span class="font-weight-bolder"
                                                                  x-text="`${result.marks} %`"></span>
                                                        </li>
                                                    </template>
                                                </ul>
                                            </template>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                                             aria-labelledby="profile-tab">
                                            <h2>Feature coming soon.</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </template>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                student: null,
                studentUId: null,
                formattedDOB: null,
                declaredResults: null,

                async init() {
                    const pathname = location.pathname;
                    const splicedPathname = pathname.split('/');
                    const uId = splicedPathname[splicedPathname.length - 1];
                    this.studentUId = uId;

                    try {
                        const [_, __] = await Promise.all([
                            await (await fetch(`/admin/ajax-routes/students/${uId}`)).json(),
                            await (await fetch(`/admin/ajax-routes/students/${uId}/results`)).json(),
                        ]);

                        this.student = _;
                        this.declaredResults = __;

                        if (!!this.student && (!!this.student.dob)) {
                            const date = Date.parse(this.student.dob)
                            const cDate = new Date(date);
                            this.formattedDOB = cDate.toISOString().substring(0, 10);
                        }

                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
            }));
        });
    </script>
@endsection
