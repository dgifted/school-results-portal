@extends('layouts.master')
@section('title', 'Add result')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Add Result</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <h2>Declare result</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <template x-if="!loading">
                                <form
                                    method="post"
                                    id="resultForm"
                                    @submit.prevent="submitForm($event)"
                                >
                                    @csrf

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align"
                                               for="class">Class<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select
                                                class="form-control" id="class"
                                                name="school_class_id"
                                                required
                                                x-ref="class"
                                                @change="getStudentForClass($event.target.value)"
                                            >
                                                <option>Choose..</option>
                                                <template x-for="cl in allClasses" :key="cl.id">
                                                    <option
                                                        x-text="`${cl.year_group.short_code} ${cl.name}`"
                                                        :value="cl.ref_id"
                                                    ></option>
                                                </template>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="student">Student
                                            name<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select
                                                class="form-control"
                                                id="student"
                                                name="student_id"
                                                required
                                                x-ref="student"
                                                @change="getSubjectForClass($refs.class.value, $el.value)"
                                            >
                                                <option value="">Choose..</option>
                                                <template x-for="std in classStudents" :key="std.id">
                                                    <option
                                                        x-text="`${std.first_name} ${std.surname}`"
                                                        :value="std.unique_id"
                                                    ></option>
                                                </template>
                                            </select>
                                        </div>
                                    </div>

                                    <template x-if="(showSubjects)">
                                        <div>
                                            <div class="field item form-group" x-show="!selectedStudentResults?.length">
                                                <label
                                                    class="col-form-label col-md-3 col-sm-3  label-align">Subjects</label>
                                            </div>
                                            <hr class="mt-0">
                                            <template x-if="!selectedStudentResults?.length">
                                                <template x-for="sC in subjectCombos" :key="sC?.id">
                                                    <div class="field item form-group mb-4">
                                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                               :for="sC?.subject?.name"
                                                        ><span x-text="sC?.subject.name"></span>
                                                            <span class="required">*</span></label>
                                                        <div class="col-md-6 col-sm-6">
                                                            <input class="form-control"
                                                                   :name="sC?.subject?.id"
                                                                   :id="sC?.subject?.name"
                                                                   min="0"
                                                                   max="100"
                                                                   placeholder="Enter marks out of 100"
                                                                   required
                                                                   type="number"
                                                            />
                                                        </div>
                                                    </div>
                                                </template>
                                            </template>
                                            <template x-if="selectedStudentResults?.length">
                                                <div class="field item form-group mb-4">
                                                    <div class="col-md-6 offset-md-3">
                                                        <h4 class="text-danger">Result already declared for this
                                                            student.</h4>
                                                        <a :href="`/admin/edit-result/${studentUId}`"
                                                           class="btn btn-outline-secondary btn-sm">Edit students
                                                            result</a>
                                                    </div>
                                                </div>
                                            </template>
                                        </div>
                                    </template>

                                    <div class="ln_solid pt-3">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Submit</button>
                                                <button type='reset' class="btn btn-success">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                isFetching: false,
                showSubjects: false,
                error: null,
                allClasses: null,
                subjectCombos: null,
                classStudents: null,
                selectedStudentResults: null,
                studentUId: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/school-classes/all`);

                        if (!resp.ok)
                            throw new Error('Could not get classes at this time. Please try again.');

                        this.allClasses = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }

                },

                async getStudentForClass(classRefId) {
                    this.isFetching = true;
                    if (this.error)
                        this.error = null;

                    try {
                        const resp = await fetch(`/admin/ajax-routes/school-classes/${classRefId}/students`);

                        if (!resp.ok)
                            throw new Error('Could not get student for the selected class.');

                        this.classStudents = await resp.json();
                        this.showSubjects = !!(this.allClasses.length && this.classStudents.length);
                        if (!this.showSubjects)
                            this.displayAlert('No student for the selected class.');

                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.isFetching = false;
                    }
                },

                async getSubjectForClass(classRefId, studentUid) {
                    this.isFetching = true;
                    if (this.error)
                        this.error = null;
                    this.studentUId = studentUid;

                    try {

                        const [_, __] = await Promise.all([
                            await (await fetch(`/admin/ajax-routes/subjects/class/${classRefId}`)).json(),
                            await (await fetch(`/admin/ajax-routes/students/${studentUid}/results`)).json(),
                        ]);

                        this.subjectCombos = _;
                        this.selectedStudentResults = __;
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.isFetching = false;
                    }
                },

                submitForm(event) {
                    const {target: form} = event;
                    const studentId = document.getElementById('student').value;

                    form.setAttribute('action', `/admin/add-result/${studentId}`);

                    form.submit();
                },

                displayAlert(message) {
                    Swal.fire({
                        title: message,
                        denyButtonText: 'Close',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    });
                }
            }));
        });
    </script>
@endsection
