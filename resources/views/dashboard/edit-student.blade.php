@extends('layouts.master')
@section('title', 'Update student\'s info')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Edit Student</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="pageData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <h2 class="d-flex justify-content-between align-items-center w-100">
                        <span>Update student info</span>
                        <button
                            type="button"
                            class="btn btn-primary btn-sm"
                            @click="history.back()"
                        ><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12 col-md-6"

                        >
                            <template x-if="!loading">
                                <form action="" method="post">
                                    @csrf
                                    @method('PATCH')

                                    <span class="section">Personal Info</span>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="firstName">First
                                            name<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control"
                                                   id="firstName"
                                                   name="first_name"
                                                   required="required"
                                                   :value="student.first_name"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="surname">Surname<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control optional"
                                                   id="surname"
                                                   name="surname"
                                                   type="text"
                                                   :value="student.surname"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="email">Email</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control email optional"
                                                   id="email"
                                                   name="email"
                                                   type="email"
                                                   :value="student.user.email"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="rollId">Roll
                                            ID
                                            <span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control"
                                                   id="rollId"
                                                   type="text"
                                                   name="roll_id"
                                                   required='required'
                                                   :value="student.roll_id"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="dob">Date of
                                            Birth
                                        </label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control optional"
                                                   id="dob"
                                                   type="date"
                                                   name="dob"
                                                   :value="formattedDOB"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align"
                                               for="dob">Gender</label>
                                        <div class="col-md-6 col-sm-6">
                                            <div id="gender" class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-secondary" data-toggle-class="btn-primary"
                                                       data-toggle-passive-class="btn-default">
                                                    <input
                                                        type="radio"
                                                        name="gender"
                                                        value="male"
                                                        class="join-btn"
                                                        :checked="student.user.gender == 'male'"
                                                    >Male &nbsp; </label>
                                                <label class="btn btn-primary" data-toggle-class="btn-primary"
                                                       data-toggle-passive-class="btn-default">
                                                    <input
                                                        type="radio"
                                                        name="gender"
                                                        value="female"
                                                        class="join-btn"
                                                        :checked="student.user.gender == 'female'"
                                                    >Female </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="class">Class<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control" id="class" name="school_class_id" required>
                                                <option value="">Choose..</option>
                                                <template x-for="cl in classes" :key="cl.id">
                                                    <option x-text="`${cl.year_group.short_code} ${cl.name}`"
                                                            :value="cl.id"
                                                            :selected="cl.id == student.school_class_id"
                                                    ></option>
                                                </template>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="ln_solid pt-4">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                student: null,
                studentUId: null,
                classes: null,
                formattedDOB: null,

                async init() {
                    const pathname = location.pathname;
                    const splicedPathname = pathname.split('/');
                    const uId = splicedPathname[splicedPathname.length - 1];
                    this.studentUId = uId;

                    try {
                        const [_, __] = await Promise.all([
                            await (await fetch(`/admin/ajax-routes/students/${uId}`)).json(),
                            await (await fetch(`/admin/ajax-routes/school-classes/all`)).json(),
                        ]);

                        this.student = _;
                        this.classes = __;

                        if(!!this.student && (!!this.student.dob)) {
                            const date = Date.parse(this.student.dob)
                            const cDate = new Date(date);
                            this.formattedDOB = cDate.toISOString().substring(0,10);
                        }

                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });
    </script>
@endsection
