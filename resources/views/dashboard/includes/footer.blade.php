<!-- footer content -->
<footer>
    <div class="pull-right">
{{--        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>--}}
        Copyright &copy;  {{ now()->year }} {{ config('app.name') }}
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
