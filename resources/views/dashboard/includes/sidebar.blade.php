<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('dashboard') }}" class="site_title">
                <img src="{{ asset('assets/images/favicon.png') }}"
                     alt="{{ config('app.name') }}"
                     width="30"
                >
                <span class="text-uppercase">{{ config('app.name') }}</span>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
{{--        <div class="profile clearfix" style="display: inline-flex; align-items: center">--}}
{{--            <div class="profile_pic">--}}
{{--                <img src="{{ $user->avatarPath }}"--}}
{{--                     alt="..."--}}
{{--                     class="img-circle profile_img">--}}
{{--            </div>--}}
{{--            <div class="profile_info">--}}
{{--                <span>Welcome,</span>--}}
{{--                <h2 class="text-uppercase">{{ $user->first_name }}&nbsp;<br>{{ $user->surname }}</h2>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Dashboard </a></li>
                    <li><a><i class="fa fa-edit"></i> Classes <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('classes.add') }}">Add Class</a></li>
                            <li><a href="{{ route('classes.manage') }}">Manage Class</a></li>
                            <li><a href="{{ route('year-groups.manage') }}">Manage Year Groups</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-desktop"></i> Subjects <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('subjects.create') }}">Create Subject</a></li>
                            <li><a href="{{ route('subjects.manage') }}">Manage Subject</a></li>
                            <li><a href="{{ route('subject.add-combination') }}">Add Subject Combination</a></li>
                            <li><a href="{{ route('subject.manage-combination') }}">Manage Subject Combination</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-users" aria-hidden="true"></i>
                            Students <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('students.add') }}">Add Student</a></li>
                            <li><a href="{{ route('students.manage') }}">Manage Students</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-bar-chart-o"></i> Result <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('results.add') }}">Add Result</a></li>
                            <li><a href="{{ route('results.manage') }}">Manage Result</a></li>
                        </ul>
                    </li>
                    <li><a><i class="fa fa-clone"></i>Notices <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('notices.add') }}">Add Notice</a></li>
                            <li><a href="{{ route('notices.manage') }}">Manage Notice</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('profile') }}"><i class="fa fa-user" aria-hidden="true"></i> Profile </a>
                </ul>
            </div>
        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="#">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
