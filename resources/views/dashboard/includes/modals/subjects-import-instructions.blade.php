<div class="modal fade" id="subjectsImportInstructions" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Instructions:</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <ol>
                    <li>
                        Your Excel file must have exactly two columns with the following headings:<br/>
                        - Column 1: Name ex: English Language<br/>
                        - Column 2: Code (optional) ex: Eng
                    </li>
                    <li>
                        The second column, 'Code,' is optional. You can leave it blank for any row if needed.
                    </li>
                    <li>
                        Your file can contain as many rows as needed to upload multiple records.
                    </li>
                </ol>
                <p>Please ensure that your file adheres to these instructions to ensure a successful upload. If you have any questions or need assistance, feel free to contact our support team.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
