<div class="modal fade" id="classesImportInstructions" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Instructions:</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <ol>
                    <li>
                        Your Excel file must have exactly three columns with the following headings:<br/>
                        - Column 1: Year group ex: JSS1<br/>
                        - Column 2: Class name ex: A<br/>
                        - Column 3: Alias (optional) ex: JA1
                    </li>
                    <li>
                        The third column, 'Alias,' is optional. You can leave it blank for any row if needed.
                    </li>
                    <li>
                        Your file can contain as many rows as needed to upload multiple records.
                    </li>
                </ol>
                <p>Please ensure that your file adheres to these instructions to ensure a successful upload. If you have any questions or need assistance, feel free to contact our support team.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
