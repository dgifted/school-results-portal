@extends('layouts.master')
@section('title', 'Manage notices')
@section('styles')
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/dataTables.bootstrap5.min.css">
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Manage Notices</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div x-cloak x-data="pageData">
        @if(session('message'))
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                        <strong>Info!</strong> <br>
                        {{ session('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <template x-if="loading">
                    @include('dashboard.includes.loading')
                </template>

                <div class="x_panel">
                    <div class="x_title">
                        <h2 class="d-flex justify-content-between align-items-center w-100 my-0">
                            <span>Manage school notices</span>
                            <a href="{{ route('notices.add') }}" class="btn btn-sm btn-info">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                Add Notice</a>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <template x-if="!!error">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <span x-text="error"></span>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </template>

                        <div class="row">
                            <div class="col-12">
                                <table class="table table-hover table-sm" id="schoolNoticesDatatable">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Approval status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <template x-if="!!notices?.length">
                                        <template x-for="(notice, idx) in notices" :key="notice.id">
                                            <tr>
                                                <td x-text="idx+1"></td>
                                                <td x-text="notice.title"></td>
                                                <td>
                                                    <span class="badge"
                                                          x-text="notice.approval_status"
                                                          :class="{
                                                            'badge-secondary': notice.approved == 0,
                                                            'badge-success': notice.approved == 1,
                                                            'badge-danger': notice.approved == 2
                                                          }"
                                                    ></span>
                                                </td>
                                                <td>
                                                    <a class="btn btn-info btn-sm"
                                                       :href="`/admin/notices/${notice.ref_id}`"
                                                    >
                                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                                    </a>
                                                    <button class="btn btn-danger btn-sm"
                                                            @click="showDeleteDialog(notice.ref_id)"
                                                    >
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </template>
                                    </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form class="d-none" id="deletionForm" method="post">
        @csrf
        @method('delete')
    </form>
@stop

@section('scripts')
    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.13.4/js/dataTables.bootstrap.min.js"></script>
    <script>
        // $(document).ready(function () {
        //     $('#schoolNoticesDatatable').DataTable();
        // });

        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                notices: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/notices`);

                        if (!resp.ok)
                            throw new Error('Error fetching notices from server. Please try again.');

                        this.notices = await resp.json();
                        console.log('Notices ', this.notices);
                        // $(document).ready(function () {
                        //     $('#schoolNoticesDatatable').DataTable();
                        // });
                    } catch (ex) {
                        this.error = ex;
                    } finally {
                        this.loading = false;
                    }
                },
                showDeleteDialog(noticeRefId) {
                    Swal.fire({
                        title: 'Do you want to delete notice?',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        denyButtonText: 'No',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    })
                        .then(result => {
                            console.log(result);
                            if (result.isConfirmed) {
                                this.deleteNotice(noticeRefId)
                            }
                        });
                },
                deleteNotice(noticeRefId) {
                    const form = document.getElementById('deletionForm');
                    form.setAttribute('action', `/admin/notices/${noticeRefId}/delete`);
                    form.submit();
                },
            }));
        });

    </script>
@endsection
