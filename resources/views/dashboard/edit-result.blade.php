@extends('layouts.master')
@section('title', 'Update result')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Edit Result</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-12"
             x-cloak
             x-data="pageData"
        >
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>

            <div class="x_panel">
                <div class="x_title">
                    <h2 x-html="`Update <span class='text-success text-uppercase'>${student.first_name} ${student.surname}</span>'s result`"></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12 col-md-6"

                        >
                            <template x-if="!loading">
                                <form :action="`/admin/edit-result/${studentUId}`" method="post">
                                    @csrf
                                    @method('PATCH')

                                    <template x-for="result in results">
                                        <div class="field item form-group mb-2">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                   :for="result.subject.name"
                                                   x-text="result.subject.name">
                                            ></label>
                                            <div class="col-md-6 col-sm-6">
                                                <input class="form-control"
                                                       type="number"
                                                       :name="result.id"
                                                       :id="result.subject.name"
                                                       :value="result.marks"
                                                       min="0"
                                                       max="100"
                                                />
                                            </div>
                                        </div>
                                    </template>

                                    <div class="ln_solid pt-3">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Update Result</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                isFetching: false,
                student: null,
                results: null,
                studentUId: null,

                async init() {
                    const pathname = location.pathname;
                    const splicedPathname = pathname.split('/');
                    const uId = splicedPathname[splicedPathname.length - 1];
                    this.studentUId = uId;

                    try {
                        const [_, __] = await Promise.all([
                            await (await fetch(`/admin/ajax-routes/students/${uId}`)).json(),
                            await (await fetch(`/admin/ajax-routes/students/${uId}/results`)).json()
                        ]);
                        this.results = __;
                        this.student = _;
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });
    </script>
@endsection
