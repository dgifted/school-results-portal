@extends('layouts.master')
@section('title', 'Add subject combination')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Add Subject Combination</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add subject combinations</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <template x-if="!loading">
                                <form action="{{ route('subject.add-combination') }}" method="post">
                                    @csrf

                                    <div class="field item form-group mb-4">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="class">Class<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control"
                                                    id="class"
                                                    name="school_class_id"
                                                    required
                                                    x-model="selectedClassRef"
                                            >
                                                <option value="">Choose..</option>
                                                <template x-for="cl in classes" :key="cl.id">
                                                    <option
                                                        x-text="`${cl.year_group.short_code} ${cl.name}`"
                                                        :value="cl.id"
                                                    ></option>
                                                </template>
                                            </select>
                                        </div>
                                    </div>

                                    <template x-if="!!subjects && !!subjects.length && !!selectedClassRef">
                                        <div class="field item form-group mb-4">
                                            <label class="col-form-label col-md-3 col-sm-3  label-align"
                                                   for="subject">Subjects<span class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="d-flex flex-column">
                                                    <template x-for="(subject, idx) in subjects">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input
                                                                    type="checkbox"
                                                                    class="flat"
                                                                    name="subjects[]"
                                                                    :checked="subject?.checked"
                                                                    :value="subject.id"
                                                                >
                                                                <span x-text="subject?.name"></span>
                                                            </label>
                                                        </div>
                                                    </template>
                                                </div>

                                            </div>
                                        </div>
                                    </template>
                                    <template x-if="!subjects ||!subjects.length">
                                        <div>
                                            <p>Your school has not subject added yet. Start by adding some subjects</p>
                                            <div class="field item form-group mb-4">
                                                <a
                                                    class="btn btn-sm btn-info"
                                                    href="{{ route('subjects.create') }}"
                                                >Add subjects</a>
                                            </div>
                                        </div>
                                    </template>

                                    <div class="ln_solid pt-3">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Submit</button>
                                                <button type='reset' class="btn btn-success">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                isFetching: false,
                classes: null,
                error: null,
                subjects: null,
                selectedClassRef: null,
                subCombos: null,

                async init() {
                    try {
                        const [_classes, _subjects] = await Promise.all([
                            await (await fetch(`/admin/ajax-routes/school-classes/all`)).json(),
                            await (await fetch(`/admin/ajax-routes/subjects/all`)).json()
                        ]);

                        this.classes = _classes;
                        this.subjects = _subjects;
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                    this.$watch('selectedClassRef', () => {
                        this.getClassSubjectCombos();
                    });
                    console.log('Subjects loaded: ', this.subjects);
                },
                async getClassSubjectCombos() {
                    if (!!this.selectedClassRef) {
                        this.isFetching = true;
                        try {
                            const resp = await fetch(`/admin/ajax-routes/school-classes/${this.selectedClassRef}/subject-combos`);

                            if (!resp.ok)
                                throw new Error('Could not fetch school class subject combination.');

                            this.subCombos = await resp.json();
                        } catch (e) {
                            this.error = e;
                        } finally {
                            this.isFetching = false;
                        }
                        this.checkSubjectAlreadyAssigned();
                    }
                },
                checkSubjectAlreadyAssigned() {
                    this.subjects = (this.subjects || []).map( item => ({
                        ...item,
                        checked: false
                    }));

                    if (this.subCombos && !!this.subCombos.length) {
                        const subCombosIdsArray = this.subCombos.map(item => item.subject_id);
                        this.subjects = this.subjects.map(subject => ({
                            ...subject,
                            checked: subCombosIdsArray.includes(subject.id)
                        }));
                    }
                }
            }));
        });
    </script>
@endsection
