@extends('layouts.master')
@section('title', 'Add student')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Add Student</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Fill the student information</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <template x-if="!loading">
                                <form action="{{ route('students.add') }}" method="post">
                                    @csrf

                                    <span class="section">Personal Info</span>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="class">Class<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select class="form-control" id="class" name="school_class_id" required>
                                                <option value="">Choose class</option>
                                                <template x-for="cl in classes" :key="cl.id">
                                                    <option x-text="`${cl.year_group.short_code} ${cl.name}`"
                                                            :value="cl.id"
                                                    ></option>
                                                </template>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align" for="firstName">First
                                            name<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control"
                                                   id="firstName"
                                                   name="first_name"
                                                   required="required"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="surname">Surname<span
                                                class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control optional"
                                                   id="surname"
                                                   name="surname"
                                                   type="text"/>
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"
                                               for="email">Email</label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control email optional"
                                                   id="email"
                                                   name="email"
                                                   type="email"/>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="dob">Date of
                                            Birth
                                        </label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control optional"
                                                   id="dob"
                                                   type="date"
                                                   name="dob"
                                            />
                                        </div>
                                    </div>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align"
                                               for="dob">Gender</label>
                                        <div class="col-md-6 col-sm-6">
                                            <div id="gender" class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-secondary" data-toggle-class="btn-primary"
                                                       data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="gender" value="male" class="join-btn">
                                                    &nbsp;Male &nbsp;
                                                </label>
                                                <label class="btn btn-primary" data-toggle-class="btn-primary"
                                                       data-toggle-passive-class="btn-default">
                                                    <input type="radio" name="gender" value="female" class="join-btn">
                                                    Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ln_solid pt-4">
                                        <div class="form-group">
                                            <div class="col-md-6 offset-md-3">
                                                <button type='submit' class="btn btn-primary">Submit</button>
                                                <button type='reset' class="btn btn-success">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                classes: null,
                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/school-classes/all`);

                        if (!resp.ok)
                            throw new Error('We could not get school classes.');

                        this.classes = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });
    </script>
@endsection
