@extends('layouts.master')
@section('title', 'Manage subjects')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Manage Subjects</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="d-flex justify-content-between align-items-center w-100 my-0">
                        <span>View subjects info</span>
                        <a href="{{ route('subjects.create') }}" class="btn btn-sm btn-info">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Add Subject</a>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>
                    <div class="row">
                        <div class="col-sm-12">
                            <template x-if="!loading">
                                <div class="card-box table-responsive">
                                    <table id="datatable-checkbox"
                                           class="table table-striped table-bordered table-sm bulk_action"
                                           style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Subject Name</th>
                                            <th>Subject Code</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <template x-for="(sub, idx) in subjects" :key="sub.id">
                                            <tr>
                                                <td x-text="idx+1"></td>
                                                <td x-text="sub.name"></td>
                                                <td x-text="sub.code"></td>
                                                <td>
                                                    <span class="badge badge-success">Active</span>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm btn-danger"
                                                            @click="showDeleteDialog(sub.ref_id)"
                                                    >
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </template>
                                        </tbody>
                                    </table>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form class="d-none" id="subjectDeleteForm" method="post">
        @csrf
        @method('delete')
    </form>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                subjects: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/subjects/all`);

                        if (!resp.ok)
                            throw new Error('Could not get subjects.');

                        this.subjects = await resp.json();
                        // console.log('subjects: ', this.subjects);
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },
                showDeleteDialog(subjectRefId) {
                    Swal.fire({
                        title: 'Do you want to delete subject?',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        denyButtonText: 'No',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    })
                        .then(result => {
                            console.log(result);
                            if (result.isConfirmed) {
                                this.deleteSubject(subjectRefId)
                            }
                        });
                },
                deleteSubject(subjectRefId) {
                    const form = document.getElementById('subjectDeleteForm');
                    const formAction = '/admin/ajax-routes/subjects/' + subjectRefId + '/delete';
                    form.setAttribute('action', formAction);
                    form.submit();
                },
            }));
        });
    </script>
@endsection
