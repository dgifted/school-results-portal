@extends('layouts.master')
@section('title', 'Manage results')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Manage Results</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2>View classes info</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <template x-if="!loading">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <form>
                                        <div class="field item form-group mb-4">
                                            <label class="col-form-label col-md-3 col-sm-3 label-align"
                                                   for="yearGroup">Year Group<span
                                                    class="required">*</span></label>
                                            <div class="col-md-6 col-sm-6">
                                                <select
                                                    class="form-control" id="yearGroup"
                                                    name="year_group"
                                                    required
                                                    x-ref="yearGroup"
                                                    @change="populateClasses($el.value)"
                                                >
                                                    <option>Choose..</option>
                                                    <template x-for="yG in yearGroups" :key="yG.id">
                                                        <option
                                                            x-text="`${yG.short_code}`"
                                                            :value="yG.ref_id"
                                                        ></option>
                                                    </template>
                                                </select>
                                            </div>
                                        </div>
                                        <template x-if="!!classes?.length">
                                            <div class="field item form-group mb-4">
                                                <label class="col-form-label col-md-3 col-sm-3 label-align"
                                                       for="class">Class<span
                                                        class="required">*</span></label>
                                                <div class="col-md-6 col-sm-6">
                                                    <select
                                                        class="form-control" id="class"
                                                        name="school_class"
                                                        required
                                                        x-ref="class"
                                                        @change="getClassResults($el.value)"
                                                        :data-classval="$el.value"
                                                    >
                                                        <option>Choose..</option>
                                                        <template x-for="cl in classes" :key="cl.id">
                                                            <option
                                                                x-text="`${cl.name}`"
                                                                :value="cl.ref_id"
                                                            ></option>
                                                        </template>
                                                    </select>
                                                </div>
                                            </div>
                                        </template>
                                    </form>
                                    <table id="datatable-checkbox"
                                           class="table table-striped table-bordered table-sm bulk_action"
                                           style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Student Name</th>
                                            <th>Student Id</th>
                                            <th>Subjects</th>
                                            <th>Date Added</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <template x-if="students?.length">
                                            <template x-for="(student, idx) in students" :key="student.id">
                                                <tr>
                                                    <td x-text="idx+1"></td>
                                                    <td x-text="`${student.first_name} ${student.surname}`"></td>
                                                    <td x-text="student?.reg_no"></td>
                                                    <td x-text="student.results?.length"></td>
                                                    <td x-text="formatDate(student?.results[0]?.created_at)"></td>
                                                    <td>
                                                        Active
                                                    </td>
                                                    <td>
                                                        <a :href="`/admin/result-detail/${student.unique_id}`"
                                                           class="btn btn-success btn-sm" title="View Result">
                                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                                        </a>
                                                        <a :href="`/admin/edit-result/${student.unique_id}`"
                                                           class="btn btn-info btn-sm" title="Edit Result">
                                                            <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </template>
                                        </template>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </template>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                isFetching: false,
                yearGroups: null,
                classes: null,
                selectedClass: null,
                results: null,
                students: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/year-groups/all`);

                        if (!resp.ok)
                            throw new Error('We could not get year groups at the moment.');

                        this.yearGroups = await resp.json();
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.loading = false;
                    }
                },

                populateClasses(selectedYearGroup) {
                    this.classes = this.yearGroups.find(yG => selectedYearGroup === yG.ref_id).classes || null;
                    if (!this.classes?.length) {
                        this.displayAlert('No class in the selected year group.');
                    }
                },

                async getClassResults(selectedClassRef) {
                    if (!!this.error)
                        this.error = null;
                    this.isFetching = true;

                    try {
                        const resp = await fetch(`/admin/ajax-routes/school-classes/${selectedClassRef}/results`);

                        if (!resp.ok)
                            throw new Error('We could not get class results at the moment.');

                        this.students = await resp.json();

                        if (!this.students?.length) {
                            this.displayAlert('No student found in the selected class.');
                        }
                    } catch (e) {
                        this.error = e;
                    } finally {
                        this.isFetching = false;
                    }
                },

                formatDate(dateString) {
                    const date = Date.parse(dateString);
                    const dDate = new Date(date);
                    return dDate.toISOString().substring(0, 10)
                },

                displayAlert(message) {
                    Swal.fire({
                        title: message,
                        denyButtonText: 'Close',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        }
                    });
                }
            }));
        });
    </script>
@endsection
