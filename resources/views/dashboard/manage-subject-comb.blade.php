@extends('layouts.master')
@section('title', 'Manage subject combination')
@section('styles')
@stop

@section('content')
    <div class="page-title">
        <div class="title_left">
            <h3 style="margin-left: 5px;">Manage Subject Combination</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row"
         x-cloak
         x-data="pageData"
    >
        <div class="col-12">
            <template x-if="loading">
                @include('dashboard.includes.loading')
            </template>
            <div class="x_panel">
                <div class="x_title">
                    <h2 class="d-flex justify-content-between align-items-center w-100 my-0">
                        <span class="">View Subjects Combination Info</span>
                        <a class="btn btn-sm btn-info" href="{{ route('subject.add-combination') }}">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Add subject combination</a>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @if(session('message'))
                        <div class="alert alert-{{ session('type') }} alert-dismissible fade show" role="alert">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <template x-if="!!error">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <span x-text="error"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </template>

                    <div class="row">
                        <div class="col-sm-12">
                            <template x-if="!loading">
                                <div class="card-box table-responsive">
                                    <table id="datatable-checkbox"
                                           class="table table-striped table-bordered table-sm bulk_action"
                                           style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Class</th>
                                            <th>Subject</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <template x-for="(sC, idx) in subjectCombos" :key="sC.id">
                                            <tr>
                                                <td x-text="idx+1"></td>
                                                <td x-text="`${sC.school_class.year_group.short_code} ${sC.school_class.name}`"></td>
                                                <td x-text="sC.subject.name"></td>
                                                <td>
                                                <span class="badge my-0 px-3 py-2"
                                                      :class="sC.status_text === 'active' ? 'badge-success' : 'badge-danger'"
                                                      x-text="sC.status_text.toUpperCase()"
                                                >
                                                </span>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm"
                                                            :class="sC.status_text == 'active' ? 'btn-danger' : 'btn-success'"
                                                            :title="sC.status_text == 'active' ? 'Deactivate' : 'Activate'"
                                                            :data-id="sC.id"
                                                            @click="toggleSubComboStatus($el.dataset.id)"
                                                    >
                                                        <i :class="sC.status_text == 'active' ? 'fa fa-ban' : 'fa fa-check'"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </template>
                                        </tbody>
                                    </table>
                                </div>
                            </template>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form class="d-none" id="toggleForm" method="post">
            @csrf
            @method('PATCH')
        </form>
    </div>
@stop

@section('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('pageData', () => ({
                loading: true,
                error: null,
                subjectCombos: null,

                async init() {
                    try {
                        const resp = await fetch(`/admin/ajax-routes/subject-combinations/all`);

                        if (!resp.ok)
                            throw new Error('Could not fetch subject combinations at this time. Please try again.');

                        this.subjectCombos = await resp.json();
                    } catch (e) {
                        this.error = e;
                        console.log('ERROR: ', e);
                    } finally {
                        this.loading = false;
                    }
                }
            }));
        });

        function toggleSubComboStatus(id) {
            const form = document.getElementById('toggleForm');
            form.setAttribute('action', `/admin/toggle-subject-combination/${id}`);
            form.submit();
        }
    </script>
@endsection
