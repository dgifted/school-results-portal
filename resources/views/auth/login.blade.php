@extends('layouts.auth')
@section('title', 'Login')

@section('styles')

@stop

@section('content')
    <div class="animate form login_form">
        <section class="login_content">
            <form action="{{ route('login') }}" method="post">
                @csrf

                <h1>Login Form</h1>
                <div>
                    <label for="email" class="d-none sr-only">Email</label>
                    <input
                        type="text"
                        class="form-control @error('email') is-invalid @enderror"
                        id="email"
                        name="email"
                        placeholder="Email"
                        required=""
                        value="{{ old('email') }}"
                    />
                    @error('email')
                    <p class="text-danger pt-0 text-left form-error">{{ $message }}</p>
                    @enderror
                </div>
                <div>

                    <label for="password" class="d-none sr-only">Password</label>
                    <input
                        type="password"
                        class="form-control @error('password') is-invalid @enderror"
                        id="password"
                        name="password"
                        placeholder="Password"
                        required=""
                    />
                    @error('password')
                    <p class="text-danger pt-0 text-left form-error">{{ $message }}</p>
                    @enderror
                </div>
                <div>
                    <button class="btn btn-default submit" type="submit">Log in</button>
                    <a class="reset_pass" href="{{ route('password.request') }}">Lost your password?</a>
                </div>

                <div class="clearfix"></div>

                <div class="separator">
                    <div class="clearfix"></div>
                    <br/>

                    <div>
{{--                        <h1><i class="fa fa-paw"></i> {{ config('app.name') }}</h1>--}}
                        <h1>
                            <a href="{{ route('home') }}" class="d-inline-block text-decoration-none">
                                <img src="{{ asset('assets/images/logo.png') }}" style="width: 250px;"/>
                            </a>
                        </h1>
                        <p>©{{ now()->year }} All Rights Reserved. {{ config('app.name') }}</p>
                    </div>
                </div>
            </form>
        </section>
    </div>
@endsection
